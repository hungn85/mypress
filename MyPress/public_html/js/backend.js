$(document).ready(function() {
    
    //ckeditor
	$( '.ckeditor' ).ckeditor();
    
    
    //remove create_at, updated_at
	$('label[for$="created_at"], label[for$="updated_at"]').parent().remove();
    
    //focus left menu
    $('#menu a').each(function() {
        
        var href = $(this).attr('href');
        if(!href) return;
        
        href = href.replace('backend_dev.php', 'backend.php');
        href = href.replace('/backend.php/', '');
        
        var loc = location.href;
        loc = loc.replace('backend_dev.php', 'backend.php');
        var arr = loc.split('backend.php');
        
        arr  = arr[1].split('/');
        
        if(href == arr[1]) {
            $(this).css('font-weight', 'bold');
        }
    });

    $('select').select2();
});