<?php

class myUtil {
  
    public static function trimArray($routeData) {
      foreach($routeData as $k => $v) {
        $routeData[$k] = trim($v);
      }
      return $routeData;
    }

    public static function removeFile($path, $uploadFolder = true) {
        if(is_file($path)) {
            @unlink($path);
        }
        elseif($uploadFolder) {
          $path = sfConfig::get('sf_web_dir').$path;
          if(is_file($path)) {
            @unlink($path);
          }
        }
    }
    
    public static function uploadFile($name, $folder, $maxSize = 0, $fileExts = array()) {
        $request = sfContext::getInstance()->getRequest();
        
        $f = '';
        if(isset($_FILES[$name]) && $_FILES[$name]['size'] > 0 && $_FILES[$name]['error'] == 0) {

            if($maxSize && $maxSize*1024000 < $_FILES[$name]['size']) {
                myUtil::exception(1, 'Dung lượng file được phép upload tối đa là '.($maxSize).'Mb');
            }
            
            $fileName = date('ymdhi').'_'.rand(1000,9999)."_".myUtil::strToSlug($request->getFileName($name));

            $ext = pathinfo($_FILES[$name]['name']);

            if(isset($ext['extension'])) {
                $ext = '.'.$ext['extension'];
            }
            else {
                $ext = $request->getFileExtension($name);
            }
            
            if($fileExts && !in_array($ext, $fileExts)) {
                myUtil::exception(1, 'Các file có đuôi dạng: '.implode(', ', $fileExts).' mới được phép tải lên');
            }
            
            $f = $fileName.$ext;
            
            try {
                $request->moveFile($name, sfConfig::get('sf_upload_dir').$folder.'/'.$f);
            }
            catch(Exception $e) {
                myUtil::exception(1, $e->getMessage());
            }

        }
        return $f;
    }
    
    public static function getDataFromExcel($file) {

      set_include_path(get_include_path() . PATH_SEPARATOR . sfConfig::get('sf_root_dir').'/excel_reader/Classes/');
      include 'PHPExcel/IOFactory.php';

      try {
          $objPHPExcel = PHPExcel_IOFactory::load($file);
      } catch(Exception $e) {
          myUtil::exception(1, $e->getMessage());
      }
      return $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
   }
   
   public static function genPassword() {
       return rand(1000, 9999);
   }
   public static $dayInWeek = array(
      
      1 => 'Thứ Hai',
      2 => 'Thứ Ba',
      3 => 'Thứ Tư',
      4 => 'Thứ Năm',
      5 => 'Thứ Sáu',
      6 => 'Thứ Bảy',
      7 => 'Chủ Nhật'
   );
   
   public static function dateTimeToSystemDateTime($dateTime) {
         
        preg_match_all('/(\d{2}):(\d{2}) (\d{2})\/(\d{2})\/(\d{4})/', $dateTime, $m);
         
        if(sizeof($m) == 6 && isset($m[5][0]) && isset($m[4][0]) && isset($m[3][0]) && isset($m[2][0]) && isset($m[1][0])) {
          $time = $m[5][0].'-'.$m[4][0]."-".$m[3][0]." ".$m[1][0].":".$m[2][0];
          if(strtotime($time)) {
              return $time;
          }
        }
        return null;
    }
    
    public static function dateToSystemDate($date) {
         
        preg_match_all('/(\d{2})\/(\d{2})\/(\d{4})/', $date, $m);
         
        if(sizeof($m) == 4 && isset($m[3][0]) && isset($m[2][0]) && isset($m[1][0]) ) {
          $date = $m[3][0].'-'.$m[2][0]."-".$m[1][0];
          if(strtotime($date)) {
            return $date;
          }
        }
        return null;
    }
    
   public static function getDayInWeek($dayNum) {
      return LangPeer::getText(self::$dayInWeek[$dayNum]);
   }
   
   public static function getDayInWeeks() {
      $dats = array();
      foreach(self::$dayInWeek as $index => $val) {
         $dats [$index] = LangPeer::getText($val);
      }
      return $dats;
   }
    
   public static function exception($code, $message, $position = '') {
      throw new Exception(
         json_encode(
            array(
               'code' => $code, 'message' => $message, 'position' => $position)));
   }

   public static function exceptionIfUnless($item) {
      if(!$item) {
        self::exception($code = 1, $message = 'Item not found' , $position = '');
      }
   }
   
   public static function getException($e) {
      $dat = $e->getMessage();
      $json = json_decode($dat, true);
      return $json;
   }
     
   public static function getRouteName() {
      return sfContext::getInstance()->getRouting()->getCurrentRouteName();
   }
   
   public static function isValidCaptcha($captchaText) {
        $ct = sfContext::getInstance();
        $myCap = new captchaValidator($ct);
        $myCap->initialize($ct);
        return $myCap->execute($captchaText, $err);
   }

   public static function url_for($query) {
      $enNotHasRoute = false;
      if(myUtil::isEn()) {

         $qr = "";
         $arr = explode('?', $query);
         $route = $arr[0];
         if(sizeof($arr) > 1) {
            $qr = $arr[1];
         }
         $hasRoute = false;
         if(strpos($query, '@') === 0) {
            $route .= '_en';
            $params = true;
         }
         else {
            $enNotHasRoute = true;
            if($qr) {
               $params = parse_str($qr);
               if (isset($params['lang'])) {
                  unset($params['lang']);
               }
               $qr = http_build_query($params);
            }  
         }
         $query = $route.'?'.$qr;
      }
      sfLoader::loadHelpers(array('Tag', 'Url'));
      return url_for($query).($enNotHasRoute?"?lang=en":"");
   } 
   
   public static function get_partial($template, $vars = array()) {
      sfLoader::loadHelpers(array('Tag', 'Partial'));
      return get_partial($template, $vars);
   }
   
   public static function isEn() {

      return self::getLang() != 'vn';
   }
 
   public static function getLang() {
      $inst = sfContext::getInstance();
      $rq = $inst->getRequest();
      $lc = 'vn';
      if ($rq->hasParameter('lang')) {
         $lc = $rq->getParameter('lang', 'vn');
      } 
      
      return $lc;
   }
 
   public static function validateCaptcha($captcha_value) {

      $ct = sfContext::getInstance();
      $myCaptchaValidator = new captchaValidator($ct);
      $myCaptchaValidator->initialize($ct);

      if (!strlen($captcha_value) || !$myCaptchaValidator->execute($captcha_value, $captcha_error)) {
         return false;
      }
      return true;
   }
    
   public static function setCookie($name, $val, $exTime) {

      setcookie($name, $val, time() + $exTime, '/', $_SERVER['HTTP_HOST']);
   }
 

   public static function getMimes() {
      $mime_types = array("avi" => "video/x-msvideo",
          "bmp" => "image/bmp",
          "gif" => "image/gif",
          "png" => "image/png",
          "jpeg" => "image/jpeg",
          "jpg" => "image/jpeg",
          "lsf" => "video/x-la-asf",
          "lsx" => "video/x-la-asf",
          "mid" => "audio/mid",
          "mov" => "video/quicktime",
          "movie" => "video/x-sgi-movie",
          "mp2" => "video/mpeg",
          "mp3" => "audio/mpeg",
          'mp4' => 'video/mp4',
          "mpa" => "video/mpeg",
          "mpe" => "video/mpeg",
          "mpeg" => "video/mpeg",
          "mpg" => "video/mpeg",
          "mpv2" => "video/mpeg",
          "pdf" => "application/pdf");
      return $mime_types;
   }

   public static function genToken($maxlength = 0) {


      // start with a blank password
      $password = "";

      // define possible characters - any character in this string can be
      // picked for use in the password, so if you want to put vowels back in
      // or add special characters such as exclamation marks, this is where
      // you should do it
      $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

      // we refer to the length of $possible a few times, so let's grab it now
      $maxlength = $maxlength?$maxlength:strlen($possible);

      // check for length overflow and truncate if necessary
      //if ($length > $maxlength) 
      {
         $length = $maxlength;
      }

      // set up a counter for how many characters are in the password so far
      $i = 0;

      // add random characters to $password until $length is reached
      while ($i < $length) {

         // pick a random character from the possible ones
         $char = substr($possible, mt_rand(0, $maxlength - 1), 1);

         // have we already used this character in $password?
         if (!strstr($password, $char)) {
            // no, so it's OK to add it onto the end of whatever we've already got...
            $password .= $char;
            // ... and increase the counter by one
            $i++;
         }
      }

      // done!
      return $password;
   }

   public static function isEmail($email) {
      return preg_match('|^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$|i', $email);
   }

   public static function getEmailFromString($string) {
      preg_match_all("/[a-z]+[\._a-zA-Z0-9-]+@[a-z]+[\._a-zA-Z0-9-]+/i", $string, $m);
      if ($m) {
         return $m[0];
      }
      return array();
   }

   public static function getViDayInWeek() {
      return array('Monday' => 'Thứ hai',
          'Tuesday' => 'Thứ ba',
          'Wednesday' => 'Thứ tư',
          'Thursday' => 'Thứ năm',
          'Friday' => 'Thứ sáu',
          'Saturday' => 'Thứ bảy',
          'Sunday' => 'Chủ nhật');
   }

   public static function phoneNumberValidTo84($phone) {
      if (strpos($phone, '+') === 0) {
         return substr($phone, 1);
      }

      if (strpos($phone, '84') === 0) {
         return $phone;
      }

      if (strpos($phone, '0') === 0) {
         return '84' . substr($phone, 1);
      }

      return '84' . $phone;
   }

   public static function phoneNumberValidTo0x($phone) {
      $phone = self::phoneNumberValidTo84($phone);
      return '0' . substr($phone, 2); 
   }

   

   public static function formatText($str) {
      $str = trim($str);
      $str = str_replace("\r", "", $str);
      while (strpos($str, "\n\n") !== false) {
         $str = str_replace("\n\n", "\n", $str);
      }
      return $str;
   }

   public static function getPhoneNumberFromString($str) {
      preg_match_all('#[0-9]+#', $str, $m);
      if ($m) {
         $dat = $m[0];
         $ps = array();
         for ($i = 0; $i < sizeof($dat); $i++) {
            $p = $dat[$i];
            if (self::isValidPhoneNumber($p)) {
               $ps [] = $p;
            }
         }
         return $ps;
      }
      return array();
   }

   public static function isValidPhoneNumber($phone) {
      $regs = self::getMobilePartterns();
      foreach ($regs as $parttern) {
         if (preg_match($parttern, $phone))
            return true;
      }
      return false;
   }

   public static function getMobileOperator($phone) {
      $regs = self::getMobilePartterns();
      foreach ($regs as $mb => $parttern) {
         if (preg_match($parttern, $phone))
            return $mb;
      }
      return false;
   }
   
   public static function getMobilePartterns() {

      $gpcPattern = '/^((\+|)84|0|)(9(1|4)|12(3|4|5|7|9))\d{7}$/';

      $vmsPattern = '/^((\+|)84|0|)(9(0|3)|12(0|1|2|6|8))\d{7}$/';

      $viettelPattern = '/^((\+|)84|0|)(9(6|7|8)|16(8|9|6|7|3|4|5|2))\d{7}$/';

      $sfone = '/^((\+|)84|0|)(95|155)\d{7}$/';

      $vnm = '/^((\+|)84|0|)(92|188)\d{7}$/';

      $beeline = '/^((\+|)84|0|)((1|)99)\d{7}$/';

      $regs = array(
          'GPC' => $gpcPattern,
          'VMS' => $vmsPattern,
          'VTL' => $viettelPattern,
          'SFONE' => $sfone,
          'VNM' => $vnm,
          'BEELINE' => $beeline);
      return $regs;
   }
   
   public static function image_resize($src, $dst, $width, $height, $crop = 0) {

      if (!list($w, $h) = getimagesize($src))
         return "Unsupported picture type!";

      $type = strtolower(substr(strrchr($src, "."), 1));
      if ($type == 'jpeg')
         $type = 'jpg';
      switch ($type) {
         case 'bmp': $img = imagecreatefromwbmp($src);
            break;
         case 'gif': $img = imagecreatefromgif($src);
            break;
         case 'jpg': $img = imagecreatefromjpeg($src);
            break;
         case 'png': $img = imagecreatefrompng($src);
            break;
         default : return "Unsupported picture type!";
      }

      // resize
      if ($crop) {
         if ($w < $width or $h < $height)
            return "Picture is too small!";
         $ratio = max($width / $w, $height / $h);
         $h = $height / $ratio;
         $x = ($w - $width / $ratio) / 2;
         $w = $width / $ratio;
      }
      else {
         if ($w < $width and $h < $height)
            return "Picture is too small!";
         //$ratio = min($width/$w, $height/$h);
         //$width = $w * $ratio;
         //$height = $h * $ratio;
         $x = 0;
      }

      $new = imagecreatetruecolor($width, $height);

      // preserve transparency
      if ($type == "gif" or $type == "png") {
         imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
         imagealphablending($new, false);
         imagesavealpha($new, true);
      }

      imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);

      switch ($type) {
         case 'bmp': imagewbmp($new, $dst);
            break;
         case 'gif': imagegif($new, $dst);
            break;
         case 'jpg': imagejpeg($new, $dst);
            break;
         case 'png': imagepng($new, $dst);
            break;
      }
      return true;
   }

   public static function strToSlug($str, $delimiter = '-', $replace = array(), $lower = true) {
      $str = self::removeMark($str);
      $str = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);

      if ($lower)
         $str = strtolower(trim($str, '-'));
      $str = preg_replace("/[\/_|+ -]+/", $delimiter, $str);
      if (sizeof($replace)) {
         $str = str_replace(array_keys($replace), array_values($replace), $str);
      }
      return $str;
   }

   public static function removeMark($str, $lower = true) {
      $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
      $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
      $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
      $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
      $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
      $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
      $str = preg_replace("/(đ)/", 'd', $str);
      $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
      $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
      $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
      $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
      $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
      $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
      $str = preg_replace("/(Đ)/", 'D', $str);
      if ($lower)
         $str = strtolower($str);
      $str = trim($str);
      $str = preg_replace('/[^\x20-\x7E\n]/', '', $str);
      return $str;
   }

}
