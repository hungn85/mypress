﻿
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow" />
	<meta name="viewport" content="width=device-width,maximum-scale=1,user-scalable=no,minimal-ui">
	<title>404 Not Found</title>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800&amp;subset=latin,latin-ext">
	<link rel="stylesheet" type="text/css" media="all" href="/assets/error/css/error.css"/>
</head>
<body>
	
	<div class="stars"></div>

	<div class="sun-moon">
		<div class="sun"></div>
		<div class="moon"></div>
	</div>

	<div id="js-hills" class="background hills"></div>
	<div id="js-country" class="background country"></div>
	<div id="js-foreground" class="background foreground"></div>

	<div class="error-content">
        System Busy
	</div>

	<a href="/" class="button">Homepage</a>

		<div class="code">
			<span>5</span>
			<span>0</span>
			<span>0</span>
		</div>

</body>

</html>
