<?php

/**
 * Project form base class.
 *
 * @package    form
 * @version    SVN: $Id: BaseFormPropel.class.php 6882 2008-01-02 08:27:53Z fabien $
 */
abstract class BaseFormPropel extends sfFormPropel
{
    public function setup()
    {
    }
  
    public function useWidgets($cols) {
       
        $all = $this->validatorSchema->getFields();
        $other = array_diff($all, $cols);
        foreach($other as $dif) {
            unset($this[$dif]);
        }
    }
}
