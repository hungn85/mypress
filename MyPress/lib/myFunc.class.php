<?php

class myFunc {

  public static function isset1($array, $key, $value) {
    return isset($array[$key]) ? $array[$key] : $value;
  }

  public static function notEmpty($array, $key, $value) {
    return !empty($array[$key]) ? $array[$key] : $value;
  }

  public static function get1False2($check, $v1, $v2) {
    return $check ? $v1 : $v2;
  }

  public static function getVnEn($v1, $v2) {
    return self::get1False2(!myUtil::isEn(), $v1, $v2);
  }

  public static function arrAddIf($check, $array, $value, $key = '') {
    if($check) {
      if($key === '') {
        $array [] = $value;
      }
      else {
        $array [$key] = $value;
      }
    }
    
    return $array;
  }

  public static function arrAddIfValue($value, $array, $key = '') {
    return self::arrAddIf($value, $array, $value, $key);
  }
}
