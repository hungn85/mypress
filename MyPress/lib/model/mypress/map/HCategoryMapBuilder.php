<?php



class HCategoryMapBuilder {

	
	const CLASS_NAME = 'lib.model.mypress.map.HCategoryMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('h_category');
		$tMap->setPhpName('HCategory');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('PARENT_ID', 'ParentId', 'int', CreoleTypes::INTEGER, 'h_category', 'ID', false, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('NAME_EN', 'NameEn', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('LINK', 'Link', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('LINK_EN', 'LinkEn', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PRIORITY', 'Priority', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('HIDE', 'Hide', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JSON', 'Json', 'string', CreoleTypes::VARCHAR, false, 1000);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addForeignKey('SEO_ROUTE_ID', 'SeoRouteId', 'int', CreoleTypes::INTEGER, 'h_route', 'ID', false, null);

		$tMap->addForeignKey('SEO_EN_ROUTE_ID', 'SeoEnRouteId', 'int', CreoleTypes::INTEGER, 'h_route', 'ID', false, null);

		$tMap->addColumn('SEO_SLUG', 'SeoSlug', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_EN_SLUG', 'SeoEnSlug', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_TITLE', 'SeoTitle', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_EN_TITLE', 'SeoEnTitle', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_DESCRIPTION', 'SeoDescription', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_EN_DESCRIPTION', 'SeoEnDescription', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_OG_IMAGE', 'SeoOgImage', 'string', CreoleTypes::VARCHAR, false, 600);

	} 
} 