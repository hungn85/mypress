<?php



class HRouteMapBuilder {

	
	const CLASS_NAME = 'lib.model.mypress.map.HRouteMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('h_route');
		$tMap->setPhpName('HRoute');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('DESCRIPTION', 'Description', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PARTTERN', 'Parttern', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('REQUIREMENTS', 'Requirements', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('MODEL', 'Model', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('MODELS', 'Models', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('MODULE', 'Module', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('ACTION', 'Action', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PARAMS', 'Params', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PRIORITY', 'Priority', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ADD_CACHE', 'AddCache', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SEO_TITLE', 'SeoTitle', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_DESCRIPTION', 'SeoDescription', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('SEO_OG_IMAGE', 'SeoOgImage', 'string', CreoleTypes::VARCHAR, false, 600);

		$tMap->addColumn('JSON', 'Json', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 