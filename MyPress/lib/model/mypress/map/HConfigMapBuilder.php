<?php



class HConfigMapBuilder {

	
	const CLASS_NAME = 'lib.model.mypress.map.HConfigMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('h_config');
		$tMap->setPhpName('HConfig');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CODE', 'Code', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('VAL', 'Val', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VAL_EN', 'ValEn', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FROM_TIME', 'FromTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('TO_TIME', 'ToTime', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 