<?php



class HCategoryPostMapBuilder {

	
	const CLASS_NAME = 'lib.model.mypress.map.HCategoryPostMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('h_category_post');
		$tMap->setPhpName('HCategoryPost');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('POST_ID', 'PostId', 'int', CreoleTypes::INTEGER, 'h_post', 'ID', false, null);

		$tMap->addForeignKey('CATEGORY_ID', 'CategoryId', 'int', CreoleTypes::INTEGER, 'h_category', 'ID', false, null);

	} 
} 