<?php

/**
 * Subclass for performing query and update operations on the 'h_category' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HCategoryPeer extends BaseHCategoryPeer
{
	public static function getBySlug($slug) {
		$c = new Criteria;
		$c->add(self::SEO_SLUG, $slug);
		return self::doSelectOne($c);
	}

   public static function retrieveBySlug($slug, $id = 0) {
      $c = new Criteria;
      if($id) {
         $c->add(self::ID, $id, Criteria::NOT_EQUAL); 
      }
      
      $c->add(self::SEO_SLUG, $slug);
      return self::doSelectOne($c);
   }

   public static function retrieveByEnSlug($slug, $id = 0) {
      $c = new Criteria;
      if($id) {
         $c->add(self::ID, $id, Criteria::NOT_EQUAL); 
      }
      
      $c->add(self::SEO_EN_SLUG, $slug);
      return self::doSelectOne($c);
   }
}
