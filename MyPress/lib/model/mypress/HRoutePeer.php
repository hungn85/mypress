<?php

/**
 * Subclass for performing query and update operations on the 'h_route' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HRoutePeer extends BaseHRoutePeer {

    public static function retreiveByRouteName($name) {
        $c =  new Criteria;
        $c->add(self::NAME, $name);
        return self::doSelectOne($c);
    }

    public static function retreiveByParttern($name, $id = 0) {
        $c =  new Criteria;
        if($id) {
            $c->add(self::ID, $id, Criteria::NOT_EQUAL);    
        }
        $c->add(self::PARTTERN, $name);
        return self::doSelectOne($c);
    }


    public static function addSeoModel($model, $routeData, $routeEnData) {
        $pasm = new PlugAddSeoModel($model, $routeData, $routeEnData);
        return $pasm->execute();
    } 
 
}
