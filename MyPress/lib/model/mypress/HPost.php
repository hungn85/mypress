<?php

/**
 * Subclass for representing a row from the 'h_post' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HPost extends BaseHPost
{
	public function getCategoryName() {
		return '';
	}

	public function getDateTime() {
		return $this->getCreatedAt('H\h l, d F Y');
	}
 
	public function getDetailUrl() {
		 
		return  myUtil::url_for('@post?slug='.myFunc::getVnEn($this->getSeoSlug(), $this->getSeoEnSlug()));
	}
}
