<?php

/**
 * Subclass for performing query and update operations on the 'h_config' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HConfigPeer extends BaseHConfigPeer
{
	static function get($code) { 
      $all = self::getAll();
      return isset($all[$code]) ? $all[$code] : null;
   }

   public static $all = null;
   public static function getAll() {
      if(self::$all === null) {
         $rs  = self::doSelect(new Criteria);
         foreach($rs as $r) {
         	self::$all[$r->getCode()] = $r->getVal();
         }
      }
      return self::$all;
   }
}
