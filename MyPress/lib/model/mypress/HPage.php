<?php

/**
 * Subclass for representing a row from the 'h_page' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HPage extends BaseHPage
{
   public function delete($con = null) {
      DeleteRouteReferencePlug::delete($this);
      parent::delete($con);

   }
}
