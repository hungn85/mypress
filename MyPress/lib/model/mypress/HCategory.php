<?php

/**
 * Subclass for representing a row from the 'h_category' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HCategory extends BaseHCategory
{ 
	public function getDetailUrl() {
		 
		return  
         $this->getLink() ? $this->getLink() : 
            myFunc::getVnEn($this->getSeoSlug(), $this->getSeoEnSlug());
	}

   public function delete($con = null) {
      DeleteRouteReferencePlug::delete($this);
      parent::delete($con);
   }
}
