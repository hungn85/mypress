<?php

/**
 * Subclass for performing query and update operations on the 'h_post' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HPostPeer extends BaseHPostPeer
{
	public static function getOnWebLimit() {
		return self::doSelect(new Criteria);
	}

	public static function retrieveBySlug($slug, $id = 0) {
		$c = new Criteria;
		if($id) {
			$c->add(self::ID, $id, Criteria::NOT_EQUAL);	
		}
		
		$c->add(self::SEO_SLUG, $slug);
		return self::doSelectOne($c);
	}

	public static function retrieveByEnSlug($slug, $id = 0) {
		$c = new Criteria;
		if($id) {
			$c->add(self::ID, $id, Criteria::NOT_EQUAL);	
		}
		
		$c->add(self::SEO_EN_SLUG, $slug);
		return self::doSelectOne($c);
	}
}
