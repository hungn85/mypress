<?php

/**
 * Subclass for representing a row from the 'h_route' table.
 *
 * 
 *
 * @package lib.model.mypress
 */ 
class HRoute extends BaseHRoute
{
	public function __toString() {
		return $this->getParttern();
	}
}
