<?php


abstract class BaseHCategory extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $parent_id = 0;


	
	protected $name;


	
	protected $name_en;


	
	protected $link;


	
	protected $link_en;


	
	protected $priority = 10;


	
	protected $hide = false;


	
	protected $json;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $seo_route_id;


	
	protected $seo_en_route_id;


	
	protected $seo_slug;


	
	protected $seo_en_slug;


	
	protected $seo_title;


	
	protected $seo_en_title;


	
	protected $seo_description;


	
	protected $seo_en_description;


	
	protected $seo_og_image;

	
	protected $aHCategoryRelatedByParentId;

	
	protected $aHRouteRelatedBySeoRouteId;

	
	protected $aHRouteRelatedBySeoEnRouteId;

	
	protected $collHCategorysRelatedByParentId;

	
	protected $lastHCategoryRelatedByParentIdCriteria = null;

	
	protected $collHCategoryPosts;

	
	protected $lastHCategoryPostCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getParentId()
	{

        return $this->parent_id;
	}

	
	public function getName()
	{

        return $this->name;
	}

	
	public function getNameEn()
	{

        return $this->name_en;
	}

	
	public function getLink()
	{

        return $this->link;
	}

	
	public function getLinkEn()
	{

        return $this->link_en;
	}

	
	public function getPriority()
	{

        return $this->priority;
	}

	
	public function getHide()
	{

        return $this->hide;
	}

	
	public function getJson()
	{

        $ret = @json_decode($this->json, true);
         if(!$ret) {
           $ret = array();
         }
         return $ret;
        
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSeoRouteId()
	{

        return $this->seo_route_id;
	}

	
	public function getSeoEnRouteId()
	{

        return $this->seo_en_route_id;
	}

	
	public function getSeoSlug()
	{

        return $this->seo_slug;
	}

	
	public function getSeoEnSlug()
	{

        return $this->seo_en_slug;
	}

	
	public function getSeoTitle()
	{

        return $this->seo_title;
	}

	
	public function getSeoEnTitle()
	{

        return $this->seo_en_title;
	}

	
	public function getSeoDescription()
	{

        return $this->seo_description;
	}

	
	public function getSeoEnDescription()
	{

        return $this->seo_en_description;
	}

	
	public function getSeoOgImage()
	{

        return $this->seo_og_image;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HCategoryPeer::ID;
		}

	} 
	
	public function setParentId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v || $v === 0) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = HCategoryPeer::PARENT_ID;
		}

		if ($this->aHCategoryRelatedByParentId !== null && $this->aHCategoryRelatedByParentId->getId() !== $v) {
			$this->aHCategoryRelatedByParentId = null;
		}

	} 
	
	public function setName($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = HCategoryPeer::NAME;
		}

	} 
	
	public function setNameEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name_en !== $v) {
			$this->name_en = $v;
			$this->modifiedColumns[] = HCategoryPeer::NAME_EN;
		}

	} 
	
	public function setLink($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->link !== $v) {
			$this->link = $v;
			$this->modifiedColumns[] = HCategoryPeer::LINK;
		}

	} 
	
	public function setLinkEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->link_en !== $v) {
			$this->link_en = $v;
			$this->modifiedColumns[] = HCategoryPeer::LINK_EN;
		}

	} 
	
	public function setPriority($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->priority !== $v || $v === 10) {
			$this->priority = $v;
			$this->modifiedColumns[] = HCategoryPeer::PRIORITY;
		}

	} 
	
	public function setHide($v)
	{

		if ($this->hide !== $v || $v === false) {
			$this->hide = $v;
			$this->modifiedColumns[] = HCategoryPeer::HIDE;
		}

	} 
	
	public function setJson($v)
	{

        $v = json_encode($v);
        
        
		if ($this->json !== $v) {
			$this->json = $v;
			$this->modifiedColumns[] = HCategoryPeer::JSON;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = HCategoryPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = HCategoryPeer::UPDATED_AT;
		}

	} 
	
	public function setSeoRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_route_id !== $v) {
			$this->seo_route_id = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoRouteId !== null && $this->aHRouteRelatedBySeoRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoRouteId = null;
		}

	} 
	
	public function setSeoEnRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_en_route_id !== $v) {
			$this->seo_en_route_id = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_EN_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoEnRouteId !== null && $this->aHRouteRelatedBySeoEnRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoEnRouteId = null;
		}

	} 
	
	public function setSeoSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_slug !== $v) {
			$this->seo_slug = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_SLUG;
		}

	} 
	
	public function setSeoEnSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_slug !== $v) {
			$this->seo_en_slug = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_EN_SLUG;
		}

	} 
	
	public function setSeoTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_title !== $v) {
			$this->seo_title = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_TITLE;
		}

	} 
	
	public function setSeoEnTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_title !== $v) {
			$this->seo_en_title = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_EN_TITLE;
		}

	} 
	
	public function setSeoDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_description !== $v) {
			$this->seo_description = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_DESCRIPTION;
		}

	} 
	
	public function setSeoEnDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_description !== $v) {
			$this->seo_en_description = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_EN_DESCRIPTION;
		}

	} 
	
	public function setSeoOgImage($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_og_image !== $v) {
			$this->seo_og_image = $v;
			$this->modifiedColumns[] = HCategoryPeer::SEO_OG_IMAGE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->parent_id = $rs->getInt($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->name_en = $rs->getString($startcol + 3);

			$this->link = $rs->getString($startcol + 4);

			$this->link_en = $rs->getString($startcol + 5);

			$this->priority = $rs->getInt($startcol + 6);

			$this->hide = $rs->getBoolean($startcol + 7);

			$this->json = $rs->getString($startcol + 8);

			$this->created_at = $rs->getTimestamp($startcol + 9, null);

			$this->updated_at = $rs->getTimestamp($startcol + 10, null);

			$this->seo_route_id = $rs->getInt($startcol + 11);

			$this->seo_en_route_id = $rs->getInt($startcol + 12);

			$this->seo_slug = $rs->getString($startcol + 13);

			$this->seo_en_slug = $rs->getString($startcol + 14);

			$this->seo_title = $rs->getString($startcol + 15);

			$this->seo_en_title = $rs->getString($startcol + 16);

			$this->seo_description = $rs->getString($startcol + 17);

			$this->seo_en_description = $rs->getString($startcol + 18);

			$this->seo_og_image = $rs->getString($startcol + 19);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 20; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HCategory object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HCategoryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HCategoryPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(HCategoryPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(HCategoryPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HCategoryPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aHCategoryRelatedByParentId !== null) {
				if ($this->aHCategoryRelatedByParentId->isModified()) {
					$affectedRows += $this->aHCategoryRelatedByParentId->save($con);
				}
				$this->setHCategoryRelatedByParentId($this->aHCategoryRelatedByParentId);
			}

			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if ($this->aHRouteRelatedBySeoRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoRouteId($this->aHRouteRelatedBySeoRouteId);
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if ($this->aHRouteRelatedBySeoEnRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoEnRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoEnRouteId($this->aHRouteRelatedBySeoEnRouteId);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HCategoryPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HCategoryPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collHCategorysRelatedByParentId !== null) {
				foreach($this->collHCategorysRelatedByParentId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHCategoryPosts !== null) {
				foreach($this->collHCategoryPosts as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aHCategoryRelatedByParentId !== null) {
				if (!$this->aHCategoryRelatedByParentId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHCategoryRelatedByParentId->getValidationFailures());
				}
			}

			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoRouteId->getValidationFailures());
				}
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoEnRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoEnRouteId->getValidationFailures());
				}
			}


			if (($retval = HCategoryPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collHCategoryPosts !== null) {
					foreach($this->collHCategoryPosts as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getParentId();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getNameEn();
				break;
			case 4:
				return $this->getLink();
				break;
			case 5:
				return $this->getLinkEn();
				break;
			case 6:
				return $this->getPriority();
				break;
			case 7:
				return $this->getHide();
				break;
			case 8:
				return $this->getJson();
				break;
			case 9:
				return $this->getCreatedAt();
				break;
			case 10:
				return $this->getUpdatedAt();
				break;
			case 11:
				return $this->getSeoRouteId();
				break;
			case 12:
				return $this->getSeoEnRouteId();
				break;
			case 13:
				return $this->getSeoSlug();
				break;
			case 14:
				return $this->getSeoEnSlug();
				break;
			case 15:
				return $this->getSeoTitle();
				break;
			case 16:
				return $this->getSeoEnTitle();
				break;
			case 17:
				return $this->getSeoDescription();
				break;
			case 18:
				return $this->getSeoEnDescription();
				break;
			case 19:
				return $this->getSeoOgImage();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HCategoryPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getParentId(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getNameEn(),
			$keys[4] => $this->getLink(),
			$keys[5] => $this->getLinkEn(),
			$keys[6] => $this->getPriority(),
			$keys[7] => $this->getHide(),
			$keys[8] => $this->getJson(),
			$keys[9] => $this->getCreatedAt(),
			$keys[10] => $this->getUpdatedAt(),
			$keys[11] => $this->getSeoRouteId(),
			$keys[12] => $this->getSeoEnRouteId(),
			$keys[13] => $this->getSeoSlug(),
			$keys[14] => $this->getSeoEnSlug(),
			$keys[15] => $this->getSeoTitle(),
			$keys[16] => $this->getSeoEnTitle(),
			$keys[17] => $this->getSeoDescription(),
			$keys[18] => $this->getSeoEnDescription(),
			$keys[19] => $this->getSeoOgImage(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setParentId($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setNameEn($value);
				break;
			case 4:
				$this->setLink($value);
				break;
			case 5:
				$this->setLinkEn($value);
				break;
			case 6:
				$this->setPriority($value);
				break;
			case 7:
				$this->setHide($value);
				break;
			case 8:
				$this->setJson($value);
				break;
			case 9:
				$this->setCreatedAt($value);
				break;
			case 10:
				$this->setUpdatedAt($value);
				break;
			case 11:
				$this->setSeoRouteId($value);
				break;
			case 12:
				$this->setSeoEnRouteId($value);
				break;
			case 13:
				$this->setSeoSlug($value);
				break;
			case 14:
				$this->setSeoEnSlug($value);
				break;
			case 15:
				$this->setSeoTitle($value);
				break;
			case 16:
				$this->setSeoEnTitle($value);
				break;
			case 17:
				$this->setSeoDescription($value);
				break;
			case 18:
				$this->setSeoEnDescription($value);
				break;
			case 19:
				$this->setSeoOgImage($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HCategoryPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setParentId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNameEn($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setLink($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setLinkEn($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPriority($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHide($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setJson($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setCreatedAt($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setUpdatedAt($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSeoRouteId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSeoEnRouteId($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSeoSlug($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSeoEnSlug($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSeoTitle($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSeoEnTitle($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setSeoDescription($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setSeoEnDescription($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setSeoOgImage($arr[$keys[19]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HCategoryPeer::DATABASE_NAME);

		if ($this->isColumnModified(HCategoryPeer::ID)) $criteria->add(HCategoryPeer::ID, $this->id);
		if ($this->isColumnModified(HCategoryPeer::PARENT_ID)) $criteria->add(HCategoryPeer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(HCategoryPeer::NAME)) $criteria->add(HCategoryPeer::NAME, $this->name);
		if ($this->isColumnModified(HCategoryPeer::NAME_EN)) $criteria->add(HCategoryPeer::NAME_EN, $this->name_en);
		if ($this->isColumnModified(HCategoryPeer::LINK)) $criteria->add(HCategoryPeer::LINK, $this->link);
		if ($this->isColumnModified(HCategoryPeer::LINK_EN)) $criteria->add(HCategoryPeer::LINK_EN, $this->link_en);
		if ($this->isColumnModified(HCategoryPeer::PRIORITY)) $criteria->add(HCategoryPeer::PRIORITY, $this->priority);
		if ($this->isColumnModified(HCategoryPeer::HIDE)) $criteria->add(HCategoryPeer::HIDE, $this->hide);
		if ($this->isColumnModified(HCategoryPeer::JSON)) $criteria->add(HCategoryPeer::JSON, $this->json);
		if ($this->isColumnModified(HCategoryPeer::CREATED_AT)) $criteria->add(HCategoryPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(HCategoryPeer::UPDATED_AT)) $criteria->add(HCategoryPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(HCategoryPeer::SEO_ROUTE_ID)) $criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->seo_route_id);
		if ($this->isColumnModified(HCategoryPeer::SEO_EN_ROUTE_ID)) $criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->seo_en_route_id);
		if ($this->isColumnModified(HCategoryPeer::SEO_SLUG)) $criteria->add(HCategoryPeer::SEO_SLUG, $this->seo_slug);
		if ($this->isColumnModified(HCategoryPeer::SEO_EN_SLUG)) $criteria->add(HCategoryPeer::SEO_EN_SLUG, $this->seo_en_slug);
		if ($this->isColumnModified(HCategoryPeer::SEO_TITLE)) $criteria->add(HCategoryPeer::SEO_TITLE, $this->seo_title);
		if ($this->isColumnModified(HCategoryPeer::SEO_EN_TITLE)) $criteria->add(HCategoryPeer::SEO_EN_TITLE, $this->seo_en_title);
		if ($this->isColumnModified(HCategoryPeer::SEO_DESCRIPTION)) $criteria->add(HCategoryPeer::SEO_DESCRIPTION, $this->seo_description);
		if ($this->isColumnModified(HCategoryPeer::SEO_EN_DESCRIPTION)) $criteria->add(HCategoryPeer::SEO_EN_DESCRIPTION, $this->seo_en_description);
		if ($this->isColumnModified(HCategoryPeer::SEO_OG_IMAGE)) $criteria->add(HCategoryPeer::SEO_OG_IMAGE, $this->seo_og_image);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HCategoryPeer::DATABASE_NAME);

		$criteria->add(HCategoryPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setParentId($this->parent_id);

		$copyObj->setName($this->name);

		$copyObj->setNameEn($this->name_en);

		$copyObj->setLink($this->link);

		$copyObj->setLinkEn($this->link_en);

		$copyObj->setPriority($this->priority);

		$copyObj->setHide($this->hide);

		$copyObj->setJson($this->json);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setSeoRouteId($this->seo_route_id);

		$copyObj->setSeoEnRouteId($this->seo_en_route_id);

		$copyObj->setSeoSlug($this->seo_slug);

		$copyObj->setSeoEnSlug($this->seo_en_slug);

		$copyObj->setSeoTitle($this->seo_title);

		$copyObj->setSeoEnTitle($this->seo_en_title);

		$copyObj->setSeoDescription($this->seo_description);

		$copyObj->setSeoEnDescription($this->seo_en_description);

		$copyObj->setSeoOgImage($this->seo_og_image);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getHCategorysRelatedByParentId() as $relObj) {
				if($this->getPrimaryKey() === $relObj->getPrimaryKey()) {
						continue;
				}

				$copyObj->addHCategoryRelatedByParentId($relObj->copy($deepCopy));
			}

			foreach($this->getHCategoryPosts() as $relObj) {
				$copyObj->addHCategoryPost($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HCategoryPeer();
		}
		return self::$peer;
	}

	
	public function setHCategoryRelatedByParentId($v)
	{


		if ($v === null) {
			$this->setParentId('0');
		} else {
			$this->setParentId($v->getId());
		}


		$this->aHCategoryRelatedByParentId = $v;
	}


	
	static $HCategoryRelatedByParentId = array();
	
	public function getHCategoryRelatedByParentId($con = null)
	{
		if ($this->aHCategoryRelatedByParentId === null && ($this->parent_id !== null)) {
						if(!isset(self::$HCategoryRelatedByParentId[$this->parent_id])){
				self::$HCategoryRelatedByParentId[$this->parent_id] = HCategoryPeer::retrieveByPK($this->parent_id, $con);
			}
			$this->aHCategoryRelatedByParentId = self::$HCategoryRelatedByParentId[$this->parent_id];

			
		}
		return $this->aHCategoryRelatedByParentId;
	}

	
	public function setHRouteRelatedBySeoRouteId($v)
	{


		if ($v === null) {
			$this->setSeoRouteId(NULL);
		} else {
			$this->setSeoRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoRouteId = $v;
	}


	
	static $HRouteRelatedBySeoRouteId = array();
	
	public function getHRouteRelatedBySeoRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoRouteId === null && ($this->seo_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoRouteId[$this->seo_route_id])){
				self::$HRouteRelatedBySeoRouteId[$this->seo_route_id] = HRoutePeer::retrieveByPK($this->seo_route_id, $con);
			}
			$this->aHRouteRelatedBySeoRouteId = self::$HRouteRelatedBySeoRouteId[$this->seo_route_id];

			
		}
		return $this->aHRouteRelatedBySeoRouteId;
	}

	
	public function setHRouteRelatedBySeoEnRouteId($v)
	{


		if ($v === null) {
			$this->setSeoEnRouteId(NULL);
		} else {
			$this->setSeoEnRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoEnRouteId = $v;
	}


	
	static $HRouteRelatedBySeoEnRouteId = array();
	
	public function getHRouteRelatedBySeoEnRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoEnRouteId === null && ($this->seo_en_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id])){
				self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id] = HRoutePeer::retrieveByPK($this->seo_en_route_id, $con);
			}
			$this->aHRouteRelatedBySeoEnRouteId = self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id];

			
		}
		return $this->aHRouteRelatedBySeoEnRouteId;
	}

	
	public function initHCategorysRelatedByParentId()
	{
		if ($this->collHCategorysRelatedByParentId === null) {
			$this->collHCategorysRelatedByParentId = array();
		}
	}

	
	public function getHCategorysRelatedByParentId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedByParentId === null) {
			if ($this->isNew()) {
			   $this->collHCategorysRelatedByParentId = array();
			} else {

				$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				if (!isset($this->lastHCategoryRelatedByParentIdCriteria) || !$this->lastHCategoryRelatedByParentIdCriteria->equals($criteria)) {
					$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHCategoryRelatedByParentIdCriteria = $criteria;
		return $this->collHCategorysRelatedByParentId;
	}

	
	public function countHCategorysRelatedByParentId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

		return HCategoryPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHCategoryRelatedByParentId(HCategory $l)
	{
		$this->collHCategorysRelatedByParentId[] = $l;
		$l->setHCategoryRelatedByParentId($this);
	}


	
	public function getHCategorysRelatedByParentIdJoinHRouteRelatedBySeoRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedByParentId === null) {
			if ($this->isNew()) {
				$this->collHCategorysRelatedByParentId = array();
			} else {

				$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

				$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelectJoinHRouteRelatedBySeoRouteId($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

			if (!isset($this->lastHCategoryRelatedByParentIdCriteria) || !$this->lastHCategoryRelatedByParentIdCriteria->equals($criteria)) {
				$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelectJoinHRouteRelatedBySeoRouteId($criteria, $con);
			}
		}
		$this->lastHCategoryRelatedByParentIdCriteria = $criteria;

		return $this->collHCategorysRelatedByParentId;
	}


	
	public function getHCategorysRelatedByParentIdJoinHRouteRelatedBySeoEnRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedByParentId === null) {
			if ($this->isNew()) {
				$this->collHCategorysRelatedByParentId = array();
			} else {

				$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

				$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelectJoinHRouteRelatedBySeoEnRouteId($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPeer::PARENT_ID, $this->getId());

			if (!isset($this->lastHCategoryRelatedByParentIdCriteria) || !$this->lastHCategoryRelatedByParentIdCriteria->equals($criteria)) {
				$this->collHCategorysRelatedByParentId = HCategoryPeer::doSelectJoinHRouteRelatedBySeoEnRouteId($criteria, $con);
			}
		}
		$this->lastHCategoryRelatedByParentIdCriteria = $criteria;

		return $this->collHCategorysRelatedByParentId;
	}

	
	public function initHCategoryPosts()
	{
		if ($this->collHCategoryPosts === null) {
			$this->collHCategoryPosts = array();
		}
	}

	
	public function getHCategoryPosts($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategoryPosts === null) {
			if ($this->isNew()) {
			   $this->collHCategoryPosts = array();
			} else {

				$criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->getId());

				HCategoryPostPeer::addSelectColumns($criteria);
				$this->collHCategoryPosts = HCategoryPostPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->getId());

				HCategoryPostPeer::addSelectColumns($criteria);
				if (!isset($this->lastHCategoryPostCriteria) || !$this->lastHCategoryPostCriteria->equals($criteria)) {
					$this->collHCategoryPosts = HCategoryPostPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHCategoryPostCriteria = $criteria;
		return $this->collHCategoryPosts;
	}

	
	public function countHCategoryPosts($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->getId());

		return HCategoryPostPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHCategoryPost(HCategoryPost $l)
	{
		$this->collHCategoryPosts[] = $l;
		$l->setHCategory($this);
	}


	
	public function getHCategoryPostsJoinHPost($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategoryPosts === null) {
			if ($this->isNew()) {
				$this->collHCategoryPosts = array();
			} else {

				$criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->getId());

				$this->collHCategoryPosts = HCategoryPostPeer::doSelectJoinHPost($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->getId());

			if (!isset($this->lastHCategoryPostCriteria) || !$this->lastHCategoryPostCriteria->equals($criteria)) {
				$this->collHCategoryPosts = HCategoryPostPeer::doSelectJoinHPost($criteria, $con);
			}
		}
		$this->lastHCategoryPostCriteria = $criteria;

		return $this->collHCategoryPosts;
	}

} 