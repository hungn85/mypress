<?php


abstract class BaseHBackendMenu extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $parent_id = 0;


	
	protected $name;


	
	protected $link;


	
	protected $priority = 10;


	
	protected $hide = false;


	
	protected $created_at;


	
	protected $updated_at;

	
	protected $aHBackendMenuRelatedByParentId;

	
	protected $collHBackendMenusRelatedByParentId;

	
	protected $lastHBackendMenuRelatedByParentIdCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getParentId()
	{

        return $this->parent_id;
	}

	
	public function getName()
	{

        return $this->name;
	}

	
	public function getLink()
	{

        return $this->link;
	}

	
	public function getPriority()
	{

        return $this->priority;
	}

	
	public function getHide()
	{

        return $this->hide;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::ID;
		}

	} 
	
	public function setParentId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v || $v === 0) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::PARENT_ID;
		}

		if ($this->aHBackendMenuRelatedByParentId !== null && $this->aHBackendMenuRelatedByParentId->getId() !== $v) {
			$this->aHBackendMenuRelatedByParentId = null;
		}

	} 
	
	public function setName($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::NAME;
		}

	} 
	
	public function setLink($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->link !== $v) {
			$this->link = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::LINK;
		}

	} 
	
	public function setPriority($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->priority !== $v || $v === 10) {
			$this->priority = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::PRIORITY;
		}

	} 
	
	public function setHide($v)
	{

		if ($this->hide !== $v || $v === false) {
			$this->hide = $v;
			$this->modifiedColumns[] = HBackendMenuPeer::HIDE;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = HBackendMenuPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = HBackendMenuPeer::UPDATED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->parent_id = $rs->getInt($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->link = $rs->getString($startcol + 3);

			$this->priority = $rs->getInt($startcol + 4);

			$this->hide = $rs->getBoolean($startcol + 5);

			$this->created_at = $rs->getTimestamp($startcol + 6, null);

			$this->updated_at = $rs->getTimestamp($startcol + 7, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HBackendMenu object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HBackendMenuPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HBackendMenuPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(HBackendMenuPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(HBackendMenuPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HBackendMenuPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aHBackendMenuRelatedByParentId !== null) {
				if ($this->aHBackendMenuRelatedByParentId->isModified()) {
					$affectedRows += $this->aHBackendMenuRelatedByParentId->save($con);
				}
				$this->setHBackendMenuRelatedByParentId($this->aHBackendMenuRelatedByParentId);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HBackendMenuPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HBackendMenuPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collHBackendMenusRelatedByParentId !== null) {
				foreach($this->collHBackendMenusRelatedByParentId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aHBackendMenuRelatedByParentId !== null) {
				if (!$this->aHBackendMenuRelatedByParentId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHBackendMenuRelatedByParentId->getValidationFailures());
				}
			}


			if (($retval = HBackendMenuPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HBackendMenuPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getParentId();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getLink();
				break;
			case 4:
				return $this->getPriority();
				break;
			case 5:
				return $this->getHide();
				break;
			case 6:
				return $this->getCreatedAt();
				break;
			case 7:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HBackendMenuPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getParentId(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getLink(),
			$keys[4] => $this->getPriority(),
			$keys[5] => $this->getHide(),
			$keys[6] => $this->getCreatedAt(),
			$keys[7] => $this->getUpdatedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HBackendMenuPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setParentId($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setLink($value);
				break;
			case 4:
				$this->setPriority($value);
				break;
			case 5:
				$this->setHide($value);
				break;
			case 6:
				$this->setCreatedAt($value);
				break;
			case 7:
				$this->setUpdatedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HBackendMenuPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setParentId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLink($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPriority($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHide($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCreatedAt($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUpdatedAt($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HBackendMenuPeer::DATABASE_NAME);

		if ($this->isColumnModified(HBackendMenuPeer::ID)) $criteria->add(HBackendMenuPeer::ID, $this->id);
		if ($this->isColumnModified(HBackendMenuPeer::PARENT_ID)) $criteria->add(HBackendMenuPeer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(HBackendMenuPeer::NAME)) $criteria->add(HBackendMenuPeer::NAME, $this->name);
		if ($this->isColumnModified(HBackendMenuPeer::LINK)) $criteria->add(HBackendMenuPeer::LINK, $this->link);
		if ($this->isColumnModified(HBackendMenuPeer::PRIORITY)) $criteria->add(HBackendMenuPeer::PRIORITY, $this->priority);
		if ($this->isColumnModified(HBackendMenuPeer::HIDE)) $criteria->add(HBackendMenuPeer::HIDE, $this->hide);
		if ($this->isColumnModified(HBackendMenuPeer::CREATED_AT)) $criteria->add(HBackendMenuPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(HBackendMenuPeer::UPDATED_AT)) $criteria->add(HBackendMenuPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HBackendMenuPeer::DATABASE_NAME);

		$criteria->add(HBackendMenuPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setParentId($this->parent_id);

		$copyObj->setName($this->name);

		$copyObj->setLink($this->link);

		$copyObj->setPriority($this->priority);

		$copyObj->setHide($this->hide);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getHBackendMenusRelatedByParentId() as $relObj) {
				if($this->getPrimaryKey() === $relObj->getPrimaryKey()) {
						continue;
				}

				$copyObj->addHBackendMenuRelatedByParentId($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HBackendMenuPeer();
		}
		return self::$peer;
	}

	
	public function setHBackendMenuRelatedByParentId($v)
	{


		if ($v === null) {
			$this->setParentId('0');
		} else {
			$this->setParentId($v->getId());
		}


		$this->aHBackendMenuRelatedByParentId = $v;
	}


	
	static $HBackendMenuRelatedByParentId = array();
	
	public function getHBackendMenuRelatedByParentId($con = null)
	{
		if ($this->aHBackendMenuRelatedByParentId === null && ($this->parent_id !== null)) {
						if(!isset(self::$HBackendMenuRelatedByParentId[$this->parent_id])){
				self::$HBackendMenuRelatedByParentId[$this->parent_id] = HBackendMenuPeer::retrieveByPK($this->parent_id, $con);
			}
			$this->aHBackendMenuRelatedByParentId = self::$HBackendMenuRelatedByParentId[$this->parent_id];

			
		}
		return $this->aHBackendMenuRelatedByParentId;
	}

	
	public function initHBackendMenusRelatedByParentId()
	{
		if ($this->collHBackendMenusRelatedByParentId === null) {
			$this->collHBackendMenusRelatedByParentId = array();
		}
	}

	
	public function getHBackendMenusRelatedByParentId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHBackendMenusRelatedByParentId === null) {
			if ($this->isNew()) {
			   $this->collHBackendMenusRelatedByParentId = array();
			} else {

				$criteria->add(HBackendMenuPeer::PARENT_ID, $this->getId());

				HBackendMenuPeer::addSelectColumns($criteria);
				$this->collHBackendMenusRelatedByParentId = HBackendMenuPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HBackendMenuPeer::PARENT_ID, $this->getId());

				HBackendMenuPeer::addSelectColumns($criteria);
				if (!isset($this->lastHBackendMenuRelatedByParentIdCriteria) || !$this->lastHBackendMenuRelatedByParentIdCriteria->equals($criteria)) {
					$this->collHBackendMenusRelatedByParentId = HBackendMenuPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHBackendMenuRelatedByParentIdCriteria = $criteria;
		return $this->collHBackendMenusRelatedByParentId;
	}

	
	public function countHBackendMenusRelatedByParentId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HBackendMenuPeer::PARENT_ID, $this->getId());

		return HBackendMenuPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHBackendMenuRelatedByParentId(HBackendMenu $l)
	{
		$this->collHBackendMenusRelatedByParentId[] = $l;
		$l->setHBackendMenuRelatedByParentId($this);
	}

} 