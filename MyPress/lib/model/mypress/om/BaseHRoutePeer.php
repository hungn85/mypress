<?php


abstract class BaseHRoutePeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'h_route';

	
	const CLASS_DEFAULT = 'lib.model.mypress.HRoute';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'h_route.ID';

	
	const DESCRIPTION = 'h_route.DESCRIPTION';

	
	const NAME = 'h_route.NAME';

	
	const PARTTERN = 'h_route.PARTTERN';

	
	const REQUIREMENTS = 'h_route.REQUIREMENTS';

	
	const MODEL = 'h_route.MODEL';

	
	const MODELS = 'h_route.MODELS';

	
	const MODULE = 'h_route.MODULE';

	
	const ACTION = 'h_route.ACTION';

	
	const PARAMS = 'h_route.PARAMS';

	
	const PRIORITY = 'h_route.PRIORITY';

	
	const ADD_CACHE = 'h_route.ADD_CACHE';

	
	const SEO_TITLE = 'h_route.SEO_TITLE';

	
	const SEO_DESCRIPTION = 'h_route.SEO_DESCRIPTION';

	
	const SEO_OG_IMAGE = 'h_route.SEO_OG_IMAGE';

	
	const JSON = 'h_route.JSON';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Description', 'Name', 'Parttern', 'Requirements', 'Model', 'Models', 'Module', 'Action', 'Params', 'Priority', 'AddCache', 'SeoTitle', 'SeoDescription', 'SeoOgImage', 'Json', ),
		BasePeer::TYPE_COLNAME => array (HRoutePeer::ID, HRoutePeer::DESCRIPTION, HRoutePeer::NAME, HRoutePeer::PARTTERN, HRoutePeer::REQUIREMENTS, HRoutePeer::MODEL, HRoutePeer::MODELS, HRoutePeer::MODULE, HRoutePeer::ACTION, HRoutePeer::PARAMS, HRoutePeer::PRIORITY, HRoutePeer::ADD_CACHE, HRoutePeer::SEO_TITLE, HRoutePeer::SEO_DESCRIPTION, HRoutePeer::SEO_OG_IMAGE, HRoutePeer::JSON, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'description', 'name', 'parttern', 'requirements', 'model', 'models', 'module', 'action', 'params', 'priority', 'add_cache', 'seo_title', 'seo_description', 'seo_og_image', 'json', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Description' => 1, 'Name' => 2, 'Parttern' => 3, 'Requirements' => 4, 'Model' => 5, 'Models' => 6, 'Module' => 7, 'Action' => 8, 'Params' => 9, 'Priority' => 10, 'AddCache' => 11, 'SeoTitle' => 12, 'SeoDescription' => 13, 'SeoOgImage' => 14, 'Json' => 15, ),
		BasePeer::TYPE_COLNAME => array (HRoutePeer::ID => 0, HRoutePeer::DESCRIPTION => 1, HRoutePeer::NAME => 2, HRoutePeer::PARTTERN => 3, HRoutePeer::REQUIREMENTS => 4, HRoutePeer::MODEL => 5, HRoutePeer::MODELS => 6, HRoutePeer::MODULE => 7, HRoutePeer::ACTION => 8, HRoutePeer::PARAMS => 9, HRoutePeer::PRIORITY => 10, HRoutePeer::ADD_CACHE => 11, HRoutePeer::SEO_TITLE => 12, HRoutePeer::SEO_DESCRIPTION => 13, HRoutePeer::SEO_OG_IMAGE => 14, HRoutePeer::JSON => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'description' => 1, 'name' => 2, 'parttern' => 3, 'requirements' => 4, 'model' => 5, 'models' => 6, 'module' => 7, 'action' => 8, 'params' => 9, 'priority' => 10, 'add_cache' => 11, 'seo_title' => 12, 'seo_description' => 13, 'seo_og_image' => 14, 'json' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		return BasePeer::getMapBuilder('lib.model.mypress.map.HRouteMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HRoutePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HRoutePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HRoutePeer::ID);

		$criteria->addSelectColumn(HRoutePeer::DESCRIPTION);

		$criteria->addSelectColumn(HRoutePeer::NAME);

		$criteria->addSelectColumn(HRoutePeer::PARTTERN);

		$criteria->addSelectColumn(HRoutePeer::REQUIREMENTS);

		$criteria->addSelectColumn(HRoutePeer::MODEL);

		$criteria->addSelectColumn(HRoutePeer::MODELS);

		$criteria->addSelectColumn(HRoutePeer::MODULE);

		$criteria->addSelectColumn(HRoutePeer::ACTION);

		$criteria->addSelectColumn(HRoutePeer::PARAMS);

		$criteria->addSelectColumn(HRoutePeer::PRIORITY);

		$criteria->addSelectColumn(HRoutePeer::ADD_CACHE);

		$criteria->addSelectColumn(HRoutePeer::SEO_TITLE);

		$criteria->addSelectColumn(HRoutePeer::SEO_DESCRIPTION);

		$criteria->addSelectColumn(HRoutePeer::SEO_OG_IMAGE);

		$criteria->addSelectColumn(HRoutePeer::JSON);

	}

	const COUNT = 'COUNT(h_route.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT h_route.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HRoutePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HRoutePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HRoutePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HRoutePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HRoutePeer::populateObjects(HRoutePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HRoutePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HRoutePeer::getOMClass();
		$cls = sfPropel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

  static public function getUniqueColumnNames()
  {
    return array();
  }
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HRoutePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(HRoutePeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(HRoutePeer::ID);
			$selectCriteria->add(HRoutePeer::ID, $criteria->remove(HRoutePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HRoutePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HRoutePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HRoute) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(HRoutePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HRoute $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HRoutePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HRoutePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HRoutePeer::DATABASE_NAME, HRoutePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HRoutePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	static $static_object_pk = array();
	public static function retrieveByPK($pk, $con = null)
	{
		if(!isset(self::$static_object_pk[$pk])){
			if ($con === null) {
				$con = Propel::getConnection(self::DATABASE_NAME);
			}

			$criteria = new Criteria(HRoutePeer::DATABASE_NAME);
	
			$criteria->add(HRoutePeer::ID, $pk);
	

			$v = HRoutePeer::doSelect($criteria, $con);
			self::$static_object_pk[$pk] = $v;
		}
		return !empty(self::$static_object_pk[$pk]) > 0 ? self::$static_object_pk[$pk][0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(HRoutePeer::ID, $pks, Criteria::IN);
			$objs = HRoutePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHRoutePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			Propel::registerMapBuilder('lib.model.mypress.map.HRouteMapBuilder');
}
