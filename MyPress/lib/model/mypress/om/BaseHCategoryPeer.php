<?php


abstract class BaseHCategoryPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'h_category';

	
	const CLASS_DEFAULT = 'lib.model.mypress.HCategory';

	
	const NUM_COLUMNS = 20;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'h_category.ID';

	
	const PARENT_ID = 'h_category.PARENT_ID';

	
	const NAME = 'h_category.NAME';

	
	const NAME_EN = 'h_category.NAME_EN';

	
	const LINK = 'h_category.LINK';

	
	const LINK_EN = 'h_category.LINK_EN';

	
	const PRIORITY = 'h_category.PRIORITY';

	
	const HIDE = 'h_category.HIDE';

	
	const JSON = 'h_category.JSON';

	
	const CREATED_AT = 'h_category.CREATED_AT';

	
	const UPDATED_AT = 'h_category.UPDATED_AT';

	
	const SEO_ROUTE_ID = 'h_category.SEO_ROUTE_ID';

	
	const SEO_EN_ROUTE_ID = 'h_category.SEO_EN_ROUTE_ID';

	
	const SEO_SLUG = 'h_category.SEO_SLUG';

	
	const SEO_EN_SLUG = 'h_category.SEO_EN_SLUG';

	
	const SEO_TITLE = 'h_category.SEO_TITLE';

	
	const SEO_EN_TITLE = 'h_category.SEO_EN_TITLE';

	
	const SEO_DESCRIPTION = 'h_category.SEO_DESCRIPTION';

	
	const SEO_EN_DESCRIPTION = 'h_category.SEO_EN_DESCRIPTION';

	
	const SEO_OG_IMAGE = 'h_category.SEO_OG_IMAGE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'ParentId', 'Name', 'NameEn', 'Link', 'LinkEn', 'Priority', 'Hide', 'Json', 'CreatedAt', 'UpdatedAt', 'SeoRouteId', 'SeoEnRouteId', 'SeoSlug', 'SeoEnSlug', 'SeoTitle', 'SeoEnTitle', 'SeoDescription', 'SeoEnDescription', 'SeoOgImage', ),
		BasePeer::TYPE_COLNAME => array (HCategoryPeer::ID, HCategoryPeer::PARENT_ID, HCategoryPeer::NAME, HCategoryPeer::NAME_EN, HCategoryPeer::LINK, HCategoryPeer::LINK_EN, HCategoryPeer::PRIORITY, HCategoryPeer::HIDE, HCategoryPeer::JSON, HCategoryPeer::CREATED_AT, HCategoryPeer::UPDATED_AT, HCategoryPeer::SEO_ROUTE_ID, HCategoryPeer::SEO_EN_ROUTE_ID, HCategoryPeer::SEO_SLUG, HCategoryPeer::SEO_EN_SLUG, HCategoryPeer::SEO_TITLE, HCategoryPeer::SEO_EN_TITLE, HCategoryPeer::SEO_DESCRIPTION, HCategoryPeer::SEO_EN_DESCRIPTION, HCategoryPeer::SEO_OG_IMAGE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'parent_id', 'name', 'name_en', 'link', 'link_en', 'priority', 'hide', 'json', 'created_at', 'updated_at', 'seo_route_id', 'seo_en_route_id', 'seo_slug', 'seo_en_slug', 'seo_title', 'seo_en_title', 'seo_description', 'seo_en_description', 'seo_og_image', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ParentId' => 1, 'Name' => 2, 'NameEn' => 3, 'Link' => 4, 'LinkEn' => 5, 'Priority' => 6, 'Hide' => 7, 'Json' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, 'SeoRouteId' => 11, 'SeoEnRouteId' => 12, 'SeoSlug' => 13, 'SeoEnSlug' => 14, 'SeoTitle' => 15, 'SeoEnTitle' => 16, 'SeoDescription' => 17, 'SeoEnDescription' => 18, 'SeoOgImage' => 19, ),
		BasePeer::TYPE_COLNAME => array (HCategoryPeer::ID => 0, HCategoryPeer::PARENT_ID => 1, HCategoryPeer::NAME => 2, HCategoryPeer::NAME_EN => 3, HCategoryPeer::LINK => 4, HCategoryPeer::LINK_EN => 5, HCategoryPeer::PRIORITY => 6, HCategoryPeer::HIDE => 7, HCategoryPeer::JSON => 8, HCategoryPeer::CREATED_AT => 9, HCategoryPeer::UPDATED_AT => 10, HCategoryPeer::SEO_ROUTE_ID => 11, HCategoryPeer::SEO_EN_ROUTE_ID => 12, HCategoryPeer::SEO_SLUG => 13, HCategoryPeer::SEO_EN_SLUG => 14, HCategoryPeer::SEO_TITLE => 15, HCategoryPeer::SEO_EN_TITLE => 16, HCategoryPeer::SEO_DESCRIPTION => 17, HCategoryPeer::SEO_EN_DESCRIPTION => 18, HCategoryPeer::SEO_OG_IMAGE => 19, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'parent_id' => 1, 'name' => 2, 'name_en' => 3, 'link' => 4, 'link_en' => 5, 'priority' => 6, 'hide' => 7, 'json' => 8, 'created_at' => 9, 'updated_at' => 10, 'seo_route_id' => 11, 'seo_en_route_id' => 12, 'seo_slug' => 13, 'seo_en_slug' => 14, 'seo_title' => 15, 'seo_en_title' => 16, 'seo_description' => 17, 'seo_en_description' => 18, 'seo_og_image' => 19, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	public static function getMapBuilder()
	{
		return BasePeer::getMapBuilder('lib.model.mypress.map.HCategoryMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HCategoryPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HCategoryPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HCategoryPeer::ID);

		$criteria->addSelectColumn(HCategoryPeer::PARENT_ID);

		$criteria->addSelectColumn(HCategoryPeer::NAME);

		$criteria->addSelectColumn(HCategoryPeer::NAME_EN);

		$criteria->addSelectColumn(HCategoryPeer::LINK);

		$criteria->addSelectColumn(HCategoryPeer::LINK_EN);

		$criteria->addSelectColumn(HCategoryPeer::PRIORITY);

		$criteria->addSelectColumn(HCategoryPeer::HIDE);

		$criteria->addSelectColumn(HCategoryPeer::JSON);

		$criteria->addSelectColumn(HCategoryPeer::CREATED_AT);

		$criteria->addSelectColumn(HCategoryPeer::UPDATED_AT);

		$criteria->addSelectColumn(HCategoryPeer::SEO_ROUTE_ID);

		$criteria->addSelectColumn(HCategoryPeer::SEO_EN_ROUTE_ID);

		$criteria->addSelectColumn(HCategoryPeer::SEO_SLUG);

		$criteria->addSelectColumn(HCategoryPeer::SEO_EN_SLUG);

		$criteria->addSelectColumn(HCategoryPeer::SEO_TITLE);

		$criteria->addSelectColumn(HCategoryPeer::SEO_EN_TITLE);

		$criteria->addSelectColumn(HCategoryPeer::SEO_DESCRIPTION);

		$criteria->addSelectColumn(HCategoryPeer::SEO_EN_DESCRIPTION);

		$criteria->addSelectColumn(HCategoryPeer::SEO_OG_IMAGE);

	}

	const COUNT = 'COUNT(h_category.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT h_category.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HCategoryPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HCategoryPeer::populateObjects(HCategoryPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HCategoryPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HCategoryPeer::getOMClass();
		$cls = sfPropel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinHRouteRelatedBySeoRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinHRouteRelatedBySeoEnRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinHRouteRelatedBySeoRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		HRoutePeer::addSelectColumns($c);

		$c->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = HRoutePeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addHCategoryRelatedBySeoRouteId($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initHCategorysRelatedBySeoRouteId();
				$obj2->addHCategoryRelatedBySeoRouteId($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinHRouteRelatedBySeoEnRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		HRoutePeer::addSelectColumns($c);

		$c->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = HRoutePeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoEnRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addHCategoryRelatedBySeoEnRouteId($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initHCategorysRelatedBySeoEnRouteId();
				$obj2->addHCategoryRelatedBySeoEnRouteId($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$criteria->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol2 = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		HRoutePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + HRoutePeer::NUM_COLUMNS;

		HRoutePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + HRoutePeer::NUM_COLUMNS;

		$c->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$c->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addHCategoryRelatedBySeoRouteId($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initHCategorysRelatedBySeoRouteId();
				$obj2->addHCategoryRelatedBySeoRouteId($obj1);
			}


					
			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getHRouteRelatedBySeoEnRouteId(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addHCategoryRelatedBySeoEnRouteId($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initHCategorysRelatedBySeoEnRouteId();
				$obj3->addHCategoryRelatedBySeoEnRouteId($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptHCategoryRelatedByParentId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$criteria->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptHRouteRelatedBySeoRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptHRouteRelatedBySeoEnRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HCategoryPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HCategoryPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HCategoryPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptHCategoryRelatedByParentId(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol2 = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		HRoutePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + HRoutePeer::NUM_COLUMNS;

		HRoutePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + HRoutePeer::NUM_COLUMNS;

		$c->addJoin(HCategoryPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$c->addJoin(HCategoryPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addHCategoryRelatedBySeoRouteId($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initHCategorysRelatedBySeoRouteId();
				$obj2->addHCategoryRelatedBySeoRouteId($obj1);
			}

			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getHRouteRelatedBySeoEnRouteId(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addHCategoryRelatedBySeoEnRouteId($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initHCategorysRelatedBySeoEnRouteId();
				$obj3->addHCategoryRelatedBySeoEnRouteId($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptHRouteRelatedBySeoRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol2 = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptHRouteRelatedBySeoEnRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HCategoryPeer::addSelectColumns($c);
		$startcol2 = (HCategoryPeer::NUM_COLUMNS - HCategoryPeer::NUM_LAZY_LOAD_COLUMNS) + 1;


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HCategoryPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$results[] = $obj1;
		}
		return $results;
	}


  static public function getUniqueColumnNames()
  {
    return array();
  }
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HCategoryPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(HCategoryPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(HCategoryPeer::ID);
			$selectCriteria->add(HCategoryPeer::ID, $criteria->remove(HCategoryPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HCategoryPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HCategoryPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HCategory) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(HCategoryPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HCategory $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HCategoryPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HCategoryPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HCategoryPeer::DATABASE_NAME, HCategoryPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HCategoryPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	static $static_object_pk = array();
	public static function retrieveByPK($pk, $con = null)
	{
		if(!isset(self::$static_object_pk[$pk])){
			if ($con === null) {
				$con = Propel::getConnection(self::DATABASE_NAME);
			}

			$criteria = new Criteria(HCategoryPeer::DATABASE_NAME);
	
			$criteria->add(HCategoryPeer::ID, $pk);
	

			$v = HCategoryPeer::doSelect($criteria, $con);
			self::$static_object_pk[$pk] = $v;
		}
		return !empty(self::$static_object_pk[$pk]) > 0 ? self::$static_object_pk[$pk][0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(HCategoryPeer::ID, $pks, Criteria::IN);
			$objs = HCategoryPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHCategoryPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			Propel::registerMapBuilder('lib.model.mypress.map.HCategoryMapBuilder');
}
