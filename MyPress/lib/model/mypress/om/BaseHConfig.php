<?php


abstract class BaseHConfig extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $code;


	
	protected $val;


	
	protected $val_en;


	
	protected $from_time;


	
	protected $to_time;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getCode()
	{

        return $this->code;
	}

	
	public function getVal()
	{

        return $this->val;
	}

	
	public function getValEn()
	{

        return $this->val_en;
	}

	
	public function getFromTime($format = 'Y-m-d H:i:s')
	{

		if ($this->from_time === null || $this->from_time === '') {
			return null;
		} elseif (!is_int($this->from_time)) {
						$ts = strtotime($this->from_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [from_time] as date/time value: " . var_export($this->from_time, true));
			}
		} else {
			$ts = $this->from_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getToTime($format = 'Y-m-d H:i:s')
	{

		if ($this->to_time === null || $this->to_time === '') {
			return null;
		} elseif (!is_int($this->to_time)) {
						$ts = strtotime($this->to_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [to_time] as date/time value: " . var_export($this->to_time, true));
			}
		} else {
			$ts = $this->to_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HConfigPeer::ID;
		}

	} 
	
	public function setCode($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->code !== $v) {
			$this->code = $v;
			$this->modifiedColumns[] = HConfigPeer::CODE;
		}

	} 
	
	public function setVal($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->val !== $v) {
			$this->val = $v;
			$this->modifiedColumns[] = HConfigPeer::VAL;
		}

	} 
	
	public function setValEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->val_en !== $v) {
			$this->val_en = $v;
			$this->modifiedColumns[] = HConfigPeer::VAL_EN;
		}

	} 
	
	public function setFromTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [from_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->from_time !== $ts) {
			$this->from_time = $ts;
			$this->modifiedColumns[] = HConfigPeer::FROM_TIME;
		}

	} 
	
	public function setToTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [to_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->to_time !== $ts) {
			$this->to_time = $ts;
			$this->modifiedColumns[] = HConfigPeer::TO_TIME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->code = $rs->getString($startcol + 1);

			$this->val = $rs->getString($startcol + 2);

			$this->val_en = $rs->getString($startcol + 3);

			$this->from_time = $rs->getTimestamp($startcol + 4, null);

			$this->to_time = $rs->getTimestamp($startcol + 5, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HConfig object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HConfigPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HConfigPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HConfigPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HConfigPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HConfigPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HConfigPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HConfigPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getCode();
				break;
			case 2:
				return $this->getVal();
				break;
			case 3:
				return $this->getValEn();
				break;
			case 4:
				return $this->getFromTime();
				break;
			case 5:
				return $this->getToTime();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HConfigPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getCode(),
			$keys[2] => $this->getVal(),
			$keys[3] => $this->getValEn(),
			$keys[4] => $this->getFromTime(),
			$keys[5] => $this->getToTime(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HConfigPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setCode($value);
				break;
			case 2:
				$this->setVal($value);
				break;
			case 3:
				$this->setValEn($value);
				break;
			case 4:
				$this->setFromTime($value);
				break;
			case 5:
				$this->setToTime($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HConfigPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setVal($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setValEn($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setFromTime($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setToTime($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HConfigPeer::DATABASE_NAME);

		if ($this->isColumnModified(HConfigPeer::ID)) $criteria->add(HConfigPeer::ID, $this->id);
		if ($this->isColumnModified(HConfigPeer::CODE)) $criteria->add(HConfigPeer::CODE, $this->code);
		if ($this->isColumnModified(HConfigPeer::VAL)) $criteria->add(HConfigPeer::VAL, $this->val);
		if ($this->isColumnModified(HConfigPeer::VAL_EN)) $criteria->add(HConfigPeer::VAL_EN, $this->val_en);
		if ($this->isColumnModified(HConfigPeer::FROM_TIME)) $criteria->add(HConfigPeer::FROM_TIME, $this->from_time);
		if ($this->isColumnModified(HConfigPeer::TO_TIME)) $criteria->add(HConfigPeer::TO_TIME, $this->to_time);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HConfigPeer::DATABASE_NAME);

		$criteria->add(HConfigPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCode($this->code);

		$copyObj->setVal($this->val);

		$copyObj->setValEn($this->val_en);

		$copyObj->setFromTime($this->from_time);

		$copyObj->setToTime($this->to_time);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HConfigPeer();
		}
		return self::$peer;
	}

} 