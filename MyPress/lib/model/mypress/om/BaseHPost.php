<?php


abstract class BaseHPost extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $name;


	
	protected $name_en;


	
	protected $img;


	
	protected $description;


	
	protected $description_en;


	
	protected $detail;


	
	protected $detail_en;


	
	protected $priority = 10;


	
	protected $hide = false;


	
	protected $json;


	
	protected $view_count = 125;


	
	protected $real_view_count = 0;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $seo_route_id;


	
	protected $seo_en_route_id;


	
	protected $seo_slug;


	
	protected $seo_en_slug;


	
	protected $seo_title;


	
	protected $seo_en_title;


	
	protected $seo_description;


	
	protected $seo_en_description;


	
	protected $seo_og_image;

	
	protected $aHRouteRelatedBySeoRouteId;

	
	protected $aHRouteRelatedBySeoEnRouteId;

	
	protected $collHCategoryPosts;

	
	protected $lastHCategoryPostCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getName()
	{

        return $this->name;
	}

	
	public function getNameEn()
	{

        return $this->name_en;
	}

	
	public function getImg()
	{

        return $this->img;
	}

	
	public function getDescription()
	{

        return $this->description;
	}

	
	public function getDescriptionEn()
	{

        return $this->description_en;
	}

	
	public function getDetail()
	{

        return $this->detail;
	}

	
	public function getDetailEn()
	{

        return $this->detail_en;
	}

	
	public function getPriority()
	{

        return $this->priority;
	}

	
	public function getHide()
	{

        return $this->hide;
	}

	
	public function getJson()
	{

        $ret = @json_decode($this->json, true);
         if(!$ret) {
           $ret = array();
         }
         return $ret;
        
	}

	
	public function getViewCount()
	{

        return $this->view_count;
	}

	
	public function getRealViewCount()
	{

        return $this->real_view_count;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSeoRouteId()
	{

        return $this->seo_route_id;
	}

	
	public function getSeoEnRouteId()
	{

        return $this->seo_en_route_id;
	}

	
	public function getSeoSlug()
	{

        return $this->seo_slug;
	}

	
	public function getSeoEnSlug()
	{

        return $this->seo_en_slug;
	}

	
	public function getSeoTitle()
	{

        return $this->seo_title;
	}

	
	public function getSeoEnTitle()
	{

        return $this->seo_en_title;
	}

	
	public function getSeoDescription()
	{

        return $this->seo_description;
	}

	
	public function getSeoEnDescription()
	{

        return $this->seo_en_description;
	}

	
	public function getSeoOgImage()
	{

        return $this->seo_og_image;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HPostPeer::ID;
		}

	} 
	
	public function setName($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = HPostPeer::NAME;
		}

	} 
	
	public function setNameEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name_en !== $v) {
			$this->name_en = $v;
			$this->modifiedColumns[] = HPostPeer::NAME_EN;
		}

	} 
	
	public function setImg($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->img !== $v) {
			$this->img = $v;
			$this->modifiedColumns[] = HPostPeer::IMG;
		}

	} 
	
	public function setDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->description !== $v) {
			$this->description = $v;
			$this->modifiedColumns[] = HPostPeer::DESCRIPTION;
		}

	} 
	
	public function setDescriptionEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->description_en !== $v) {
			$this->description_en = $v;
			$this->modifiedColumns[] = HPostPeer::DESCRIPTION_EN;
		}

	} 
	
	public function setDetail($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->detail !== $v) {
			$this->detail = $v;
			$this->modifiedColumns[] = HPostPeer::DETAIL;
		}

	} 
	
	public function setDetailEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->detail_en !== $v) {
			$this->detail_en = $v;
			$this->modifiedColumns[] = HPostPeer::DETAIL_EN;
		}

	} 
	
	public function setPriority($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->priority !== $v || $v === 10) {
			$this->priority = $v;
			$this->modifiedColumns[] = HPostPeer::PRIORITY;
		}

	} 
	
	public function setHide($v)
	{

		if ($this->hide !== $v || $v === false) {
			$this->hide = $v;
			$this->modifiedColumns[] = HPostPeer::HIDE;
		}

	} 
	
	public function setJson($v)
	{

        $v = json_encode($v);
        
        
		if ($this->json !== $v) {
			$this->json = $v;
			$this->modifiedColumns[] = HPostPeer::JSON;
		}

	} 
	
	public function setViewCount($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->view_count !== $v || $v === 125) {
			$this->view_count = $v;
			$this->modifiedColumns[] = HPostPeer::VIEW_COUNT;
		}

	} 
	
	public function setRealViewCount($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->real_view_count !== $v || $v === 0) {
			$this->real_view_count = $v;
			$this->modifiedColumns[] = HPostPeer::REAL_VIEW_COUNT;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = HPostPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = HPostPeer::UPDATED_AT;
		}

	} 
	
	public function setSeoRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_route_id !== $v) {
			$this->seo_route_id = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoRouteId !== null && $this->aHRouteRelatedBySeoRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoRouteId = null;
		}

	} 
	
	public function setSeoEnRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_en_route_id !== $v) {
			$this->seo_en_route_id = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_EN_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoEnRouteId !== null && $this->aHRouteRelatedBySeoEnRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoEnRouteId = null;
		}

	} 
	
	public function setSeoSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_slug !== $v) {
			$this->seo_slug = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_SLUG;
		}

	} 
	
	public function setSeoEnSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_slug !== $v) {
			$this->seo_en_slug = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_EN_SLUG;
		}

	} 
	
	public function setSeoTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_title !== $v) {
			$this->seo_title = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_TITLE;
		}

	} 
	
	public function setSeoEnTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_title !== $v) {
			$this->seo_en_title = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_EN_TITLE;
		}

	} 
	
	public function setSeoDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_description !== $v) {
			$this->seo_description = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_DESCRIPTION;
		}

	} 
	
	public function setSeoEnDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_description !== $v) {
			$this->seo_en_description = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_EN_DESCRIPTION;
		}

	} 
	
	public function setSeoOgImage($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_og_image !== $v) {
			$this->seo_og_image = $v;
			$this->modifiedColumns[] = HPostPeer::SEO_OG_IMAGE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->name = $rs->getString($startcol + 1);

			$this->name_en = $rs->getString($startcol + 2);

			$this->img = $rs->getString($startcol + 3);

			$this->description = $rs->getString($startcol + 4);

			$this->description_en = $rs->getString($startcol + 5);

			$this->detail = $rs->getString($startcol + 6);

			$this->detail_en = $rs->getString($startcol + 7);

			$this->priority = $rs->getInt($startcol + 8);

			$this->hide = $rs->getBoolean($startcol + 9);

			$this->json = $rs->getString($startcol + 10);

			$this->view_count = $rs->getInt($startcol + 11);

			$this->real_view_count = $rs->getInt($startcol + 12);

			$this->created_at = $rs->getTimestamp($startcol + 13, null);

			$this->updated_at = $rs->getTimestamp($startcol + 14, null);

			$this->seo_route_id = $rs->getInt($startcol + 15);

			$this->seo_en_route_id = $rs->getInt($startcol + 16);

			$this->seo_slug = $rs->getString($startcol + 17);

			$this->seo_en_slug = $rs->getString($startcol + 18);

			$this->seo_title = $rs->getString($startcol + 19);

			$this->seo_en_title = $rs->getString($startcol + 20);

			$this->seo_description = $rs->getString($startcol + 21);

			$this->seo_en_description = $rs->getString($startcol + 22);

			$this->seo_og_image = $rs->getString($startcol + 23);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 24; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HPost object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HPostPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HPostPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(HPostPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(HPostPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HPostPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if ($this->aHRouteRelatedBySeoRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoRouteId($this->aHRouteRelatedBySeoRouteId);
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if ($this->aHRouteRelatedBySeoEnRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoEnRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoEnRouteId($this->aHRouteRelatedBySeoEnRouteId);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HPostPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HPostPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collHCategoryPosts !== null) {
				foreach($this->collHCategoryPosts as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoRouteId->getValidationFailures());
				}
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoEnRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoEnRouteId->getValidationFailures());
				}
			}


			if (($retval = HPostPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collHCategoryPosts !== null) {
					foreach($this->collHCategoryPosts as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HPostPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getName();
				break;
			case 2:
				return $this->getNameEn();
				break;
			case 3:
				return $this->getImg();
				break;
			case 4:
				return $this->getDescription();
				break;
			case 5:
				return $this->getDescriptionEn();
				break;
			case 6:
				return $this->getDetail();
				break;
			case 7:
				return $this->getDetailEn();
				break;
			case 8:
				return $this->getPriority();
				break;
			case 9:
				return $this->getHide();
				break;
			case 10:
				return $this->getJson();
				break;
			case 11:
				return $this->getViewCount();
				break;
			case 12:
				return $this->getRealViewCount();
				break;
			case 13:
				return $this->getCreatedAt();
				break;
			case 14:
				return $this->getUpdatedAt();
				break;
			case 15:
				return $this->getSeoRouteId();
				break;
			case 16:
				return $this->getSeoEnRouteId();
				break;
			case 17:
				return $this->getSeoSlug();
				break;
			case 18:
				return $this->getSeoEnSlug();
				break;
			case 19:
				return $this->getSeoTitle();
				break;
			case 20:
				return $this->getSeoEnTitle();
				break;
			case 21:
				return $this->getSeoDescription();
				break;
			case 22:
				return $this->getSeoEnDescription();
				break;
			case 23:
				return $this->getSeoOgImage();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HPostPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getName(),
			$keys[2] => $this->getNameEn(),
			$keys[3] => $this->getImg(),
			$keys[4] => $this->getDescription(),
			$keys[5] => $this->getDescriptionEn(),
			$keys[6] => $this->getDetail(),
			$keys[7] => $this->getDetailEn(),
			$keys[8] => $this->getPriority(),
			$keys[9] => $this->getHide(),
			$keys[10] => $this->getJson(),
			$keys[11] => $this->getViewCount(),
			$keys[12] => $this->getRealViewCount(),
			$keys[13] => $this->getCreatedAt(),
			$keys[14] => $this->getUpdatedAt(),
			$keys[15] => $this->getSeoRouteId(),
			$keys[16] => $this->getSeoEnRouteId(),
			$keys[17] => $this->getSeoSlug(),
			$keys[18] => $this->getSeoEnSlug(),
			$keys[19] => $this->getSeoTitle(),
			$keys[20] => $this->getSeoEnTitle(),
			$keys[21] => $this->getSeoDescription(),
			$keys[22] => $this->getSeoEnDescription(),
			$keys[23] => $this->getSeoOgImage(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HPostPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setName($value);
				break;
			case 2:
				$this->setNameEn($value);
				break;
			case 3:
				$this->setImg($value);
				break;
			case 4:
				$this->setDescription($value);
				break;
			case 5:
				$this->setDescriptionEn($value);
				break;
			case 6:
				$this->setDetail($value);
				break;
			case 7:
				$this->setDetailEn($value);
				break;
			case 8:
				$this->setPriority($value);
				break;
			case 9:
				$this->setHide($value);
				break;
			case 10:
				$this->setJson($value);
				break;
			case 11:
				$this->setViewCount($value);
				break;
			case 12:
				$this->setRealViewCount($value);
				break;
			case 13:
				$this->setCreatedAt($value);
				break;
			case 14:
				$this->setUpdatedAt($value);
				break;
			case 15:
				$this->setSeoRouteId($value);
				break;
			case 16:
				$this->setSeoEnRouteId($value);
				break;
			case 17:
				$this->setSeoSlug($value);
				break;
			case 18:
				$this->setSeoEnSlug($value);
				break;
			case 19:
				$this->setSeoTitle($value);
				break;
			case 20:
				$this->setSeoEnTitle($value);
				break;
			case 21:
				$this->setSeoDescription($value);
				break;
			case 22:
				$this->setSeoEnDescription($value);
				break;
			case 23:
				$this->setSeoOgImage($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HPostPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNameEn($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setImg($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDescription($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDescriptionEn($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDetail($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setDetailEn($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPriority($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setHide($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setJson($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setViewCount($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setRealViewCount($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setCreatedAt($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setUpdatedAt($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSeoRouteId($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSeoEnRouteId($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setSeoSlug($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setSeoEnSlug($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setSeoTitle($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setSeoEnTitle($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setSeoDescription($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setSeoEnDescription($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setSeoOgImage($arr[$keys[23]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HPostPeer::DATABASE_NAME);

		if ($this->isColumnModified(HPostPeer::ID)) $criteria->add(HPostPeer::ID, $this->id);
		if ($this->isColumnModified(HPostPeer::NAME)) $criteria->add(HPostPeer::NAME, $this->name);
		if ($this->isColumnModified(HPostPeer::NAME_EN)) $criteria->add(HPostPeer::NAME_EN, $this->name_en);
		if ($this->isColumnModified(HPostPeer::IMG)) $criteria->add(HPostPeer::IMG, $this->img);
		if ($this->isColumnModified(HPostPeer::DESCRIPTION)) $criteria->add(HPostPeer::DESCRIPTION, $this->description);
		if ($this->isColumnModified(HPostPeer::DESCRIPTION_EN)) $criteria->add(HPostPeer::DESCRIPTION_EN, $this->description_en);
		if ($this->isColumnModified(HPostPeer::DETAIL)) $criteria->add(HPostPeer::DETAIL, $this->detail);
		if ($this->isColumnModified(HPostPeer::DETAIL_EN)) $criteria->add(HPostPeer::DETAIL_EN, $this->detail_en);
		if ($this->isColumnModified(HPostPeer::PRIORITY)) $criteria->add(HPostPeer::PRIORITY, $this->priority);
		if ($this->isColumnModified(HPostPeer::HIDE)) $criteria->add(HPostPeer::HIDE, $this->hide);
		if ($this->isColumnModified(HPostPeer::JSON)) $criteria->add(HPostPeer::JSON, $this->json);
		if ($this->isColumnModified(HPostPeer::VIEW_COUNT)) $criteria->add(HPostPeer::VIEW_COUNT, $this->view_count);
		if ($this->isColumnModified(HPostPeer::REAL_VIEW_COUNT)) $criteria->add(HPostPeer::REAL_VIEW_COUNT, $this->real_view_count);
		if ($this->isColumnModified(HPostPeer::CREATED_AT)) $criteria->add(HPostPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(HPostPeer::UPDATED_AT)) $criteria->add(HPostPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(HPostPeer::SEO_ROUTE_ID)) $criteria->add(HPostPeer::SEO_ROUTE_ID, $this->seo_route_id);
		if ($this->isColumnModified(HPostPeer::SEO_EN_ROUTE_ID)) $criteria->add(HPostPeer::SEO_EN_ROUTE_ID, $this->seo_en_route_id);
		if ($this->isColumnModified(HPostPeer::SEO_SLUG)) $criteria->add(HPostPeer::SEO_SLUG, $this->seo_slug);
		if ($this->isColumnModified(HPostPeer::SEO_EN_SLUG)) $criteria->add(HPostPeer::SEO_EN_SLUG, $this->seo_en_slug);
		if ($this->isColumnModified(HPostPeer::SEO_TITLE)) $criteria->add(HPostPeer::SEO_TITLE, $this->seo_title);
		if ($this->isColumnModified(HPostPeer::SEO_EN_TITLE)) $criteria->add(HPostPeer::SEO_EN_TITLE, $this->seo_en_title);
		if ($this->isColumnModified(HPostPeer::SEO_DESCRIPTION)) $criteria->add(HPostPeer::SEO_DESCRIPTION, $this->seo_description);
		if ($this->isColumnModified(HPostPeer::SEO_EN_DESCRIPTION)) $criteria->add(HPostPeer::SEO_EN_DESCRIPTION, $this->seo_en_description);
		if ($this->isColumnModified(HPostPeer::SEO_OG_IMAGE)) $criteria->add(HPostPeer::SEO_OG_IMAGE, $this->seo_og_image);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HPostPeer::DATABASE_NAME);

		$criteria->add(HPostPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setName($this->name);

		$copyObj->setNameEn($this->name_en);

		$copyObj->setImg($this->img);

		$copyObj->setDescription($this->description);

		$copyObj->setDescriptionEn($this->description_en);

		$copyObj->setDetail($this->detail);

		$copyObj->setDetailEn($this->detail_en);

		$copyObj->setPriority($this->priority);

		$copyObj->setHide($this->hide);

		$copyObj->setJson($this->json);

		$copyObj->setViewCount($this->view_count);

		$copyObj->setRealViewCount($this->real_view_count);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setSeoRouteId($this->seo_route_id);

		$copyObj->setSeoEnRouteId($this->seo_en_route_id);

		$copyObj->setSeoSlug($this->seo_slug);

		$copyObj->setSeoEnSlug($this->seo_en_slug);

		$copyObj->setSeoTitle($this->seo_title);

		$copyObj->setSeoEnTitle($this->seo_en_title);

		$copyObj->setSeoDescription($this->seo_description);

		$copyObj->setSeoEnDescription($this->seo_en_description);

		$copyObj->setSeoOgImage($this->seo_og_image);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getHCategoryPosts() as $relObj) {
				$copyObj->addHCategoryPost($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HPostPeer();
		}
		return self::$peer;
	}

	
	public function setHRouteRelatedBySeoRouteId($v)
	{


		if ($v === null) {
			$this->setSeoRouteId(NULL);
		} else {
			$this->setSeoRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoRouteId = $v;
	}


	
	static $HRouteRelatedBySeoRouteId = array();
	
	public function getHRouteRelatedBySeoRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoRouteId === null && ($this->seo_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoRouteId[$this->seo_route_id])){
				self::$HRouteRelatedBySeoRouteId[$this->seo_route_id] = HRoutePeer::retrieveByPK($this->seo_route_id, $con);
			}
			$this->aHRouteRelatedBySeoRouteId = self::$HRouteRelatedBySeoRouteId[$this->seo_route_id];

			
		}
		return $this->aHRouteRelatedBySeoRouteId;
	}

	
	public function setHRouteRelatedBySeoEnRouteId($v)
	{


		if ($v === null) {
			$this->setSeoEnRouteId(NULL);
		} else {
			$this->setSeoEnRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoEnRouteId = $v;
	}


	
	static $HRouteRelatedBySeoEnRouteId = array();
	
	public function getHRouteRelatedBySeoEnRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoEnRouteId === null && ($this->seo_en_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id])){
				self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id] = HRoutePeer::retrieveByPK($this->seo_en_route_id, $con);
			}
			$this->aHRouteRelatedBySeoEnRouteId = self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id];

			
		}
		return $this->aHRouteRelatedBySeoEnRouteId;
	}

	
	public function initHCategoryPosts()
	{
		if ($this->collHCategoryPosts === null) {
			$this->collHCategoryPosts = array();
		}
	}

	
	public function getHCategoryPosts($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategoryPosts === null) {
			if ($this->isNew()) {
			   $this->collHCategoryPosts = array();
			} else {

				$criteria->add(HCategoryPostPeer::POST_ID, $this->getId());

				HCategoryPostPeer::addSelectColumns($criteria);
				$this->collHCategoryPosts = HCategoryPostPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HCategoryPostPeer::POST_ID, $this->getId());

				HCategoryPostPeer::addSelectColumns($criteria);
				if (!isset($this->lastHCategoryPostCriteria) || !$this->lastHCategoryPostCriteria->equals($criteria)) {
					$this->collHCategoryPosts = HCategoryPostPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHCategoryPostCriteria = $criteria;
		return $this->collHCategoryPosts;
	}

	
	public function countHCategoryPosts($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HCategoryPostPeer::POST_ID, $this->getId());

		return HCategoryPostPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHCategoryPost(HCategoryPost $l)
	{
		$this->collHCategoryPosts[] = $l;
		$l->setHPost($this);
	}


	
	public function getHCategoryPostsJoinHCategory($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategoryPosts === null) {
			if ($this->isNew()) {
				$this->collHCategoryPosts = array();
			} else {

				$criteria->add(HCategoryPostPeer::POST_ID, $this->getId());

				$this->collHCategoryPosts = HCategoryPostPeer::doSelectJoinHCategory($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPostPeer::POST_ID, $this->getId());

			if (!isset($this->lastHCategoryPostCriteria) || !$this->lastHCategoryPostCriteria->equals($criteria)) {
				$this->collHCategoryPosts = HCategoryPostPeer::doSelectJoinHCategory($criteria, $con);
			}
		}
		$this->lastHCategoryPostCriteria = $criteria;

		return $this->collHCategoryPosts;
	}

} 