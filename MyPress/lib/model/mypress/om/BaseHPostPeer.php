<?php


abstract class BaseHPostPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'h_post';

	
	const CLASS_DEFAULT = 'lib.model.mypress.HPost';

	
	const NUM_COLUMNS = 24;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'h_post.ID';

	
	const NAME = 'h_post.NAME';

	
	const NAME_EN = 'h_post.NAME_EN';

	
	const IMG = 'h_post.IMG';

	
	const DESCRIPTION = 'h_post.DESCRIPTION';

	
	const DESCRIPTION_EN = 'h_post.DESCRIPTION_EN';

	
	const DETAIL = 'h_post.DETAIL';

	
	const DETAIL_EN = 'h_post.DETAIL_EN';

	
	const PRIORITY = 'h_post.PRIORITY';

	
	const HIDE = 'h_post.HIDE';

	
	const JSON = 'h_post.JSON';

	
	const VIEW_COUNT = 'h_post.VIEW_COUNT';

	
	const REAL_VIEW_COUNT = 'h_post.REAL_VIEW_COUNT';

	
	const CREATED_AT = 'h_post.CREATED_AT';

	
	const UPDATED_AT = 'h_post.UPDATED_AT';

	
	const SEO_ROUTE_ID = 'h_post.SEO_ROUTE_ID';

	
	const SEO_EN_ROUTE_ID = 'h_post.SEO_EN_ROUTE_ID';

	
	const SEO_SLUG = 'h_post.SEO_SLUG';

	
	const SEO_EN_SLUG = 'h_post.SEO_EN_SLUG';

	
	const SEO_TITLE = 'h_post.SEO_TITLE';

	
	const SEO_EN_TITLE = 'h_post.SEO_EN_TITLE';

	
	const SEO_DESCRIPTION = 'h_post.SEO_DESCRIPTION';

	
	const SEO_EN_DESCRIPTION = 'h_post.SEO_EN_DESCRIPTION';

	
	const SEO_OG_IMAGE = 'h_post.SEO_OG_IMAGE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Name', 'NameEn', 'Img', 'Description', 'DescriptionEn', 'Detail', 'DetailEn', 'Priority', 'Hide', 'Json', 'ViewCount', 'RealViewCount', 'CreatedAt', 'UpdatedAt', 'SeoRouteId', 'SeoEnRouteId', 'SeoSlug', 'SeoEnSlug', 'SeoTitle', 'SeoEnTitle', 'SeoDescription', 'SeoEnDescription', 'SeoOgImage', ),
		BasePeer::TYPE_COLNAME => array (HPostPeer::ID, HPostPeer::NAME, HPostPeer::NAME_EN, HPostPeer::IMG, HPostPeer::DESCRIPTION, HPostPeer::DESCRIPTION_EN, HPostPeer::DETAIL, HPostPeer::DETAIL_EN, HPostPeer::PRIORITY, HPostPeer::HIDE, HPostPeer::JSON, HPostPeer::VIEW_COUNT, HPostPeer::REAL_VIEW_COUNT, HPostPeer::CREATED_AT, HPostPeer::UPDATED_AT, HPostPeer::SEO_ROUTE_ID, HPostPeer::SEO_EN_ROUTE_ID, HPostPeer::SEO_SLUG, HPostPeer::SEO_EN_SLUG, HPostPeer::SEO_TITLE, HPostPeer::SEO_EN_TITLE, HPostPeer::SEO_DESCRIPTION, HPostPeer::SEO_EN_DESCRIPTION, HPostPeer::SEO_OG_IMAGE, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'name', 'name_en', 'img', 'description', 'description_en', 'detail', 'detail_en', 'priority', 'hide', 'json', 'view_count', 'real_view_count', 'created_at', 'updated_at', 'seo_route_id', 'seo_en_route_id', 'seo_slug', 'seo_en_slug', 'seo_title', 'seo_en_title', 'seo_description', 'seo_en_description', 'seo_og_image', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Name' => 1, 'NameEn' => 2, 'Img' => 3, 'Description' => 4, 'DescriptionEn' => 5, 'Detail' => 6, 'DetailEn' => 7, 'Priority' => 8, 'Hide' => 9, 'Json' => 10, 'ViewCount' => 11, 'RealViewCount' => 12, 'CreatedAt' => 13, 'UpdatedAt' => 14, 'SeoRouteId' => 15, 'SeoEnRouteId' => 16, 'SeoSlug' => 17, 'SeoEnSlug' => 18, 'SeoTitle' => 19, 'SeoEnTitle' => 20, 'SeoDescription' => 21, 'SeoEnDescription' => 22, 'SeoOgImage' => 23, ),
		BasePeer::TYPE_COLNAME => array (HPostPeer::ID => 0, HPostPeer::NAME => 1, HPostPeer::NAME_EN => 2, HPostPeer::IMG => 3, HPostPeer::DESCRIPTION => 4, HPostPeer::DESCRIPTION_EN => 5, HPostPeer::DETAIL => 6, HPostPeer::DETAIL_EN => 7, HPostPeer::PRIORITY => 8, HPostPeer::HIDE => 9, HPostPeer::JSON => 10, HPostPeer::VIEW_COUNT => 11, HPostPeer::REAL_VIEW_COUNT => 12, HPostPeer::CREATED_AT => 13, HPostPeer::UPDATED_AT => 14, HPostPeer::SEO_ROUTE_ID => 15, HPostPeer::SEO_EN_ROUTE_ID => 16, HPostPeer::SEO_SLUG => 17, HPostPeer::SEO_EN_SLUG => 18, HPostPeer::SEO_TITLE => 19, HPostPeer::SEO_EN_TITLE => 20, HPostPeer::SEO_DESCRIPTION => 21, HPostPeer::SEO_EN_DESCRIPTION => 22, HPostPeer::SEO_OG_IMAGE => 23, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'name' => 1, 'name_en' => 2, 'img' => 3, 'description' => 4, 'description_en' => 5, 'detail' => 6, 'detail_en' => 7, 'priority' => 8, 'hide' => 9, 'json' => 10, 'view_count' => 11, 'real_view_count' => 12, 'created_at' => 13, 'updated_at' => 14, 'seo_route_id' => 15, 'seo_en_route_id' => 16, 'seo_slug' => 17, 'seo_en_slug' => 18, 'seo_title' => 19, 'seo_en_title' => 20, 'seo_description' => 21, 'seo_en_description' => 22, 'seo_og_image' => 23, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	public static function getMapBuilder()
	{
		return BasePeer::getMapBuilder('lib.model.mypress.map.HPostMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HPostPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HPostPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HPostPeer::ID);

		$criteria->addSelectColumn(HPostPeer::NAME);

		$criteria->addSelectColumn(HPostPeer::NAME_EN);

		$criteria->addSelectColumn(HPostPeer::IMG);

		$criteria->addSelectColumn(HPostPeer::DESCRIPTION);

		$criteria->addSelectColumn(HPostPeer::DESCRIPTION_EN);

		$criteria->addSelectColumn(HPostPeer::DETAIL);

		$criteria->addSelectColumn(HPostPeer::DETAIL_EN);

		$criteria->addSelectColumn(HPostPeer::PRIORITY);

		$criteria->addSelectColumn(HPostPeer::HIDE);

		$criteria->addSelectColumn(HPostPeer::JSON);

		$criteria->addSelectColumn(HPostPeer::VIEW_COUNT);

		$criteria->addSelectColumn(HPostPeer::REAL_VIEW_COUNT);

		$criteria->addSelectColumn(HPostPeer::CREATED_AT);

		$criteria->addSelectColumn(HPostPeer::UPDATED_AT);

		$criteria->addSelectColumn(HPostPeer::SEO_ROUTE_ID);

		$criteria->addSelectColumn(HPostPeer::SEO_EN_ROUTE_ID);

		$criteria->addSelectColumn(HPostPeer::SEO_SLUG);

		$criteria->addSelectColumn(HPostPeer::SEO_EN_SLUG);

		$criteria->addSelectColumn(HPostPeer::SEO_TITLE);

		$criteria->addSelectColumn(HPostPeer::SEO_EN_TITLE);

		$criteria->addSelectColumn(HPostPeer::SEO_DESCRIPTION);

		$criteria->addSelectColumn(HPostPeer::SEO_EN_DESCRIPTION);

		$criteria->addSelectColumn(HPostPeer::SEO_OG_IMAGE);

	}

	const COUNT = 'COUNT(h_post.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT h_post.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HPostPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HPostPeer::populateObjects(HPostPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HPostPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HPostPeer::getOMClass();
		$cls = sfPropel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinHRouteRelatedBySeoRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HPostPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinHRouteRelatedBySeoEnRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HPostPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinHRouteRelatedBySeoRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HPostPeer::addSelectColumns($c);
		$startcol = (HPostPeer::NUM_COLUMNS - HPostPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		HRoutePeer::addSelectColumns($c);

		$c->addJoin(HPostPeer::SEO_ROUTE_ID, HRoutePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HPostPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = HRoutePeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addHPostRelatedBySeoRouteId($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initHPostsRelatedBySeoRouteId();
				$obj2->addHPostRelatedBySeoRouteId($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinHRouteRelatedBySeoEnRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HPostPeer::addSelectColumns($c);
		$startcol = (HPostPeer::NUM_COLUMNS - HPostPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		HRoutePeer::addSelectColumns($c);

		$c->addJoin(HPostPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HPostPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = HRoutePeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoEnRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addHPostRelatedBySeoEnRouteId($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initHPostsRelatedBySeoEnRouteId();
				$obj2->addHPostRelatedBySeoEnRouteId($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(HPostPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$criteria->addJoin(HPostPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HPostPeer::addSelectColumns($c);
		$startcol2 = (HPostPeer::NUM_COLUMNS - HPostPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		HRoutePeer::addSelectColumns($c);
		$startcol3 = $startcol2 + HRoutePeer::NUM_COLUMNS;

		HRoutePeer::addSelectColumns($c);
		$startcol4 = $startcol3 + HRoutePeer::NUM_COLUMNS;

		$c->addJoin(HPostPeer::SEO_ROUTE_ID, HRoutePeer::ID);

		$c->addJoin(HPostPeer::SEO_EN_ROUTE_ID, HRoutePeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HPostPeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getHRouteRelatedBySeoRouteId(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addHPostRelatedBySeoRouteId($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initHPostsRelatedBySeoRouteId();
				$obj2->addHPostRelatedBySeoRouteId($obj1);
			}


					
			$omClass = HRoutePeer::getOMClass();


			$cls = sfPropel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getHRouteRelatedBySeoEnRouteId(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addHPostRelatedBySeoEnRouteId($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initHPostsRelatedBySeoEnRouteId();
				$obj3->addHPostRelatedBySeoEnRouteId($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptHRouteRelatedBySeoRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptHRouteRelatedBySeoEnRouteId(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HPostPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HPostPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HPostPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptHRouteRelatedBySeoRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HPostPeer::addSelectColumns($c);
		$startcol2 = (HPostPeer::NUM_COLUMNS - HPostPeer::NUM_LAZY_LOAD_COLUMNS) + 1;


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HPostPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptHRouteRelatedBySeoEnRouteId(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		HPostPeer::addSelectColumns($c);
		$startcol2 = (HPostPeer::NUM_COLUMNS - HPostPeer::NUM_LAZY_LOAD_COLUMNS) + 1;


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = HPostPeer::getOMClass();

			$cls = sfPropel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$results[] = $obj1;
		}
		return $results;
	}


  static public function getUniqueColumnNames()
  {
    return array();
  }
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HPostPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(HPostPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(HPostPeer::ID);
			$selectCriteria->add(HPostPeer::ID, $criteria->remove(HPostPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HPostPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HPostPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HPost) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(HPostPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HPost $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HPostPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HPostPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HPostPeer::DATABASE_NAME, HPostPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HPostPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	static $static_object_pk = array();
	public static function retrieveByPK($pk, $con = null)
	{
		if(!isset(self::$static_object_pk[$pk])){
			if ($con === null) {
				$con = Propel::getConnection(self::DATABASE_NAME);
			}

			$criteria = new Criteria(HPostPeer::DATABASE_NAME);
	
			$criteria->add(HPostPeer::ID, $pk);
	

			$v = HPostPeer::doSelect($criteria, $con);
			self::$static_object_pk[$pk] = $v;
		}
		return !empty(self::$static_object_pk[$pk]) > 0 ? self::$static_object_pk[$pk][0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(HPostPeer::ID, $pks, Criteria::IN);
			$objs = HPostPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHPostPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			Propel::registerMapBuilder('lib.model.mypress.map.HPostMapBuilder');
}
