<?php


abstract class BaseHCategoryPost extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $post_id;


	
	protected $category_id;

	
	protected $aHPost;

	
	protected $aHCategory;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getPostId()
	{

        return $this->post_id;
	}

	
	public function getCategoryId()
	{

        return $this->category_id;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HCategoryPostPeer::ID;
		}

	} 
	
	public function setPostId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->post_id !== $v) {
			$this->post_id = $v;
			$this->modifiedColumns[] = HCategoryPostPeer::POST_ID;
		}

		if ($this->aHPost !== null && $this->aHPost->getId() !== $v) {
			$this->aHPost = null;
		}

	} 
	
	public function setCategoryId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->category_id !== $v) {
			$this->category_id = $v;
			$this->modifiedColumns[] = HCategoryPostPeer::CATEGORY_ID;
		}

		if ($this->aHCategory !== null && $this->aHCategory->getId() !== $v) {
			$this->aHCategory = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->post_id = $rs->getInt($startcol + 1);

			$this->category_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HCategoryPost object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HCategoryPostPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HCategoryPostPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HCategoryPostPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aHPost !== null) {
				if ($this->aHPost->isModified()) {
					$affectedRows += $this->aHPost->save($con);
				}
				$this->setHPost($this->aHPost);
			}

			if ($this->aHCategory !== null) {
				if ($this->aHCategory->isModified()) {
					$affectedRows += $this->aHCategory->save($con);
				}
				$this->setHCategory($this->aHCategory);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HCategoryPostPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HCategoryPostPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aHPost !== null) {
				if (!$this->aHPost->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHPost->getValidationFailures());
				}
			}

			if ($this->aHCategory !== null) {
				if (!$this->aHCategory->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHCategory->getValidationFailures());
				}
			}


			if (($retval = HCategoryPostPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HCategoryPostPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getPostId();
				break;
			case 2:
				return $this->getCategoryId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HCategoryPostPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getPostId(),
			$keys[2] => $this->getCategoryId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HCategoryPostPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setPostId($value);
				break;
			case 2:
				$this->setCategoryId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HCategoryPostPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPostId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCategoryId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HCategoryPostPeer::DATABASE_NAME);

		if ($this->isColumnModified(HCategoryPostPeer::ID)) $criteria->add(HCategoryPostPeer::ID, $this->id);
		if ($this->isColumnModified(HCategoryPostPeer::POST_ID)) $criteria->add(HCategoryPostPeer::POST_ID, $this->post_id);
		if ($this->isColumnModified(HCategoryPostPeer::CATEGORY_ID)) $criteria->add(HCategoryPostPeer::CATEGORY_ID, $this->category_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HCategoryPostPeer::DATABASE_NAME);

		$criteria->add(HCategoryPostPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPostId($this->post_id);

		$copyObj->setCategoryId($this->category_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HCategoryPostPeer();
		}
		return self::$peer;
	}

	
	public function setHPost($v)
	{


		if ($v === null) {
			$this->setPostId(NULL);
		} else {
			$this->setPostId($v->getId());
		}


		$this->aHPost = $v;
	}


	
	static $HPost = array();
	
	public function getHPost($con = null)
	{
		if ($this->aHPost === null && ($this->post_id !== null)) {
						if(!isset(self::$HPost[$this->post_id])){
				self::$HPost[$this->post_id] = HPostPeer::retrieveByPK($this->post_id, $con);
			}
			$this->aHPost = self::$HPost[$this->post_id];

			
		}
		return $this->aHPost;
	}

	
	public function setHCategory($v)
	{


		if ($v === null) {
			$this->setCategoryId(NULL);
		} else {
			$this->setCategoryId($v->getId());
		}


		$this->aHCategory = $v;
	}


	
	static $HCategory = array();
	
	public function getHCategory($con = null)
	{
		if ($this->aHCategory === null && ($this->category_id !== null)) {
						if(!isset(self::$HCategory[$this->category_id])){
				self::$HCategory[$this->category_id] = HCategoryPeer::retrieveByPK($this->category_id, $con);
			}
			$this->aHCategory = self::$HCategory[$this->category_id];

			
		}
		return $this->aHCategory;
	}

} 