<?php


abstract class BaseHRoute extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $description;


	
	protected $name;


	
	protected $parttern;


	
	protected $requirements;


	
	protected $model;


	
	protected $models;


	
	protected $module;


	
	protected $action;


	
	protected $params;


	
	protected $priority = 10;


	
	protected $add_cache;


	
	protected $seo_title;


	
	protected $seo_description;


	
	protected $seo_og_image;


	
	protected $json;

	
	protected $collHPagesRelatedBySeoRouteId;

	
	protected $lastHPageRelatedBySeoRouteIdCriteria = null;

	
	protected $collHPagesRelatedBySeoEnRouteId;

	
	protected $lastHPageRelatedBySeoEnRouteIdCriteria = null;

	
	protected $collHCategorysRelatedBySeoRouteId;

	
	protected $lastHCategoryRelatedBySeoRouteIdCriteria = null;

	
	protected $collHCategorysRelatedBySeoEnRouteId;

	
	protected $lastHCategoryRelatedBySeoEnRouteIdCriteria = null;

	
	protected $collHPostsRelatedBySeoRouteId;

	
	protected $lastHPostRelatedBySeoRouteIdCriteria = null;

	
	protected $collHPostsRelatedBySeoEnRouteId;

	
	protected $lastHPostRelatedBySeoEnRouteIdCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getDescription()
	{

        return $this->description;
	}

	
	public function getName()
	{

        return $this->name;
	}

	
	public function getParttern()
	{

        return $this->parttern;
	}

	
	public function getRequirements()
	{

        return $this->requirements;
	}

	
	public function getModel()
	{

        return $this->model;
	}

	
	public function getModels()
	{

        return $this->models;
	}

	
	public function getModule()
	{

        return $this->module;
	}

	
	public function getAction()
	{

        return $this->action;
	}

	
	public function getParams()
	{

        return $this->params;
	}

	
	public function getPriority()
	{

        return $this->priority;
	}

	
	public function getAddCache()
	{

        return $this->add_cache;
	}

	
	public function getSeoTitle()
	{

        return $this->seo_title;
	}

	
	public function getSeoDescription()
	{

        return $this->seo_description;
	}

	
	public function getSeoOgImage()
	{

        return $this->seo_og_image;
	}

	
	public function getJson()
	{

        $ret = @json_decode($this->json, true);
         if(!$ret) {
           $ret = array();
         }
         return $ret;
        
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HRoutePeer::ID;
		}

	} 
	
	public function setDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->description !== $v) {
			$this->description = $v;
			$this->modifiedColumns[] = HRoutePeer::DESCRIPTION;
		}

	} 
	
	public function setName($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = HRoutePeer::NAME;
		}

	} 
	
	public function setParttern($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->parttern !== $v) {
			$this->parttern = $v;
			$this->modifiedColumns[] = HRoutePeer::PARTTERN;
		}

	} 
	
	public function setRequirements($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->requirements !== $v) {
			$this->requirements = $v;
			$this->modifiedColumns[] = HRoutePeer::REQUIREMENTS;
		}

	} 
	
	public function setModel($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->model !== $v) {
			$this->model = $v;
			$this->modifiedColumns[] = HRoutePeer::MODEL;
		}

	} 
	
	public function setModels($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->models !== $v) {
			$this->models = $v;
			$this->modifiedColumns[] = HRoutePeer::MODELS;
		}

	} 
	
	public function setModule($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->module !== $v) {
			$this->module = $v;
			$this->modifiedColumns[] = HRoutePeer::MODULE;
		}

	} 
	
	public function setAction($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->action !== $v) {
			$this->action = $v;
			$this->modifiedColumns[] = HRoutePeer::ACTION;
		}

	} 
	
	public function setParams($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->params !== $v) {
			$this->params = $v;
			$this->modifiedColumns[] = HRoutePeer::PARAMS;
		}

	} 
	
	public function setPriority($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->priority !== $v || $v === 10) {
			$this->priority = $v;
			$this->modifiedColumns[] = HRoutePeer::PRIORITY;
		}

	} 
	
	public function setAddCache($v)
	{

		if ($this->add_cache !== $v) {
			$this->add_cache = $v;
			$this->modifiedColumns[] = HRoutePeer::ADD_CACHE;
		}

	} 
	
	public function setSeoTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_title !== $v) {
			$this->seo_title = $v;
			$this->modifiedColumns[] = HRoutePeer::SEO_TITLE;
		}

	} 
	
	public function setSeoDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_description !== $v) {
			$this->seo_description = $v;
			$this->modifiedColumns[] = HRoutePeer::SEO_DESCRIPTION;
		}

	} 
	
	public function setSeoOgImage($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_og_image !== $v) {
			$this->seo_og_image = $v;
			$this->modifiedColumns[] = HRoutePeer::SEO_OG_IMAGE;
		}

	} 
	
	public function setJson($v)
	{

        $v = json_encode($v);
        
        
		if ($this->json !== $v) {
			$this->json = $v;
			$this->modifiedColumns[] = HRoutePeer::JSON;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->description = $rs->getString($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->parttern = $rs->getString($startcol + 3);

			$this->requirements = $rs->getString($startcol + 4);

			$this->model = $rs->getString($startcol + 5);

			$this->models = $rs->getString($startcol + 6);

			$this->module = $rs->getString($startcol + 7);

			$this->action = $rs->getString($startcol + 8);

			$this->params = $rs->getString($startcol + 9);

			$this->priority = $rs->getInt($startcol + 10);

			$this->add_cache = $rs->getBoolean($startcol + 11);

			$this->seo_title = $rs->getString($startcol + 12);

			$this->seo_description = $rs->getString($startcol + 13);

			$this->seo_og_image = $rs->getString($startcol + 14);

			$this->json = $rs->getString($startcol + 15);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 16; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HRoute object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HRoutePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HRoutePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HRoutePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HRoutePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HRoutePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collHPagesRelatedBySeoRouteId !== null) {
				foreach($this->collHPagesRelatedBySeoRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHPagesRelatedBySeoEnRouteId !== null) {
				foreach($this->collHPagesRelatedBySeoEnRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHCategorysRelatedBySeoRouteId !== null) {
				foreach($this->collHCategorysRelatedBySeoRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHCategorysRelatedBySeoEnRouteId !== null) {
				foreach($this->collHCategorysRelatedBySeoEnRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHPostsRelatedBySeoRouteId !== null) {
				foreach($this->collHPostsRelatedBySeoRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collHPostsRelatedBySeoEnRouteId !== null) {
				foreach($this->collHPostsRelatedBySeoEnRouteId as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HRoutePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collHPagesRelatedBySeoRouteId !== null) {
					foreach($this->collHPagesRelatedBySeoRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collHPagesRelatedBySeoEnRouteId !== null) {
					foreach($this->collHPagesRelatedBySeoEnRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collHCategorysRelatedBySeoRouteId !== null) {
					foreach($this->collHCategorysRelatedBySeoRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collHCategorysRelatedBySeoEnRouteId !== null) {
					foreach($this->collHCategorysRelatedBySeoEnRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collHPostsRelatedBySeoRouteId !== null) {
					foreach($this->collHPostsRelatedBySeoRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collHPostsRelatedBySeoEnRouteId !== null) {
					foreach($this->collHPostsRelatedBySeoEnRouteId as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HRoutePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getDescription();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getParttern();
				break;
			case 4:
				return $this->getRequirements();
				break;
			case 5:
				return $this->getModel();
				break;
			case 6:
				return $this->getModels();
				break;
			case 7:
				return $this->getModule();
				break;
			case 8:
				return $this->getAction();
				break;
			case 9:
				return $this->getParams();
				break;
			case 10:
				return $this->getPriority();
				break;
			case 11:
				return $this->getAddCache();
				break;
			case 12:
				return $this->getSeoTitle();
				break;
			case 13:
				return $this->getSeoDescription();
				break;
			case 14:
				return $this->getSeoOgImage();
				break;
			case 15:
				return $this->getJson();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HRoutePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getDescription(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getParttern(),
			$keys[4] => $this->getRequirements(),
			$keys[5] => $this->getModel(),
			$keys[6] => $this->getModels(),
			$keys[7] => $this->getModule(),
			$keys[8] => $this->getAction(),
			$keys[9] => $this->getParams(),
			$keys[10] => $this->getPriority(),
			$keys[11] => $this->getAddCache(),
			$keys[12] => $this->getSeoTitle(),
			$keys[13] => $this->getSeoDescription(),
			$keys[14] => $this->getSeoOgImage(),
			$keys[15] => $this->getJson(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HRoutePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setDescription($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setParttern($value);
				break;
			case 4:
				$this->setRequirements($value);
				break;
			case 5:
				$this->setModel($value);
				break;
			case 6:
				$this->setModels($value);
				break;
			case 7:
				$this->setModule($value);
				break;
			case 8:
				$this->setAction($value);
				break;
			case 9:
				$this->setParams($value);
				break;
			case 10:
				$this->setPriority($value);
				break;
			case 11:
				$this->setAddCache($value);
				break;
			case 12:
				$this->setSeoTitle($value);
				break;
			case 13:
				$this->setSeoDescription($value);
				break;
			case 14:
				$this->setSeoOgImage($value);
				break;
			case 15:
				$this->setJson($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HRoutePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDescription($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setParttern($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRequirements($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setModel($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setModels($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setModule($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAction($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setParams($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setPriority($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setAddCache($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSeoTitle($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSeoDescription($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSeoOgImage($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setJson($arr[$keys[15]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HRoutePeer::DATABASE_NAME);

		if ($this->isColumnModified(HRoutePeer::ID)) $criteria->add(HRoutePeer::ID, $this->id);
		if ($this->isColumnModified(HRoutePeer::DESCRIPTION)) $criteria->add(HRoutePeer::DESCRIPTION, $this->description);
		if ($this->isColumnModified(HRoutePeer::NAME)) $criteria->add(HRoutePeer::NAME, $this->name);
		if ($this->isColumnModified(HRoutePeer::PARTTERN)) $criteria->add(HRoutePeer::PARTTERN, $this->parttern);
		if ($this->isColumnModified(HRoutePeer::REQUIREMENTS)) $criteria->add(HRoutePeer::REQUIREMENTS, $this->requirements);
		if ($this->isColumnModified(HRoutePeer::MODEL)) $criteria->add(HRoutePeer::MODEL, $this->model);
		if ($this->isColumnModified(HRoutePeer::MODELS)) $criteria->add(HRoutePeer::MODELS, $this->models);
		if ($this->isColumnModified(HRoutePeer::MODULE)) $criteria->add(HRoutePeer::MODULE, $this->module);
		if ($this->isColumnModified(HRoutePeer::ACTION)) $criteria->add(HRoutePeer::ACTION, $this->action);
		if ($this->isColumnModified(HRoutePeer::PARAMS)) $criteria->add(HRoutePeer::PARAMS, $this->params);
		if ($this->isColumnModified(HRoutePeer::PRIORITY)) $criteria->add(HRoutePeer::PRIORITY, $this->priority);
		if ($this->isColumnModified(HRoutePeer::ADD_CACHE)) $criteria->add(HRoutePeer::ADD_CACHE, $this->add_cache);
		if ($this->isColumnModified(HRoutePeer::SEO_TITLE)) $criteria->add(HRoutePeer::SEO_TITLE, $this->seo_title);
		if ($this->isColumnModified(HRoutePeer::SEO_DESCRIPTION)) $criteria->add(HRoutePeer::SEO_DESCRIPTION, $this->seo_description);
		if ($this->isColumnModified(HRoutePeer::SEO_OG_IMAGE)) $criteria->add(HRoutePeer::SEO_OG_IMAGE, $this->seo_og_image);
		if ($this->isColumnModified(HRoutePeer::JSON)) $criteria->add(HRoutePeer::JSON, $this->json);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HRoutePeer::DATABASE_NAME);

		$criteria->add(HRoutePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDescription($this->description);

		$copyObj->setName($this->name);

		$copyObj->setParttern($this->parttern);

		$copyObj->setRequirements($this->requirements);

		$copyObj->setModel($this->model);

		$copyObj->setModels($this->models);

		$copyObj->setModule($this->module);

		$copyObj->setAction($this->action);

		$copyObj->setParams($this->params);

		$copyObj->setPriority($this->priority);

		$copyObj->setAddCache($this->add_cache);

		$copyObj->setSeoTitle($this->seo_title);

		$copyObj->setSeoDescription($this->seo_description);

		$copyObj->setSeoOgImage($this->seo_og_image);

		$copyObj->setJson($this->json);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getHPagesRelatedBySeoRouteId() as $relObj) {
				$copyObj->addHPageRelatedBySeoRouteId($relObj->copy($deepCopy));
			}

			foreach($this->getHPagesRelatedBySeoEnRouteId() as $relObj) {
				$copyObj->addHPageRelatedBySeoEnRouteId($relObj->copy($deepCopy));
			}

			foreach($this->getHCategorysRelatedBySeoRouteId() as $relObj) {
				$copyObj->addHCategoryRelatedBySeoRouteId($relObj->copy($deepCopy));
			}

			foreach($this->getHCategorysRelatedBySeoEnRouteId() as $relObj) {
				$copyObj->addHCategoryRelatedBySeoEnRouteId($relObj->copy($deepCopy));
			}

			foreach($this->getHPostsRelatedBySeoRouteId() as $relObj) {
				$copyObj->addHPostRelatedBySeoRouteId($relObj->copy($deepCopy));
			}

			foreach($this->getHPostsRelatedBySeoEnRouteId() as $relObj) {
				$copyObj->addHPostRelatedBySeoEnRouteId($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HRoutePeer();
		}
		return self::$peer;
	}

	
	public function initHPagesRelatedBySeoRouteId()
	{
		if ($this->collHPagesRelatedBySeoRouteId === null) {
			$this->collHPagesRelatedBySeoRouteId = array();
		}
	}

	
	public function getHPagesRelatedBySeoRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHPagesRelatedBySeoRouteId === null) {
			if ($this->isNew()) {
			   $this->collHPagesRelatedBySeoRouteId = array();
			} else {

				$criteria->add(HPagePeer::SEO_ROUTE_ID, $this->getId());

				HPagePeer::addSelectColumns($criteria);
				$this->collHPagesRelatedBySeoRouteId = HPagePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HPagePeer::SEO_ROUTE_ID, $this->getId());

				HPagePeer::addSelectColumns($criteria);
				if (!isset($this->lastHPageRelatedBySeoRouteIdCriteria) || !$this->lastHPageRelatedBySeoRouteIdCriteria->equals($criteria)) {
					$this->collHPagesRelatedBySeoRouteId = HPagePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHPageRelatedBySeoRouteIdCriteria = $criteria;
		return $this->collHPagesRelatedBySeoRouteId;
	}

	
	public function countHPagesRelatedBySeoRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HPagePeer::SEO_ROUTE_ID, $this->getId());

		return HPagePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHPageRelatedBySeoRouteId(HPage $l)
	{
		$this->collHPagesRelatedBySeoRouteId[] = $l;
		$l->setHRouteRelatedBySeoRouteId($this);
	}

	
	public function initHPagesRelatedBySeoEnRouteId()
	{
		if ($this->collHPagesRelatedBySeoEnRouteId === null) {
			$this->collHPagesRelatedBySeoEnRouteId = array();
		}
	}

	
	public function getHPagesRelatedBySeoEnRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHPagesRelatedBySeoEnRouteId === null) {
			if ($this->isNew()) {
			   $this->collHPagesRelatedBySeoEnRouteId = array();
			} else {

				$criteria->add(HPagePeer::SEO_EN_ROUTE_ID, $this->getId());

				HPagePeer::addSelectColumns($criteria);
				$this->collHPagesRelatedBySeoEnRouteId = HPagePeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HPagePeer::SEO_EN_ROUTE_ID, $this->getId());

				HPagePeer::addSelectColumns($criteria);
				if (!isset($this->lastHPageRelatedBySeoEnRouteIdCriteria) || !$this->lastHPageRelatedBySeoEnRouteIdCriteria->equals($criteria)) {
					$this->collHPagesRelatedBySeoEnRouteId = HPagePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHPageRelatedBySeoEnRouteIdCriteria = $criteria;
		return $this->collHPagesRelatedBySeoEnRouteId;
	}

	
	public function countHPagesRelatedBySeoEnRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HPagePeer::SEO_EN_ROUTE_ID, $this->getId());

		return HPagePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHPageRelatedBySeoEnRouteId(HPage $l)
	{
		$this->collHPagesRelatedBySeoEnRouteId[] = $l;
		$l->setHRouteRelatedBySeoEnRouteId($this);
	}

	
	public function initHCategorysRelatedBySeoRouteId()
	{
		if ($this->collHCategorysRelatedBySeoRouteId === null) {
			$this->collHCategorysRelatedBySeoRouteId = array();
		}
	}

	
	public function getHCategorysRelatedBySeoRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedBySeoRouteId === null) {
			if ($this->isNew()) {
			   $this->collHCategorysRelatedBySeoRouteId = array();
			} else {

				$criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				$this->collHCategorysRelatedBySeoRouteId = HCategoryPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				if (!isset($this->lastHCategoryRelatedBySeoRouteIdCriteria) || !$this->lastHCategoryRelatedBySeoRouteIdCriteria->equals($criteria)) {
					$this->collHCategorysRelatedBySeoRouteId = HCategoryPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHCategoryRelatedBySeoRouteIdCriteria = $criteria;
		return $this->collHCategorysRelatedBySeoRouteId;
	}

	
	public function countHCategorysRelatedBySeoRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->getId());

		return HCategoryPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHCategoryRelatedBySeoRouteId(HCategory $l)
	{
		$this->collHCategorysRelatedBySeoRouteId[] = $l;
		$l->setHRouteRelatedBySeoRouteId($this);
	}


	
	public function getHCategorysRelatedBySeoRouteIdJoinHCategoryRelatedByParentId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedBySeoRouteId === null) {
			if ($this->isNew()) {
				$this->collHCategorysRelatedBySeoRouteId = array();
			} else {

				$criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->getId());

				$this->collHCategorysRelatedBySeoRouteId = HCategoryPeer::doSelectJoinHCategoryRelatedByParentId($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPeer::SEO_ROUTE_ID, $this->getId());

			if (!isset($this->lastHCategoryRelatedBySeoRouteIdCriteria) || !$this->lastHCategoryRelatedBySeoRouteIdCriteria->equals($criteria)) {
				$this->collHCategorysRelatedBySeoRouteId = HCategoryPeer::doSelectJoinHCategoryRelatedByParentId($criteria, $con);
			}
		}
		$this->lastHCategoryRelatedBySeoRouteIdCriteria = $criteria;

		return $this->collHCategorysRelatedBySeoRouteId;
	}

	
	public function initHCategorysRelatedBySeoEnRouteId()
	{
		if ($this->collHCategorysRelatedBySeoEnRouteId === null) {
			$this->collHCategorysRelatedBySeoEnRouteId = array();
		}
	}

	
	public function getHCategorysRelatedBySeoEnRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedBySeoEnRouteId === null) {
			if ($this->isNew()) {
			   $this->collHCategorysRelatedBySeoEnRouteId = array();
			} else {

				$criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				$this->collHCategorysRelatedBySeoEnRouteId = HCategoryPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->getId());

				HCategoryPeer::addSelectColumns($criteria);
				if (!isset($this->lastHCategoryRelatedBySeoEnRouteIdCriteria) || !$this->lastHCategoryRelatedBySeoEnRouteIdCriteria->equals($criteria)) {
					$this->collHCategorysRelatedBySeoEnRouteId = HCategoryPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHCategoryRelatedBySeoEnRouteIdCriteria = $criteria;
		return $this->collHCategorysRelatedBySeoEnRouteId;
	}

	
	public function countHCategorysRelatedBySeoEnRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->getId());

		return HCategoryPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHCategoryRelatedBySeoEnRouteId(HCategory $l)
	{
		$this->collHCategorysRelatedBySeoEnRouteId[] = $l;
		$l->setHRouteRelatedBySeoEnRouteId($this);
	}


	
	public function getHCategorysRelatedBySeoEnRouteIdJoinHCategoryRelatedByParentId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHCategorysRelatedBySeoEnRouteId === null) {
			if ($this->isNew()) {
				$this->collHCategorysRelatedBySeoEnRouteId = array();
			} else {

				$criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->getId());

				$this->collHCategorysRelatedBySeoEnRouteId = HCategoryPeer::doSelectJoinHCategoryRelatedByParentId($criteria, $con);
			}
		} else {
									
			$criteria->add(HCategoryPeer::SEO_EN_ROUTE_ID, $this->getId());

			if (!isset($this->lastHCategoryRelatedBySeoEnRouteIdCriteria) || !$this->lastHCategoryRelatedBySeoEnRouteIdCriteria->equals($criteria)) {
				$this->collHCategorysRelatedBySeoEnRouteId = HCategoryPeer::doSelectJoinHCategoryRelatedByParentId($criteria, $con);
			}
		}
		$this->lastHCategoryRelatedBySeoEnRouteIdCriteria = $criteria;

		return $this->collHCategorysRelatedBySeoEnRouteId;
	}

	
	public function initHPostsRelatedBySeoRouteId()
	{
		if ($this->collHPostsRelatedBySeoRouteId === null) {
			$this->collHPostsRelatedBySeoRouteId = array();
		}
	}

	
	public function getHPostsRelatedBySeoRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHPostsRelatedBySeoRouteId === null) {
			if ($this->isNew()) {
			   $this->collHPostsRelatedBySeoRouteId = array();
			} else {

				$criteria->add(HPostPeer::SEO_ROUTE_ID, $this->getId());

				HPostPeer::addSelectColumns($criteria);
				$this->collHPostsRelatedBySeoRouteId = HPostPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HPostPeer::SEO_ROUTE_ID, $this->getId());

				HPostPeer::addSelectColumns($criteria);
				if (!isset($this->lastHPostRelatedBySeoRouteIdCriteria) || !$this->lastHPostRelatedBySeoRouteIdCriteria->equals($criteria)) {
					$this->collHPostsRelatedBySeoRouteId = HPostPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHPostRelatedBySeoRouteIdCriteria = $criteria;
		return $this->collHPostsRelatedBySeoRouteId;
	}

	
	public function countHPostsRelatedBySeoRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HPostPeer::SEO_ROUTE_ID, $this->getId());

		return HPostPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHPostRelatedBySeoRouteId(HPost $l)
	{
		$this->collHPostsRelatedBySeoRouteId[] = $l;
		$l->setHRouteRelatedBySeoRouteId($this);
	}

	
	public function initHPostsRelatedBySeoEnRouteId()
	{
		if ($this->collHPostsRelatedBySeoEnRouteId === null) {
			$this->collHPostsRelatedBySeoEnRouteId = array();
		}
	}

	
	public function getHPostsRelatedBySeoEnRouteId($criteria = null, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collHPostsRelatedBySeoEnRouteId === null) {
			if ($this->isNew()) {
			   $this->collHPostsRelatedBySeoEnRouteId = array();
			} else {

				$criteria->add(HPostPeer::SEO_EN_ROUTE_ID, $this->getId());

				HPostPeer::addSelectColumns($criteria);
				$this->collHPostsRelatedBySeoEnRouteId = HPostPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(HPostPeer::SEO_EN_ROUTE_ID, $this->getId());

				HPostPeer::addSelectColumns($criteria);
				if (!isset($this->lastHPostRelatedBySeoEnRouteIdCriteria) || !$this->lastHPostRelatedBySeoEnRouteIdCriteria->equals($criteria)) {
					$this->collHPostsRelatedBySeoEnRouteId = HPostPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastHPostRelatedBySeoEnRouteIdCriteria = $criteria;
		return $this->collHPostsRelatedBySeoEnRouteId;
	}

	
	public function countHPostsRelatedBySeoEnRouteId($criteria = null, $distinct = false, $con = null)
	{
				if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(HPostPeer::SEO_EN_ROUTE_ID, $this->getId());

		return HPostPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addHPostRelatedBySeoEnRouteId(HPost $l)
	{
		$this->collHPostsRelatedBySeoEnRouteId[] = $l;
		$l->setHRouteRelatedBySeoEnRouteId($this);
	}

} 