<?php


abstract class BaseHPage extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $detail;


	
	protected $detail_en;


	
	protected $priority = 10;


	
	protected $hide = false;


	
	protected $json;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $seo_route_id;


	
	protected $seo_en_route_id;


	
	protected $seo_slug;


	
	protected $seo_en_slug;


	
	protected $seo_title;


	
	protected $seo_en_title;


	
	protected $seo_description;


	
	protected $seo_en_description;


	
	protected $seo_og_image;

	
	protected $aHRouteRelatedBySeoRouteId;

	
	protected $aHRouteRelatedBySeoEnRouteId;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

        return $this->id;
	}

	
	public function getDetail()
	{

        return $this->detail;
	}

	
	public function getDetailEn()
	{

        return $this->detail_en;
	}

	
	public function getPriority()
	{

        return $this->priority;
	}

	
	public function getHide()
	{

        return $this->hide;
	}

	
	public function getJson()
	{

        $ret = @json_decode($this->json, true);
         if(!$ret) {
           $ret = array();
         }
         return $ret;
        
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSeoRouteId()
	{

        return $this->seo_route_id;
	}

	
	public function getSeoEnRouteId()
	{

        return $this->seo_en_route_id;
	}

	
	public function getSeoSlug()
	{

        return $this->seo_slug;
	}

	
	public function getSeoEnSlug()
	{

        return $this->seo_en_slug;
	}

	
	public function getSeoTitle()
	{

        return $this->seo_title;
	}

	
	public function getSeoEnTitle()
	{

        return $this->seo_en_title;
	}

	
	public function getSeoDescription()
	{

        return $this->seo_description;
	}

	
	public function getSeoEnDescription()
	{

        return $this->seo_en_description;
	}

	
	public function getSeoOgImage()
	{

        return $this->seo_og_image;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HPagePeer::ID;
		}

	} 
	
	public function setDetail($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->detail !== $v) {
			$this->detail = $v;
			$this->modifiedColumns[] = HPagePeer::DETAIL;
		}

	} 
	
	public function setDetailEn($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->detail_en !== $v) {
			$this->detail_en = $v;
			$this->modifiedColumns[] = HPagePeer::DETAIL_EN;
		}

	} 
	
	public function setPriority($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->priority !== $v || $v === 10) {
			$this->priority = $v;
			$this->modifiedColumns[] = HPagePeer::PRIORITY;
		}

	} 
	
	public function setHide($v)
	{

		if ($this->hide !== $v || $v === false) {
			$this->hide = $v;
			$this->modifiedColumns[] = HPagePeer::HIDE;
		}

	} 
	
	public function setJson($v)
	{

        $v = json_encode($v);
        
        
		if ($this->json !== $v) {
			$this->json = $v;
			$this->modifiedColumns[] = HPagePeer::JSON;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = HPagePeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = HPagePeer::UPDATED_AT;
		}

	} 
	
	public function setSeoRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_route_id !== $v) {
			$this->seo_route_id = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoRouteId !== null && $this->aHRouteRelatedBySeoRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoRouteId = null;
		}

	} 
	
	public function setSeoEnRouteId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->seo_en_route_id !== $v) {
			$this->seo_en_route_id = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_EN_ROUTE_ID;
		}

		if ($this->aHRouteRelatedBySeoEnRouteId !== null && $this->aHRouteRelatedBySeoEnRouteId->getId() !== $v) {
			$this->aHRouteRelatedBySeoEnRouteId = null;
		}

	} 
	
	public function setSeoSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_slug !== $v) {
			$this->seo_slug = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_SLUG;
		}

	} 
	
	public function setSeoEnSlug($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_slug !== $v) {
			$this->seo_en_slug = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_EN_SLUG;
		}

	} 
	
	public function setSeoTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_title !== $v) {
			$this->seo_title = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_TITLE;
		}

	} 
	
	public function setSeoEnTitle($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_title !== $v) {
			$this->seo_en_title = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_EN_TITLE;
		}

	} 
	
	public function setSeoDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_description !== $v) {
			$this->seo_description = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_DESCRIPTION;
		}

	} 
	
	public function setSeoEnDescription($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_en_description !== $v) {
			$this->seo_en_description = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_EN_DESCRIPTION;
		}

	} 
	
	public function setSeoOgImage($v)
	{

                        if ($v !== null && !is_string($v)) {
          $v = (string) $v; 
        }
        
		if ($this->seo_og_image !== $v) {
			$this->seo_og_image = $v;
			$this->modifiedColumns[] = HPagePeer::SEO_OG_IMAGE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->detail = $rs->getString($startcol + 1);

			$this->detail_en = $rs->getString($startcol + 2);

			$this->priority = $rs->getInt($startcol + 3);

			$this->hide = $rs->getBoolean($startcol + 4);

			$this->json = $rs->getString($startcol + 5);

			$this->created_at = $rs->getTimestamp($startcol + 6, null);

			$this->updated_at = $rs->getTimestamp($startcol + 7, null);

			$this->seo_route_id = $rs->getInt($startcol + 8);

			$this->seo_en_route_id = $rs->getInt($startcol + 9);

			$this->seo_slug = $rs->getString($startcol + 10);

			$this->seo_en_slug = $rs->getString($startcol + 11);

			$this->seo_title = $rs->getString($startcol + 12);

			$this->seo_en_title = $rs->getString($startcol + 13);

			$this->seo_description = $rs->getString($startcol + 14);

			$this->seo_en_description = $rs->getString($startcol + 15);

			$this->seo_og_image = $rs->getString($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HPage object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HPagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HPagePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(HPagePeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(HPagePeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HPagePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if ($this->aHRouteRelatedBySeoRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoRouteId($this->aHRouteRelatedBySeoRouteId);
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if ($this->aHRouteRelatedBySeoEnRouteId->isModified()) {
					$affectedRows += $this->aHRouteRelatedBySeoEnRouteId->save($con);
				}
				$this->setHRouteRelatedBySeoEnRouteId($this->aHRouteRelatedBySeoEnRouteId);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HPagePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += HPagePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aHRouteRelatedBySeoRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoRouteId->getValidationFailures());
				}
			}

			if ($this->aHRouteRelatedBySeoEnRouteId !== null) {
				if (!$this->aHRouteRelatedBySeoEnRouteId->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aHRouteRelatedBySeoEnRouteId->getValidationFailures());
				}
			}


			if (($retval = HPagePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HPagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getDetail();
				break;
			case 2:
				return $this->getDetailEn();
				break;
			case 3:
				return $this->getPriority();
				break;
			case 4:
				return $this->getHide();
				break;
			case 5:
				return $this->getJson();
				break;
			case 6:
				return $this->getCreatedAt();
				break;
			case 7:
				return $this->getUpdatedAt();
				break;
			case 8:
				return $this->getSeoRouteId();
				break;
			case 9:
				return $this->getSeoEnRouteId();
				break;
			case 10:
				return $this->getSeoSlug();
				break;
			case 11:
				return $this->getSeoEnSlug();
				break;
			case 12:
				return $this->getSeoTitle();
				break;
			case 13:
				return $this->getSeoEnTitle();
				break;
			case 14:
				return $this->getSeoDescription();
				break;
			case 15:
				return $this->getSeoEnDescription();
				break;
			case 16:
				return $this->getSeoOgImage();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HPagePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getDetail(),
			$keys[2] => $this->getDetailEn(),
			$keys[3] => $this->getPriority(),
			$keys[4] => $this->getHide(),
			$keys[5] => $this->getJson(),
			$keys[6] => $this->getCreatedAt(),
			$keys[7] => $this->getUpdatedAt(),
			$keys[8] => $this->getSeoRouteId(),
			$keys[9] => $this->getSeoEnRouteId(),
			$keys[10] => $this->getSeoSlug(),
			$keys[11] => $this->getSeoEnSlug(),
			$keys[12] => $this->getSeoTitle(),
			$keys[13] => $this->getSeoEnTitle(),
			$keys[14] => $this->getSeoDescription(),
			$keys[15] => $this->getSeoEnDescription(),
			$keys[16] => $this->getSeoOgImage(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HPagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setDetail($value);
				break;
			case 2:
				$this->setDetailEn($value);
				break;
			case 3:
				$this->setPriority($value);
				break;
			case 4:
				$this->setHide($value);
				break;
			case 5:
				$this->setJson($value);
				break;
			case 6:
				$this->setCreatedAt($value);
				break;
			case 7:
				$this->setUpdatedAt($value);
				break;
			case 8:
				$this->setSeoRouteId($value);
				break;
			case 9:
				$this->setSeoEnRouteId($value);
				break;
			case 10:
				$this->setSeoSlug($value);
				break;
			case 11:
				$this->setSeoEnSlug($value);
				break;
			case 12:
				$this->setSeoTitle($value);
				break;
			case 13:
				$this->setSeoEnTitle($value);
				break;
			case 14:
				$this->setSeoDescription($value);
				break;
			case 15:
				$this->setSeoEnDescription($value);
				break;
			case 16:
				$this->setSeoOgImage($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HPagePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetail($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDetailEn($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPriority($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setHide($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setJson($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCreatedAt($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUpdatedAt($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSeoRouteId($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setSeoEnRouteId($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSeoSlug($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSeoEnSlug($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSeoTitle($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSeoEnTitle($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSeoDescription($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSeoEnDescription($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSeoOgImage($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HPagePeer::DATABASE_NAME);

		if ($this->isColumnModified(HPagePeer::ID)) $criteria->add(HPagePeer::ID, $this->id);
		if ($this->isColumnModified(HPagePeer::DETAIL)) $criteria->add(HPagePeer::DETAIL, $this->detail);
		if ($this->isColumnModified(HPagePeer::DETAIL_EN)) $criteria->add(HPagePeer::DETAIL_EN, $this->detail_en);
		if ($this->isColumnModified(HPagePeer::PRIORITY)) $criteria->add(HPagePeer::PRIORITY, $this->priority);
		if ($this->isColumnModified(HPagePeer::HIDE)) $criteria->add(HPagePeer::HIDE, $this->hide);
		if ($this->isColumnModified(HPagePeer::JSON)) $criteria->add(HPagePeer::JSON, $this->json);
		if ($this->isColumnModified(HPagePeer::CREATED_AT)) $criteria->add(HPagePeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(HPagePeer::UPDATED_AT)) $criteria->add(HPagePeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(HPagePeer::SEO_ROUTE_ID)) $criteria->add(HPagePeer::SEO_ROUTE_ID, $this->seo_route_id);
		if ($this->isColumnModified(HPagePeer::SEO_EN_ROUTE_ID)) $criteria->add(HPagePeer::SEO_EN_ROUTE_ID, $this->seo_en_route_id);
		if ($this->isColumnModified(HPagePeer::SEO_SLUG)) $criteria->add(HPagePeer::SEO_SLUG, $this->seo_slug);
		if ($this->isColumnModified(HPagePeer::SEO_EN_SLUG)) $criteria->add(HPagePeer::SEO_EN_SLUG, $this->seo_en_slug);
		if ($this->isColumnModified(HPagePeer::SEO_TITLE)) $criteria->add(HPagePeer::SEO_TITLE, $this->seo_title);
		if ($this->isColumnModified(HPagePeer::SEO_EN_TITLE)) $criteria->add(HPagePeer::SEO_EN_TITLE, $this->seo_en_title);
		if ($this->isColumnModified(HPagePeer::SEO_DESCRIPTION)) $criteria->add(HPagePeer::SEO_DESCRIPTION, $this->seo_description);
		if ($this->isColumnModified(HPagePeer::SEO_EN_DESCRIPTION)) $criteria->add(HPagePeer::SEO_EN_DESCRIPTION, $this->seo_en_description);
		if ($this->isColumnModified(HPagePeer::SEO_OG_IMAGE)) $criteria->add(HPagePeer::SEO_OG_IMAGE, $this->seo_og_image);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HPagePeer::DATABASE_NAME);

		$criteria->add(HPagePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDetail($this->detail);

		$copyObj->setDetailEn($this->detail_en);

		$copyObj->setPriority($this->priority);

		$copyObj->setHide($this->hide);

		$copyObj->setJson($this->json);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setSeoRouteId($this->seo_route_id);

		$copyObj->setSeoEnRouteId($this->seo_en_route_id);

		$copyObj->setSeoSlug($this->seo_slug);

		$copyObj->setSeoEnSlug($this->seo_en_slug);

		$copyObj->setSeoTitle($this->seo_title);

		$copyObj->setSeoEnTitle($this->seo_en_title);

		$copyObj->setSeoDescription($this->seo_description);

		$copyObj->setSeoEnDescription($this->seo_en_description);

		$copyObj->setSeoOgImage($this->seo_og_image);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

    public function __toString() {
		return method_exists($this, 'getName') ? $this->getName(): $this->getId();
	}
	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HPagePeer();
		}
		return self::$peer;
	}

	
	public function setHRouteRelatedBySeoRouteId($v)
	{


		if ($v === null) {
			$this->setSeoRouteId(NULL);
		} else {
			$this->setSeoRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoRouteId = $v;
	}


	
	static $HRouteRelatedBySeoRouteId = array();
	
	public function getHRouteRelatedBySeoRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoRouteId === null && ($this->seo_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoRouteId[$this->seo_route_id])){
				self::$HRouteRelatedBySeoRouteId[$this->seo_route_id] = HRoutePeer::retrieveByPK($this->seo_route_id, $con);
			}
			$this->aHRouteRelatedBySeoRouteId = self::$HRouteRelatedBySeoRouteId[$this->seo_route_id];

			
		}
		return $this->aHRouteRelatedBySeoRouteId;
	}

	
	public function setHRouteRelatedBySeoEnRouteId($v)
	{


		if ($v === null) {
			$this->setSeoEnRouteId(NULL);
		} else {
			$this->setSeoEnRouteId($v->getId());
		}


		$this->aHRouteRelatedBySeoEnRouteId = $v;
	}


	
	static $HRouteRelatedBySeoEnRouteId = array();
	
	public function getHRouteRelatedBySeoEnRouteId($con = null)
	{
		if ($this->aHRouteRelatedBySeoEnRouteId === null && ($this->seo_en_route_id !== null)) {
						if(!isset(self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id])){
				self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id] = HRoutePeer::retrieveByPK($this->seo_en_route_id, $con);
			}
			$this->aHRouteRelatedBySeoEnRouteId = self::$HRouteRelatedBySeoEnRouteId[$this->seo_en_route_id];

			
		}
		return $this->aHRouteRelatedBySeoEnRouteId;
	}

} 