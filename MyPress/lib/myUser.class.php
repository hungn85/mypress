<?php

class myUser extends sfBasicSecurityUser {
    
   public function getId() {
      return $this->getMember()->getId();
   }

   public function doLogout() {
       PlugLogin::logout();
   }

   public function getMember($session = true) {
      return PlugLogin::getUserModel($session);
   }
  
   public function isEn() {
      return myUtil::isEn();
   }
   public function getFacebookApi() {


      require sfConfig::get('sf_root_dir') . '/facebook_sdk_src/facebook.php';

      $p = $this->getPartner();

      # Creating the facebook object  
      $facebook = new Facebook(array(
          'appId' => $p->getFbAppId(),
          'secret' => $p->getFbAppSecret(),
          'cookie' => true
      ));
      return $facebook;
   }
 

}
