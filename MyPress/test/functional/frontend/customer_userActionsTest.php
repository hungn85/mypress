<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

// create a new test browser
$browser = new sfTestBrowser();

$browser->
  get('/customer_user/index')->
  isStatusCode(200)->
  isRequestParameter('module', 'customer_user')->
  isRequestParameter('action', 'index')->
  checkResponseElement('body', '!/This is a temporary page/');
