<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

// create a new test browser
$browser = new sfTestBrowser();

$browser->
  get('/system/index')->
  isStatusCode(200)->
  isRequestParameter('module', 'system')->
  isRequestParameter('action', 'index')->
  checkResponseElement('body', '!/This is a temporary page/');
