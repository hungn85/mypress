<?php

/**
 * hpost actions.
 *
 * @package    sf_sandbox
 * @subpackage hpost
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class h_postActions extends autoh_postActions
{
	protected function saveHPost($h_post) {
      parent::saveHPost($h_post);
      HRoutePeer::addSeoModel(
          $h_post, 
          $this->getRequestParameter('seo'), 
          $this->getRequestParameter('seo_en'));
    }
}
