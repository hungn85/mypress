<?php

/**
 * user actions.
 *
 * @package    sf_sandbox
 * @subpackage user
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class h_userActions extends autoh_userActions {

   protected function updateHUserFromRequest() {
      $huser = $this->getRequestParameter('h_user');

      if (isset($huser['username'])) {
         $this->h_user->setUsername($huser['username']);
      } 


      if (isset($huser['password'])) {
         if (strlen($huser['password'])) {
            $this->h_user->setPassword(md5($huser['password']));
         }
      }
      $this->h_user->setActive(isset($huser['active']) ? $huser['active'] : 0);
   }

}
