<style>
    .seo_row {margin-bottom: 10px;}
    .seo_row p { margin-top: 10px !important;}
    .seo_row input, .seo_row textarea { padding: 10px !important; width: 380px;}
    .seo_row textarea {height: 100px;}
    .seo_row select{ width: 400px; }
    .slug_frame span{ display: inline-block; width: 400px;}
    .slug_frame > span > input { width: 200px;}
</style>
<script>
    $(document).ready(function() {
        function genSlug(id, genTo, name, slug) {
            var opt = $('#'+id+' option:selected');
            var parttern = opt.attr('data-parttern');
            var routeName = opt.attr('data-parttern');
            var slInput = '<input type="text" name="'+name+'" placeholder="slug" value="'+slug+'"/>';
            if(parttern) {
                $(genTo).html(parttern.replace(':slug', slInput));
                $('.frontend_ma_frame').hide();
            }
            else {
                
                $(genTo).html(slInput);
                $('.frontend_ma_frame').show();
            }
            
        }
        genSlug('seo_route_id', '.slug_frame .slug', 'seo[slug]', '<?=$model->getSeoSlug()?>');
        genSlug('seo_en_route_id', '.slug_frame .slug_en', 'seo_en[slug]', '<?=$model->getSeoEnSlug()?>');
        
        $('#seo_route_id').change(function() {
            genSlug('seo_route_id', '.slug_frame .slug', 'seo[slug]', '<?=$model->getSeoSlug()?>');
        });

        $('#seo_en_route_id').change(function() {
            genSlug('seo_en_route_id', '.slug_frame .slug_en', 'seo_en[slug]', '<?=$model->getSeoEnSlug()?>');
        });
    });
</script>
 
<div class="seo_row">
     
    <p> 
        <select name="seo[route_id]" id="seo_route_id">
        <?php if(!$routes):?>
        
             
            <?php if($vroute = $model->getHRouteRelatedBySeoRouteId()) : ?>
                <option value="<?=$model->getSeoRouteId()?>">
                    <?=$vroute->getParttern()?>
                </option>

            <?php endif?>

        <?php else: ?>
            <?php foreach($routes as  $route ):?>
                <?php $selected = $route->getId()==$model->getSeoRouteId(); ?> 

                <option <?=$route->getId()==$model->getSeoRouteId()?'selected':''?> value="<?=$route->getId()?>" data-parttern="<?=$route->getParttern()?>" data-route-name="<?=$route->getName()?>"><?=$route->getParttern()?></option>
            <?php endforeach?>
        <?php endif?>
        </select>
     
        <select name="seo_en[route_id]" id="seo_en_route_id">
        <?php if(!$en_routes):?>
        
            <?php if($eroute = $model->getHRouteRelatedBySeoEnRouteId()):?>
                <option value="<?=$model->getSeoEnRouteId()?>"><?=$eroute->getParttern()?></option>
            <?php endif?>

        <?php else: ?>
            <?php foreach($en_routes as $name => $route ):?>
                <option data-route-name="<?=$route->getName()?>" <?=$route->getId()==$model->getSeoEnRouteId()?'selected':''?> value="<?=$route->getId()?>" data-parttern="<?=$route->getParttern()?>"><?=$route->getParttern()?></option>
            <?php endforeach?>
        <?php endif?>
        </select>
    </p>

    <p class="slug_frame">
        <span class="slug"></span>
        <span class="slug_en"></span> 
    </p>
    <p>
        <input type="text" name="seo[title]" placeholder="Nhập title" value="<?=$model->getSeoTitle()?>"/>
     
        <input type="text" name="seo_en[title]" placeholder="Nhập title tiếng anh" value="<?=$model->getSeoEnTitle()?>"/>
    </p>
    
    
    <p>
        <textarea placeholder="Nhập description"  name="seo[description]"><?=$model->getSeoDescription()?></textarea>
     
        <textarea placeholder="Nhập description tiếng anh" name="seo_en[description]"><?=$model->getSeoEnDescription()?></textarea>
    </p>
    OG-IMG: <input style="width: 85px;" type="file" name="seo[og_image]"/> (200x200)
    <?php if($model->getSeoOgImage()): ?>
    [ 
        <a href="<?=$model->getSeoOgImage()?>" target="_blank">
            <img src="<?=$model->getSeoOgImage()?>" style="height: 35px; vertical-align: top;">
        </a> 
    ]
    [ <input style=" width: 20px;" type="checkbox" name="seo[remove_og_image]" value="1"/> Remove ]
    <?php endif?>

    <p class="frontend_ma_frame">
        <?php $route = $model->getHRouteRelatedBySeoRouteId();?> 
        
        <input placeholder="Frontend Module"  name="seo[frontend_module]" id="frontend_module" value="<?=$route?$route->getModule():'' ?>"/>
     
        <input placeholder="Frontend Action" name="seo[frontend_action]" id="frontend_action" value="<?=$route?$route->getAction():'' ?>"/>
    </p>
</div> 
