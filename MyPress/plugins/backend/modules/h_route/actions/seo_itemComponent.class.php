<?php
    
    class seo_itemComponent extends sfComponent {
        public function execute($request) {
            //model
            
            $this->routes = array();
            $this->en_routes = array();
            $this->getRoutes();
        }
        
        protected function getRoutes() {
            $c = new Criteria;
            $c->add(HRoutePeer::MODEL, get_class($this->model));
            $cr = $c->getNewCriterion(HRoutePeer::NAME, '%_auto', Criteria::NOT_LIKE);
            $cr->addAnd($c->getNewCriterion(HRoutePeer::NAME, '%_auto_en', Criteria::NOT_LIKE));
            $c->add($cr);
            $rs = HRoutePeer::doSelect($c);
            $ret = array();
            foreach($rs as $o) {
                $name = $o->getName();
                if(substr($name, -3) == '_en') {
                     
                    $this->en_routes[$name] = $o;
                }
                else {
                    $this->routes[$name] =  $o;
                }
            }
            return $ret;
        }
    }