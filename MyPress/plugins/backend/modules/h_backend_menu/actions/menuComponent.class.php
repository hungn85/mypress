<?php 
	
	class menuComponent extends sfComponent {
		public function execute($r) {
			$c = new Criteria;
			$c->addAscendingOrderByColumn(HBackendMenuPeer::PRIORITY);
			$bms = HBackendMenuPeer::doSelect($c);
			$menus = array();
			foreach($bms as $bm) {
				$menus [] = array(
					'id' => $bm->getId(),
				 	'name' => $bm->getName(), 
				 	'link' => $bm->getLink(), 
				 	'parent_id' => intval($bm->getParentId()));
			}; 

			$menu = new RecursiveMenu($menus);
			echo $menu->build();
			return sfView::NONE;
		}
	}