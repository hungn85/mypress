<?php

/**
 * hmenu actions.
 *
 * @package    sf_sandbox
 * @subpackage hmenu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class h_pageActions extends autoh_pageActions
{
	protected function saveHPage($h_page) {
      parent::saveHPage($h_page);
      HRoutePeer::addSeoModel(
          $h_page, 
          $this->getRequestParameter('seo'), 
          $this->getRequestParameter('seo_en'));
    }
}
