<?php

/**
 * security actions.
 *
 * @package    bot_quang
 * @subpackage security
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class change_passwordAction extends sfAction
{
  /**
   * Executes index action
   *
   */
  public function execute($r)
  {
  	if($this->getRequest()->getMethod() == sfRequest::POST)
	{
		$user = $this->getRequestParameter('user');
		$me = HUserPeer::retrieveByPk($this->getUser()->getAttribute("id", "", "user"));
		if($user["password"] != '')
		{
			$me->setPassword(md5($user["password"]));
			$me->save();
			$this->getUser()->setFlash("notice", "Your password has been changed !");
		}
		else {
			$this->getUser()->setFlash("notice", "Please type your password");
		}
		$this->redirect('h_security/change_password');
	}
  
  }
}
