<?php use_helper('I18N', 'Date') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<form action="" method="post">
<div id="sf_admin_container">
   <h1>Change Password</h1>
   <div id="sf_admin_header"></div>
   <div id="sf_admin_bar"></div>
   <div id="sf_admin_content">
      <table cellspacing="0" class="sf_admin_list">
           
        <tr>
			<td>&nbsp;</td>
			<td>Username: </td>
			<td><label>
			  <input type="text" name="user[user_name]" style="width:180px;"  value="<?php echo $sf_user->getAttribute('username', '', 'user') ?>" readonly="" />
			</label></td>
		</tr>
	    <tr>
			<td>&nbsp;</td>
			<td>Password:</td>
			<td><label>
			  <input type="password" name="user[password]" style="width:180px;" />
			</label></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
				<input type="submit" name="Submit" value="Submit" class="button" />
			</td>
		</tr>
		<tr>
			<td colspan="3" style="color:#FF0000">
				<?php if ($sf_user->hasFlash('notice')) echo $sf_user->getFlash('notice');?>
			</td>
		</tr>
      </table>
       
   </div> 
</div>
</form>