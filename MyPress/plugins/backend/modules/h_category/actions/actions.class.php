<?php

/**
 * hmenu actions.
 *
 * @package    sf_sandbox
 * @subpackage hmenu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class h_categoryActions extends autoh_categoryActions
{
	protected function saveHCategory($h_category) {
      parent::saveHCategory($h_category);
      HRoutePeer::addSeoModel(
          $h_category, 
          $this->getRequestParameter('seo'), 
          $this->getRequestParameter('seo_en'));
    }
}
