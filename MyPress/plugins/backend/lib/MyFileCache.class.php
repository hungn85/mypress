<?php

class MyFileCache {
   private $cache;
   private static $bc;
   public static function getInstance() {
      if(!self::$bc) {
         self::$bc = new MyFileCache;
         self::$bc->cache = new sfFileCache(array(
            'cache_dir' => sfConfig::get('sf_cache_dir').'/my_file_cache',
            'lifetime' => 36000
         )); 
      }
      return self::$bc;
   }
   
   private function getKey($var) {
      
      $params = $var;
      $class = "";
      if(is_object($params)) {
         $class = get_class($params);
         $params = get_object_vars($params);
      }
      return $class."_".$_SERVER['HTTP_HOST']."_".md5(serialize($params));
   }
   
   public function has($var) { 
      $key = $this->getKey($var);
      return self::getInstance()->cache->has($key);
   }
   
   public function clean() {  
      return self::getInstance()->cache->clean();
   }
   public function set($var, $value) {
      $key = $this->getKey($var);
      self::getInstance()->cache->set($key, serialize($value));
   }
   
   public function get($var) {
      $key = $this->getKey($var);
      return unserialize(self::getInstance()->cache->get($key));
   }
}