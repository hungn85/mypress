<?php 
   
   class DeleteRouteReferencePlug {

      public static function delete($object) {
         $c = new Criteria;
         $c->add(HRoutePeer::MODEL, get_class($object));
         $k = 'id:'.$object->getId();
         $cr = $c->getNewCriterion(HRoutePeer::PARAMS, $k.'|%', Criteria::LIKE);
         $cr->addOr($c->getNewCriterion(HRoutePeer::PARAMS, $k));
         $cr->addOr($c->getNewCriterion(HRoutePeer::PARAMS, '%|'.$k), Criteria::LIKE);
         $c->add($cr);
         HRoutePeer::doDelete($c);
      }
   }