<?php 
   
   class PlugAddSeoModel {

      protected $model, $routeData, $routeEnData, $module, $action;

      public function __construct($model, $routeData, $routeEnData) {
         $this->model = $model;
         $this->routeData = myUtil::trimArray($routeData);
         $this->routeEnData = myUtil::trimArray($routeEnData);
         
         $this->module = $routeData['frontend_module'];
         $this->action = $routeData['frontend_action'];
      }

      public function execute() {
         $this->addSeoModel($this->routeData);
         $this->addSeoModel($this->routeEnData, true);

         $this->processUploadFile($this->routeData);
      }

      function addSeoModel($routeData, $isEn = false) {
        $data = new PlugAddRouteData(array(
         'model' => $this->model, 
         'route_data' => $routeData,
         'module' => $this->module,
         'action' => $this->action,
         'en_str' => myFunc::get1False2($isEn, 'En', '')));
        $data->execute();
      }


    function processUploadFile($routeData) {
      //process og-image
        $fName = $_FILES['seo']['name']['og_image'];
        $size = $_FILES['seo']['size']['og_image'];
        $error = $_FILES['seo']['error']['og_image'];
        $tmp = $_FILES['seo']['tmp_name']['og_image'];
        $type = $_FILES['seo']['type']['og_image'];

        $hasFile = $error == 0 && $size > 0;
        $isRemove = isset($routeData['remove_og_image']);
        if(($hasFile || $isRemove) && $this->model->getSeoOgImage()) {
            @unlink(sfConfig::get('sf_web_dir').$this->model->getSeoOgImage()); 
            $this->model->setSeoOgImage(null);
        }

        if($hasFile) {

            $newFname = rand(1000, 10000).'_'.str_replace(' ', '', $fName);
            $peer = get_class($this->model).'Peer';
            $path = '/uploads/'.  ($peer::TABLE_NAME);
            $folder = sfConfig::get('sf_web_dir').$path;
            !is_dir($folder) ? mkdir($folder) : null;

            move_uploaded_file($tmp, $folder.'/'.$newFname);
            $this->model->setSeoOgImage($path.'/'.$newFname);
        } 
        $this->model->save();  
    }
   }