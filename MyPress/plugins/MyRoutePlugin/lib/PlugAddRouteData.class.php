<?php
   
   class PlugAddRouteData {

      protected $model, $module, $action, $routeData, $enStr;

      public function __construct($data) {
         $this->model = $data['model'];
         $this->routeData = myUtil::trimArray($data['route_data']);
         $this->module = $data['module'];
         $this->action = $data['action'];
         $this->enStr = $data['en_str'];

         $this->rawSlug = $this->routeData['slug'];
         $this->slug = $this->filterSlug($this->rawSlug);
         $this->hasRouteId = isset($this->routeData['route_id']);
      }

      public function execute() {
 
         $this->updateDataToModel();

         //khoi tao lan dau tien
         if($this->slug && (!$this->hasRouteId || !$this->routeData['route_id'])) {
            $this->processFirstCreate();
         }
         //da ton tai 
         elseif($this->rawSlug) {

            $this->updateOldAutoRoute();

            $this->addRandomSlugToModel();
         }

         $this->model->save();
         
      }


      protected function updateOldAutoRoute() {

         $method = 'getHRouteRelatedBySeo'.$this->enStr.'RouteId';

         $route = $this->model->$method();

         //Neu route cu la route tao tu dong 
         if(strpos($route->getName(), $this->model->getId().'_auto') !== false) {
              
           $route->setModule($this->module);
           $route->setAction($this->action);
           $route->setParttern($this->slug);
           $route->save();
         }
      }

      protected function addRandomSlugToModel() {
         $clazz = get_class($this->model);
         $otherModel = 
             call_user_func(array($clazz.'Peer', 'retrieveBy'.$this->enStr.'Slug'), $this->slug, $this->model->getId());
                 
         $slug = $otherModel ? $this->genRandomSlug($this->slug): $this->slug;
         call_user_func(
            array(
               $this->model, 'setSeo'.$this->enStr.'Slug'), 
            $slug); 

      }

      protected function filterSlug($slug) {
         if(!$slug) {
            return;
         }
         $arr = explode('/', $slug);
         foreach($arr as $i => $sl) {
            if(strpos($sl, '.') !== false) {
               continue;
            }
            $arr[$i] = myUtil::strToSlug($sl);
         }
         return implode('/', $arr);
      }

      protected function processFirstCreate() {
         $oldRoute = $this->getOldRoute();

         //tao moi
         if(!$oldRoute) {
             $this->createAutoRoute();
         }
         //xu ly duplicate
         else {
             $this->processUnique($oldRoute);
         }
      }

      protected function updateDataToModel() {
           call_user_func(
            array($this->model, 'setSeo'.$this->enStr.'RouteId'), 
            $this->hasRouteId ? $this->routeData['route_id']: 0);

         call_user_func(
            array($this->model, 'setSeo'.$this->enStr.'Slug'), 
            $this->slug);

         call_user_func(
            array($this->model, 'setSeo'.$this->enStr.'Title'), 
            $this->routeData['title']);

         call_user_func(
            array($this->model, 'setSeo'.$this->enStr.'Description'), 
            $this->routeData['description']);
         $this->model->save();
      }

      function getOldRoute() {
         $c = new Criteria;
         $c->add(HRoutePeer::MODEL, get_class($this->model));
         $c->add(HRoutePeer::PARAMS, 'id:'.$this->model->getId());
         $cr = $this->enStr?Criteria::LIKE:Criteria::NOT_LIKE;
         $c->add(HRoutePeer::NAME, '%_en', $cr);
        return HRoutePeer::doSelectOne($c);
    }

    function createAutoRoute() {
        
        $clazz = get_class($this->model);
        $id = $this->model->getId();

        $hr = new HRoute;
        $hr->setDescription($clazz.' '.trim(preg_replace('#[^0-9a-z]#', ' ', $slug)).' auto');
        $hr->setModel($clazz);
        $hr->setParams('id:'.$id.($this->enStr?'|lang:en':''));
        $hr->setRequirements('id: [0-9]+');
        $hr->setModels('id: '.$clazz.'Peer.retrieveByPk');
        $hr->setModule($this->module ? $this->module : 'index');
        $hr->setAction($this->action ? $this->action : substr(strtolower($clazz), 1));

        $name = strtolower($clazz).'_'.$id.'_auto';
        $name .= $this->enStr?'_en':'';

        $hr->setName($name);

        //check unique parttern
        $this->processUnique($hr);

        $hr->setPriority(1);
        $hr->setAddCache(1);
        $hr->save();
        call_user_func(array($this->model, 'setSeo'.$this->enStr.'RouteId'), $hr->getId());
        $this->model->save();
        
        return $hr;
    }

    function processUnique($hr) {
        
        $parttern = call_user_func(array($this->model, 'getSeo'.$this->enStr.'Slug'));
        
        $checkRoute = HRoutePeer::retreiveByParttern($parttern, $hr->getId());
        
        $parttern = $checkRoute? HRoutePeer::genRandomSlug($parttern): $parttern;
        $hr->setParttern($parttern);
        call_user_func(array($this->model, 'setSeo'.$this->enStr.'Slug'), $parttern);
        
        $this->model->save();
        $hr->save();
    }

    function genRandomSlug($parttern) {
        return $parttern.'-'.rand(1, 100);
    }

   }