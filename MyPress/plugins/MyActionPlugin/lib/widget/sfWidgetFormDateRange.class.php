<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormDateRange represents a date range widget.
 *
 * @package    symfony
 * @subpackage widget
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id$
 */
class sfWidgetFormDateRange extends sfWidgetForm
{
  /**
   * Configures the current widget.
   *
   * Available options:
   *
   *  * from_date:   The from date widget (required)
   *  * to_date:     The to date widget (required)
   *  * template:    The template to use to render the widget
   *                 Available placeholders: %from_date%, %to_date%
   *
   * @param array $options     An array of options
   * @param array $attributes  An array of default HTML attributes
   *
   * @see sfWidgetForm
   */
  protected function configure($options = array(), $attributes = array())
  {
    $this->addOption('from_date');
    $this->addOption('to_date');

    $this->addOption('template', 
        '<div class="col col-6 pdl0">%from_date%</div>'
        . '<div class="col col-6 pdl0">%to_date%</div>');
  }

  /**
   * Renders the widget.
   *
   * @param  string $name        The element name
   * @param  string $value       The date displayed in this widget
   * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   * @param  array  $errors      An array of errors for the field
   *
   * @return string An HTML tag string
   *
   * @see sfWidgetForm
   */
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
      
    $value = array_merge(array('from' => '', 'to' => ''), is_array($value) ? $value : array());
    $fromDate = new sfWidgetFormInput();
    $toDate = new sfWidgetFormInput();
    
    return strtr($this->translate($this->getOption('template')), array(
      '%from_date%'      => $fromDate->render($name.'[from]', $value['from'], array('placeholder' => 'From')),
      '%to_date%'        => $toDate->render($name.'[to]', $value['to'], array('placeholder' => 'To')),
    ));
  }


  protected  $parent = null;
  protected function translate($text, array $parameters = array())
  {
    if (null === $this->parent)
    {
      return $text;
    }
    else
    {
      return $this->parent->getFormFormatter()->translate($text, $parameters);
    }
  }

  /**
   * Translates all values of the given array.
   *
   * @param  array $texts       The texts with optional placeholders
   * @param  array $parameters  The values to replace the placeholders
   *
   * @return array              The translated texts
   * 
   * @see sfWidgetFormSchemaFormatter::translate()
   */
  protected function translateAll(array $texts, array $parameters = array())
  {
    if (null === $this->parent)
    {
      return $texts;
    }
    else
    {
      $result = array();

      foreach ($texts as $key => $text)
      {
        $result[$key] = $this->parent->getFormFormatter()->translate($text, $parameters);
      }

      return $result;
    }
  }
}
