<?php

    abstract class PlugMyLoggedAction extends PlugMyAction {
        
        public function validate() {
            if(!PlugLogin::getUserModel()) {
                $this->redirect('@homepage');
                return false;
            }
            return true;
        } 
    }