<?php
                
    abstract class PlugMyAction extends sfAction {
        

        public function isPost() {
            return $this->getRequest()->getMethod() == sfRequest::POST;
        }
        
        public function isAjax() {
            return $this->getRequest()->isXmlHttpRequest();
        }
        
        public function handleError() {
            $r = $this->getRequest();
            if($r->isXmlHttpRequest()) {
                return $this->renderJson(array(
                    'code' => 1, 
                    'errors' => $r->getErrors()));
            }
            else {
                $this->setTemplate($this->getErrorTemplate());
                return sfView::SUCCESS;
            }
        }
        
        protected function getErrorTemplate() {
            return $this->getActionName();
        }
        
        public function renderJson($data) {
            
            return $this->renderText(json_encode($data));
        }
    }