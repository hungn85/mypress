<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorString validates a string. It also converts the input value to a string.
 *
 * @package    symfony
 * @subpackage validator
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfValidatorString.class.php 12641 2008-11-04 18:22:00Z fabien $
 */
class PlugMyValidatorSmt extends sfValidatorBase
{
  /**
   * Configures the current validator.
   *
   * Available options:
   *
   *  * max_length: The maximum length of the string
   *  * min_length: The minimum length of the string
   *
   * Available error codes:
   *
   *  * max_length
   *  * min_length
   *
   * @param array $options   An array of options
   * @param array $messages  An array of error messages
   *
   * @see sfValidatorBase
   */
  protected function configure($options = array(), $messages = array()) {
//    $this->addMessage('max_length', '"%value%" is too long (%max_length% characters max).');
//    $this->addMessage('min_length', '"%value%" is too short (%min_length% characters min).');
//
//    $this->addOption('max_length');
//    $this->addOption('min_length');
//
//    $this->setOption('empty_value', '');
  }

  /**
   * @see sfValidatorBase
   */
  
    protected function doClean($value) {
        
        $clean = (string) $value; 
        $clazzs = is_array($this->clazz)?$this->clazz:array($this->clazz);
        $methods = is_array($this->method)?$this->method:array($this->method); 
        $argss = $this->args&&is_array($this->args[0])?$this->args:array($this->args);
        foreach($argss as $k => $args) {
            $argss [$k] = array_merge(array($value), $args);
        }
        
        $existss = is_array($this->exists)?$this->exists:array($this->exists);
         
        foreach($clazzs as $k => $clazz) {
            $method = $methods[$k];
            $args = $argss[$k];
            $exists = $existss[$k];
             
            $ret = call_user_func_array(array($clazz, $method), $args);
            if ((!$ret && $exists) || ($ret && !$exists)) {
                throw new sfValidatorError($this, $method.'_invalid', array('value' => $value, $method.'_invalid' => $this->getOption($method.'_invalid')));
            }
        }
        return $clean;
    }
    
    protected $clazz;
    public function setClazz($v) {
        $this->clazz = $v;
    }
    
    protected $method;
    public function setMethod($v) {
        $this->method = $v;
    }
    
    protected $args = array();
    public function setArgs($v) {
        $this->args = $v;
    }
    
    protected $exists = true;
    public function setExists($v) {
        $this->exists = $v;
    }
    
}
