<?php

class PlugMyValidatorPropelUniquePhoneNumber extends sfValidatorPropelUnique
{
    protected function doClean($values) {
        $columns = $this->getOption('column');
        foreach($columns as $col) {
            $values[$col] = myUtil::phoneNumberValidTo84($values[$col]);
        }
        return parent::doClean($values);
    }
}