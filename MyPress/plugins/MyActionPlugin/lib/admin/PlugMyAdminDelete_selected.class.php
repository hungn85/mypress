<?php

abstract class PlugMyAdminDelete_selected extends PlugMyAdminModel {
    public function execute() {
        $ids = $this->getRequestParameter($this->getIdsParam(), array());

        try {
            foreach (call_user_func(array($this->getPeerName(), 'retrieveByPks'), $ids) as $object) {
                $this->deleteModel($object);
            }
        } catch (PropelException $e) {
            $this->getRequest()->setError('delete', $e->getMessage());
            return $this->forward($this->getModuleName(), 'list');
        }

        return $this->redirect($this->getModuleName() . '/list');
    }
}
