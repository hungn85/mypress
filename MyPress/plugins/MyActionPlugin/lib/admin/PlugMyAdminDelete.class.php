<?php

abstract class PlugMyAdminDelete extends PlugMyAdminModel {
 
    public function execute($r) {

        $model = call_user_func(array($this->getPeerName(), 'retrieveByPk'), $this->getRequestParameter($this->getIdParam()));
        
        $this->forward404Unless($model);

        try {
            $this->deleteModel($model);
        } catch (PropelException $e) {
            $this->getUser()->setFlash('error', $e->getMessage()); 
        }
 
        return $this->renderText('<META http-equiv="refresh" content="0;URL='.$this->getListUri().'"> ');
    } 
     
}
