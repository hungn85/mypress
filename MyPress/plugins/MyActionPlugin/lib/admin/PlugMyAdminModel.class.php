<?php

    abstract class PlugMyAdminModel extends PlugMyLoggedAction {
         
        
        protected function getMuser() {
            return PlugLogin::getUserModel(false);
        }
        
        protected function getEditUri() {
            return myUtil::url_for($this->getModuleName() . '/edit');
        }

        protected function getDeleteUri() {
            return myUtil::url_for($this->getModuleName() . '/delete');
        }

        protected function getListUri() {
            return myUtil::url_for($this->getModuleName().'/index');
        }
        
        protected function getIndexUri() {
            return $this->getListUri();
        }
        
        abstract protected function getModelName();
        
        protected function getFixIcons() {
            return $icons = array(
                'phone_number' => 'mobile',
                'phone' => 'mobile',
                'email' => 'inbox',
                'address' => 'map-marker',
                'fullname' => 'user',
                'full_name' => 'user');
        }
        
        
        public static function getFixTitles() {
            return  array(
                'phone_number' => 'Số điện thoại',
                'phone' => 'Số điện thoại',
                'email' => 'Email',
                'address' => 'Địa chỉ',
                'fullname' => 'Tên đầy đủ',
                'full_name' => 'Tên đầy đủ',
                'mobile_operator' => 'Mạng',
                'message' => 'Nội dung',
                'created_at' => 'Thời gian tạo', 
                'updated_at' => 'Thời gian cập nhật');
        }
        
        public static function translateTitle($column, $label) {
            $titles = self::getFixTitles();   
            $arr = explode('_', $column);
            $arr[0] = ucfirst($arr[0]);
            $newLabel = implode(' ', $arr);
            if($newLabel == $label && isset($titles[$column])) {
                return $titles[$column];
            }
            return $label;
        }
        
        protected function getIcon($col) {
            $icons = $this->getFixIcons();
             
            $fixIcon = 'info-circle';
            $calendarIcon = 'calendar';
            if(in_array($col, array_keys($icons))) {
                return $icons[$col];
            }
            elseif(strpos($col, 'date') !== false || $col == 'dob') {
                return $calendarIcon;
            }
            else {
                return $fixIcon;
            } 
        }
        
        protected function getFormModelName() {
            return $this->getModelName().'Form';
        }
    
        protected function deleteModel($model) {
            $model->delete();
        }

        protected function getIdParam() {
            return 'id';
        } 
        
        protected function getIdsParam() {
            return 'ids';
        }
        
        protected function getPeerName() {
            return $this->getModelName() . 'Peer';
        }

        protected function getBaseNs() {
            return 'mrhero_admin';
        }

        protected function getNs() {
            return $this->getBaseNs() . '/' . $this->getModuleName();
        }
    }
