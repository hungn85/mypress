<?php

abstract class PlugMyAdminList extends PlugMyAdminModel {

    abstract protected function getPageTitle();

    abstract protected function getFilterColumns();

    protected function addFiltersCriteria($c) {
        
    }

    protected function genRowData($item) {

        $cols = $this->getColumTitles();
        $ret = array();
        foreach (array_keys($cols) as $col) {
            if ($col != 'actions') {
                $method = 'get' . sfInflector::camelize(strtolower($col));
                $ret[$col] = call_user_func(array($item, $method));
            }
        }
        
        if (isset($cols['actions'])) {
            $idPr = $this->getIdParam();
            $ret['actions'] = 
                '<div class="tbody_actions">'.
                '   <a href="' . $this->getEditUri() . '?'.$idPr.'=' . $item->getId() . '"><i title="Sửa" class="fa fa-edit"></i> </a>' .
                '   <i title="Xóa" data-delete-url="' . $this->getDeleteUri() . '?'.$idPr.'=' . $item->getId() . '" class="fa fa-remove"></i>'.
                '</div>';
        }
        return $ret;
    }

    

    protected function processFilters() {
        if ($this->getRequest()->hasParameter('filter')) {
            $peer = $this->getPeerName();
            $filters = $this->getRequestParameter($peer::TABLE_NAME);
            $this->getUser()->getAttributeHolder()->removeNamespace($this->getNs());
            $this->getUser()->getAttributeHolder()->removeNamespace($this->getFilterNs());
            $this->getUser()->getAttributeHolder()->add($filters, $this->getFilterNs());
        }
    }

    public function execute($r) {

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll($this->getFilterNs());

        // pager
        $this->pager = new sfPropelPager($this->getModelName(), $this->getLimit());
        $c = new Criteria();
        $this->addSortCriteria($c);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);

//        $this->pager->setPage($this->getRequestParameter($this->getPageParam(), $this->getUser()->getAttribute($this->getPageParam(), 1, $this->getNs())));
        $this->pager->setPage($this->getRequestParameter($this->getPageParam(), 1));
        $this->pager->init();

//        if ($this->getRequestParameter($this->getPageParam())) {
//            $this->getUser()->setAttribute($this->getPageParam(), $this->getRequestParameter($this->getPageParam()), $this->getNs());
//        }

        $this->rows = $this->processData();

        $this->paging();

        $this->column_titles = $this->getColumTitles();

        $this->filter_columns = $this->processFilterColumns();

        $this->page_title = $this->getPageTitle();

        $this->list_uri = $this->getListUri();
        
        $this->filter_form = $this->getFilterForm();
         
        $this->form = $this->getForm();
        
        $this->edit_uri = $this->getEditUri();
        
        $this->actions = $this->getActions(); 
        
    }

    protected function getActions() {
        return array (
            'create' => array('icon' => 'plus-square', 'label' => 'Tạo mới', 'uri' => $this->getEditUri()));
    }
    
    protected function processFilterColumns() {
        
        $form = $this->getFilterForm();  
        $cols = $this->getFilterColumns(); 
        
        $newCols = array();
        
        if(!$cols) {
            return $newCols;
        }
        
        foreach($cols as $col => $v ) {
            if(!is_array($v)) {
                $col = $v;
            }
            
            if(!isset($cols[$col]['label'])) {
                $newCols [$col]['label'] = $form[$col]->renderLabelName();
            }
            
            if(!isset($cols[$col]['icon'])) {
                
                $newCols[$col]['icon'] = $this->getIcon($col);
            } 
            
            if(!isset($cols[$col]['placeholder'])) { 
                $schema = $form->getWidgetSchema();
                $newCols[$col]['placeholder'] = $schema[$col]->getAttribute('placeholder');
            }
            else {
                $newCols[$col]['placeholder'] = $cols[$col]['placeholder'];
            }
        }
         
        return $newCols;
    }
    
    protected function getColumTitles() {
         
        $form = $this->getForm();
        
        $ret = array();
        foreach ($this->getFields() as $col) {
            $ret [$col] = $form[$col]->renderLabelName();
        }
        $ret['actions'] = 'Actions';
        return $ret;
    }

    protected function getForm() {
        if(!$this->form) {
            $fm = $this->getFormModelName(); 
            $this->form = new $fm(null);
            $this->form->getValidatorSchema()->setOption('allow_extra_fields', true);
            $this->form->getValidatorSchema()->setOption('filter_extra_fields', true);	
        }
        return $this->form;
    }
    
    protected function getFilterForm() {
        if(!$this->filter_form) {
            $fm = $this->getFilterFormModelName(); 
            $this->filter_form = new $fm(null);
            $this->filter_form->getValidatorSchema()->setOption('allow_extra_fields', true);
            $this->filter_form->getValidatorSchema()->setOption('filter_extra_fields', true);	
        }
        return $this->filter_form;
    }
    
    protected function getFilterFormModelName() {
        return $this->getFormModelName();
    }


    protected function getFields() {
        
        $form = $this->getForm();
        $schemas = $form->getWidgetSchema();
        return array_keys($schemas->getFields());
    }
    
    protected function processData() {
        $data = array();
        foreach ($this->pager->getResults() as $member) {

            $data [] = $this->genRowData($member);
        }
        return $data;
    }

    protected function paging() {
        if (!$this->pager->haveToPaginate()) {
            return;
        }

        $pages = $this->pager->getLinks();

        $ppr = $this->getPageParam();
        $this->page = $this->getRequestParameter($ppr, 1);

        $this->pages = array(
            array(
                'page' => '<i title="Trang đầu" class="fa fa-angle-double-left fa-2x"></i>',
                'link' => $this->getListUri() . '?' . $ppr . '=1'
            ),
            array(
                'page' => '<i  title="Trang trước" class="fa fa-angle-left fa-2x"></i>',
                'link' => $this->getListUri() . '?' . $ppr . '=' . $this->pager->getPreviousPage()
            )
        );

        foreach ($pages as $page) {
            $this->pages [] = array(
                'page' => $page,
                'link' => $this->getListUri() . '?' . $ppr . '=' . $page
            );
        }

        $this->pages = array_merge(
                $this->pages, array(
            array(
                'page' => '<i  title="Trang sau" class="fa fa-angle-right fa-2x"></i>',
                'link' => $this->getListUri() . '?' . $ppr . '=' . $this->pager->getNextPage()
            ),
            array(
                'page' => '<i  title="Trang cuối" class="fa fa-angle-double-right fa-2x"></i>',
                'link' => $this->getListUri() . '?' . $ppr . '=' . $this->pager->getLastPage()
            )
                )
        );
    }

    protected function processSort() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), $this->getSortNs());
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), $this->getSortNs());
        }

        if (!$this->getUser()->getAttribute('sort', null, $this->getSortNs())) {
            $this->getUser()->setAttribute('sort', $this->getIdParam(), $this->getSortNs());
            $this->getUser()->setAttribute('type', 'desc', $this->getSortNs());
        }
    }

    protected function addSortCriteria($c) {
        $sort_column = $this->getUser()->getAttribute('sort', null, $this->getSortNs());
        if ($sort_column) {
            $sort_column = sfInflector::camelize(strtolower($sort_column));
            $sort_column = call_user_func_array(
                    array($this->getPeerName(), 'translateFieldName'), array($sort_column, BasePeer::TYPE_PHPNAME, BasePeer::TYPE_COLNAME));

            if ($this->getUser()->getAttribute('type', null, $this->getSortNs()) == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function getPageParam() {
        return 'page';
    }

    protected function getFilterNs() {
        return $this->getBaseNs() . '/' . $this->getModuleName().'_'.$this->getActionName() . '/filters';
    }

    protected function getSortNs() {
        return $this->getBaseNs() . '/' . $this->getModuleName().'_'.$this->getActionName() . '/sort';
    }

    protected function getLimit() {
        return 20;
    }

}
