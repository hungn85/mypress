<?php

abstract class PlugMyAdminEdit extends PlugMyAdminModel {
    
    protected function getActions() {
        return array (
            'right' => array(
                'save' => array('label' => 'Lưu', 'icon' => 'save'),
                'save_and_add' => array('label' => 'Lưu và Tạo mới', 'icon' => 'plus-square-o')),
            'left' => array(
                'back' => array('label' => 'Quay lại', 'icon' => 'rotate-left', 'uri' => $this->getListUri()) )
        );
    }
    protected function hasLabel() {
        return true;
    }
    
    protected function getIcons() { 
        return $this->getFixIcons();
    }
    
    protected function getCreateTitle() {
        return 'Tạo mới';
    }
    
    protected function getEditTitle() {
        return 'Sửa thông tin';
    }
    
    public function validate() {
        $this->has_label = $this->hasLabel();
        $this->icons = $this->getIcons();
        $this->create_title = $this->getCreateTitle();
        $this->edit_title = $this->getEditTitle();
        $this->list_uri = $this->getListUri();
        $this->actions = $this->getActions(); 
        $this->form = $this->getForm(); 
        $valid = parent::validate();
        if(!$valid) {
            return false;
        }
         
        if(!$this->isPost()) {
            return true;
        }
        
        $tableName = $this->getTableName();
       
        $form = $this->getForm(); 
        $form->bind($this->getRequestParameter($tableName), $this->getRequest()->getFiles($this->getTableName($tableName)));
         
        return $form->isValid();
    }
    
    protected function getTableName() {
        $model = $this->getModelName();
        $peer = new $model();
        $peer = $peer->getPeer();
        return $peer::TABLE_NAME;
    }
    
    protected function saveForm() {
        $this->getForm()->save();
        $this->saveFileMethod();
    }
    
    protected function saveFileMethod() {
        $object = $this->getForm()->getObject();
        foreach($this->getFileMethods() as $method => $k) {
            $setMethod = 'set'.$method;
            $getMethod = 'get'.$method;
            $object->$setMethod('/uploads/'.str_replace(sfConfig::get('sf_upload_dir').'/', '', $object->$getMethod()));
        }
        $object->save();
    }


    protected function getForm() {
        if($this->form === null) {
            $form = $this->getFormModelName();
            $this->form = new $form($this->getModelOrCreate());
            $this->form->getValidatorSchema()->setOption('allow_extra_fields', true);
            $this->form->getValidatorSchema()->setOption('filter_extra_fields', true);	
        }
        return $this->form;
    } 
     
    
    protected function getSaveSuccessMsg() {
        return 'Cập nhật thành công';
    }
    
    public function execute($request) {
 
        if ($this->getRequest()->isMethod('post')) {
  
            try {  
                $this->saveFile(); 
                
                $this->saveForm();  
                
                
                $this->getUser()->setFlash('success', $this->getSaveSuccessMsg());

                if ($request->hasParameter('save_and_add')) {
                    return $this->redirect($this->getModuleName() . '/create');
                 
                } else {
                    
                    return $this->redirect($this->getAfterSaveUrl());
                }
            } catch (PropelException $e) {
                $this->getRequest()->setError('error', $e->getMessage()); 
            } 
        }  
    } 
    
    protected function saveFile() {
        
        $object = $this->getForm()->getObject();
         
        $pathAfterUpload = '/'.$this->getTableName();
        $folder = sfConfig::get('sf_upload_dir').$pathAfterUpload;

        foreach($this->getFileMethods() as $method => $sfValidatedFile) {
            $getMethod = 'get'.$method;  
            if(!$object->isNew()) { 
                $path = sfConfig::get('sf_web_dir').$object->$getMethod(); 
                is_file($path)?@unlink($path):null;
            } 

            $filename = date('Ymd').'_'.rand(1000, 9999).'_'.$sfValidatedFile->getOriginalName().$sfValidatedFile->getExtension($sfValidatedFile->getOriginalExtension());

            $sfValidatedFile->save($folder.'/'.$filename);
        }
    }
    
    protected $fileMethods = array();
    protected function getFileMethods() {
        if(!$this->fileMethods) {
            $form = $this->getForm();
            $schemas = $form->getWidgetSchema();
             
            foreach($schemas->getFields() as $col => $schema) {

                //tim tat ca cac truong la file
                if($schema instanceof sfWidgetFormInputFile) {

                    if( $sfValidatedFile = $form->getValue($col) ) {

                        $this->fileMethods [sfInflector::camelize(strtolower($col))] =  $sfValidatedFile;
                        
                    } 
                }
            }
        }
        return $this->fileMethods;
    }
    
    
    
    protected function getAfterSaveUrl() {
        return $this->getModuleName() . '/edit?id=' . $this->getForm()->getObject()->getId();
    }
    
    protected function getModelOrCreate() {

        $id = $this->getRequestParameter($this->getIdParam());
        if ($id === '' || $id === null) {
            $modelName = $this->getModelName();
            $model = new $modelName();
        } else {
            $model = call_user_func(array($this->getPeerName(), 'retrieveByPk'), $id);
            $this->forward404Unless($model);
        }

        return $model;
    }
    
    public function handleError() {  
        return parent::handleError();
    }
    
    protected function saveModel($model) {
        $model->save();
    }

    public static function saveModelManyToMany($model, $manyModelName, $manyModelColName, $manyOtherColName, $requestParamName ) {
        // Update many-to-many for "categorys"
        $peer = $manyModelName.'Peer';
        $col = call_user_func_array(array($peer, 'translateFieldName'), array($manyModelColName, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME ));
        $c = new Criteria();
        $c->add($col, $model->getPrimaryKey());
        call_user_func(array($peer, 'doDelete'), $c);
        
//        $ids = $this->getRequestParameter('associated_categorys');
        $ids = sfContext::getInstance()->getRequest()->getParameter($requestParamName);
        
        if (is_array($ids))
        {
            
          $method = call_user_func_array(array($peer, 'translateFieldName'), array($manyModelColName, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_PHPNAME  ));
          $otherMethod = call_user_func_array(array($peer, 'translateFieldName'), array($manyOtherColName, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_PHPNAME ));  
          foreach ($ids as $id) {
            $manyModel = new $manyModelName();
            call_user_func(array($manyModel, 'set'.$method), $model->getPrimaryKey());
            call_user_func(array($manyModel, 'set'.$otherMethod), $id); 
            $manyModel->save();
          }
        }
    }
}
