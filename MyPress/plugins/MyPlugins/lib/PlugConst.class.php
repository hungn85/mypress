<?php
                
    class PlugConst {
        
        const REG_SUCCESS = 
            'Bạn đã đăng ký tài khoản thành công</br>Hệ thống vừa gửi tin nhắn mật khẩu đăng nhập tới số %s';
        //const REG_SUCCESS_SMS = "[HOST] thong bao:\nMK dang nhap cua ban la: %s";
        const REG_SUCCESS_SMS = "Thong bao:\nMK dang nhap cua ban la: %s";
        const MT_TYPE_PENDING = 1;
        const MT_TYPE_SENDING = 2;
        const MT_TYPE_DONE = 3;
        
        const RESET_MSG = 'Hệ thống vừa gửi tới số điện thoại %s mật khẩu đăng nhập mới';
        
        const CHANGE_PASSWORD_SUCCESS = 'Chúc mừng bạn, bạn đã đổi mật khẩu thành công';
        
        public static function get($v, $options = array()) {
            
            $ret = constant('PlugConst::'.strtoupper($v));
            if($ret) {
                $ret = str_replace(
                    array('[HOST]'), 
                    array(ucfirst($_SERVER['HTTP_HOST'])), 
                    $ret);
                if($options) {
                    
                    $ret =  vsprintf($ret, $options);
                     
                }
                return $ret;
            }
        }
         
    }