﻿<?php
    
    class PlugMyCart {

        //cart
        const CART_NS = 'CART_NS'; 
        const CART_ITEM_ATTR = 'ID'; 
        const CUSTOMER_ATTR = 'CUSTOMER';
        protected $myUser;
        protected static $PlugMyCart = null;
        public static function getInstance() {
           if(self::$PlugMyCart === null) {
            self::$PlugMyCart = new PlugMyCart(sfContext::getInstance()->getUser());
           }
           return self::$PlugMyCart;
        }

        public function __construct($myUser) {
            $this->myUser = $myUser;
        }

        public function getCarts() {
          return $this->myUser->getAttribute(self::CART_ITEM_ATTR, array(), self::CART_NS);
        }

        public function getCartCount() {
            $total = 0;
            foreach($this->getCarts() as $rr) {
                $total += sizeof($rr);
            }
            return $total;
        }

        public function getCartTotalPrice() {
          $total = 0;
          
          foreach($this->getCarts() as $rr) {
             
            foreach($rr as $size => $r) {
                //echo $r['item']->getPrice().'|';
             $total += $r['item']->getPrice()*$r['count'];
            }
          }
          return $total;
        }

        public function getCartTotalAndShipPrice() {
          
          return $this->getCartTotalPrice()+$this->getShipCost();
        }

        public function getShipCost() {
            return floatval(ConfigPeer::get('SHIPPING_COST'));
        }

        public function getCartTotalPriceFormat() {
          
          return number_format($this->getCartTotalPrice(), 0, 0, '.');
          
        }

        public function addCart($item, $count = 1, $size = 0, $update = false) {
           
            $carts = $this->getCarts();
            $id = $item->getId();
            if(!isset($carts[$id][$size])) {
                $carts[$id][$size]['count'] = 0;
                $carts[$id][$size]['item'] = $item;
            }
            
            if(!$update) {
                $carts[$id][$size]['count'] += $count; 
            }
            else {
                $carts[$id][$size]['count'] = $count; 
            }
            $this->updateCart($carts);
        }

        public function updateCart($items) {
            $this->myUser->setAttribute(self::CART_ITEM_ATTR, $items, self::CART_NS);
        }

        public function clear() {
            $this->myUser->getAttributeHolder()->removeNamespace(self::CART_NS);
        }

        public function delete($id, $size = 0) {
            $items = $this->getCarts();
            unset($items[$id][$size]);
            $this->updateCart($items);
        }

        public function saveCustomerInfoToSession($data) {
            $this->myUser->setAttribute(self::CUSTOMER_ATTR, $data, self::CART_NS);
        }

        public function getCustomerInfoFromSession() {
            return $this->myUser->getAttribute(self::CUSTOMER_ATTR, array(), self::CART_NS);
        }

        public function saveToDb($data) {
            
            $items = $this->getCarts();
            if(!$items) return;
            
            $fullname = $data['fullname'];
            $email = $data['email'];
            $phone = $data['phone_number'];
            $city = $data['city'];
            $stateId = $data['state_id'];
            $address = $data['address'];
            $zipCode  = $data['zip_code'];
            $other = $data['note'];

            $cart = new Cart();
            $cart->setFullname($fullname);
            $cart->setEmail($email);
            $cart->setPhone($phone);
            $cart->setCity($city);
            $cart->setStateId($stateId);
            
            $cart->setAddress($address);
            $cart->setZipCode($zipCode);
            $cart->setOther($other);
            $cart->setCost($this->getCartTotalPrice());
            $cart->setShipCost($this->getShipCost());
            $cart->setTotalCost($this->getCartTotalAndShipPrice());
            $cart->save();
            
            foreach($items as $rr) {
                foreach($rr as $size => $dat) {
                    $item = $dat['item'];
                    $price = $item->getPrice();
                    $qty = $dat['count'];
                    $cost = $price*$qty;

                    $ci = new CartItem();
                    $ci->setCart($cart);
                    $ci->setSizeId($size);
                    $ci->setItem($item);
                    $ci->setItemCount($qty);
                    $ci->setPrice($item->getPrice());
                    $ci->setCost($cost); 
                    $ci->save();
                }
            }
            
            $this->clear();
            $this->myUser->setAttribute('cart', $cart);
            return $cart;
        }

        public function getCartDb() {
            return $this->myUser->getAttribute('cart');
        }
    }