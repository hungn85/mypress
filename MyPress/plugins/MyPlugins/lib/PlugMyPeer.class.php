<?php

   class PlugMyPeer {
      
      public static function getCriteria($peerClazzName, $c = null) {
         
         $c = $c?$c:(new Criteria);
         if(defined($peerClazzName.'::PRIORITY')) {
            $c->addAscendingOrderByColumn(constant($peerClazzName.'::PRIORITY'));
         }
         
         if(defined($peerClazzName.'::HIDE')) {
            $c->add(constant($peerClazzName.'::HIDE'), 0);
         }
         return $c;
      }
      
      public static function show($obj, $method) {
        $methodEn = $method.'En';
        if(method_exists($obj, $method)) {
          if(myUtil::isEn()) {
            if(method_exists($obj, $methodEn)) {
              return call_user_func(array($obj, $methodEn));
            }
          }
          return call_user_func(array($obj, $method));
        }
      }
      
      public static function slug($obj, $setSlugMethod, $getSlugMethods) {
        if(!is_array($getSlugMethods)) {
          $getSlugMethods = array($getSlugMethods);
        }

        $arr = array();
        foreach($getSlugMethods as $m) {
          if(method_exists($obj, $m)) {
            $arr [] = myUtil::strToSlug(call_user_func(array($obj, $m)));
          }
        }

        if(method_exists($obj, $setSlugMethod)) {
          return call_user_func(array($obj, $setSlugMethod), implode('-', $arr));
        }
      }
      
      public static function verifyRequestObject($object, $slug, $slugMethod = 'getSlugName') {
         if(method_exists($object, $slugMethod)) {
            $oSlug = call_user_func(array($object, $slugMethod));
            return $oSlug == $slug;
         }
         return true;
      }
   }