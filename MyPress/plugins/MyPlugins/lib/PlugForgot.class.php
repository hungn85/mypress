<?php

                
    class PlugForgot extends PlugAbstractUser {  
         
        public function validate() {
            $register = new PlugValidate(
                array(
                    'phone' => array(
                        'valids' => 'EMPTY|PHONE',
                        'empty_msg' => 'Bạn hãy nhập số điện thoại',
                        'phone_msg' => 'Số điện thoại không hợp lệ'
                    ),
                    'captcha' => array(
                        'valids' => 'EMPTY|CAPTCHA',
                        'empty_msg' => 'Bạn hãy nhập mã xác thực',
                        'captcha_msg' => 'Mã xác thực không hợp lệ'
                    ), 
                    'MODEL' => array (
                        array (
                            'key' => 'phone',
                            'class' => 'MuserPeer',
                            'peer_method' => 'checkPhoneExists',
                            'msg' => 'Số điện thoại không tồn tại',
                            'exists' => false)
                    )
                ));
            return $register->validate();
        }
        
        public function execute() {
            $phone84 = myUtil::phoneNumberValidTo84($this->params['phone']);
            $pas = myUtil::genPassword(); 
            $mt = PlugConst::get('REG_SUCCESS_SMS', array($pas));
            $muser = MuserPeer::checkPhoneExists($phone84);
            $muser->setPassword(md5($pas));
            $muser->setResetPassword(false);
            $muser->save();
            
            $q = new MtQueue();
            $q->sendMt($this->params['phone'], $mt);
        }
    }