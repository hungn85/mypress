<?php

                
    abstract class PlugAbstractUser {  
        
        protected $params;
            
        public function __construct($params = array()) {
            $this->R = $this->getRequest();
            $this->params = $params;
        }
        
        public function getUser() {
            return sfContext::getInstance()->getUser();
        }
        
        public function getRequest() {
            return sfContext::getInstance()->getRequest();
        } 
    }