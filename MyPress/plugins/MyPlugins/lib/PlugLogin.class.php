<?php

                
    class PlugLogin  extends PlugAbstractUser {
        const LOGIN_USER_NS = 'user';
        const LOGIN_USER_KEY = 'model';
        
        protected static $username, $password, $options; 
         
        public function login() {  
            self::logout();
            if(!self::getUserModel()) { 
                self::setUserModel($this->getMuser());
            }
            return self::getUserModel();
        }
        
        public static function logged() {
            return self::getUserModel();
        }
        
        public static function logout() {
            self::getStaticUser()->getAttributeHolder()->removeNamespace(self::LOGIN_USER_NS);
        }
        
        public static function getUserModel($session = true) {
            $user = self::getStaticUser();
            
            $muser = $user->getAttribute(self::LOGIN_USER_KEY, null, self::LOGIN_USER_NS);
            if($muser) {
                if($session) {
                    return $muser;
                }
                else {
                    return MuserPeer::retrieveByPK($muser->getId());
                }
            }
            return  null;
            
        }
        
        protected static function setUserModel($userModel) {
            $user = self::getStaticUser();
            return $user->setAttribute(self::LOGIN_USER_KEY, $userModel, self::LOGIN_USER_NS);
        }
        
        protected static function getStaticUser() {
            
            return sfContext::getInstance()->getUser();
        } 
        
        
        public function validate() {
            
            return true;
        }
        
        protected  $muser = null;
        public function getMuser() {
            if($this->muser === null) {
                $c = new Criteria;
                $c->add(MuserPeer::PHONE_NUMBER, myUtil::phoneNumberValidTo84(self::$username));
                $c->add(MuserPeer::PASSWORD, md5(self::$password));
                $this->muser = MuserPeer::doSelectOne($c);
            }
            return $this->muser;
        }
    }