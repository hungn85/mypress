<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HeaderMenu
 *
 * @author CHIP
 */

/*
  $menus = array (
        array('id' => '1', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu1.php', 'parent_id' => '0'),
        array('id' => '2', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu2.php', 'parent_id' => '1'),
        array('id' => '3', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu3.php', 'parent_id' => '2'),
        array('id' => '4', 'name' => 'LEVEL 4', 'link' => 'localhost/trangchu4.php', 'parent_id' => '3'),
        array('id' => '5', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu5.php', 'parent_id' => '0'),
        array('id' => '6', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu6.php', 'parent_id' => '0'),
        array('id' => '7', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu7.php', 'parent_id' => '6'),
        array('id' => '8', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu8.php', 'parent_id' => '6'),
        array('id' => '9', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu9.php', 'parent_id' => '6'),
        array('id' => '10', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu10.php', 'parent_id' => '9'),
        array('id' => '11', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu11.php', 'parent_id' => '9'),
        array('id' => '12', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu12.php', 'parent_id' => '9')
    );  
   
  $menu = new RecursiveMenu($menus);
  print_r($menu->build());
*/

class PlugHeaderMenu extends RecursiveMenu{
   
   //override
   protected function getRootUlClass(){ return 'sf-menu clearfix menu-content';} 
   
   //override
   protected function getFocusClass(){ return 'sfHoverForce';} 
   
   public static function buildMenu() {
      $menu = new PlugHeaderMenu(PlugDataMenu::getDatas());
      $menu->setFocusId(sfContext::getInstance()->getRequest()->getParameter('cid', 0));
       
      return $menu->build();
   }
   
   
}
