<?php

/*
  $menus = array (
        array('id' => '1', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu1.php', 'parent_id' => '0'),
        array('id' => '2', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu2.php', 'parent_id' => '1'),
        array('id' => '3', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu3.php', 'parent_id' => '2'),
        array('id' => '4', 'name' => 'LEVEL 4', 'link' => 'localhost/trangchu4.php', 'parent_id' => '3'),
        array('id' => '5', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu5.php', 'parent_id' => '0'),
        array('id' => '6', 'name' => 'LEVEL 1', 'link' => 'localhost/trangchu6.php', 'parent_id' => '0'),
        array('id' => '7', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu7.php', 'parent_id' => '6'),
        array('id' => '8', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu8.php', 'parent_id' => '6'),
        array('id' => '9', 'name' => 'LEVEL 2', 'link' => 'localhost/trangchu9.php', 'parent_id' => '6'),
        array('id' => '10', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu10.php', 'parent_id' => '9'),
        array('id' => '11', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu11.php', 'parent_id' => '9'),
        array('id' => '12', 'name' => 'LEVEL 3', 'link' => 'localhost/trangchu12.php', 'parent_id' => '9')
    );  
   
  $menu = new RecursiveMenu($menus);
  print_r($menu->build());
*/

class RecursiveMenu {
  
   protected $items; 

   //override for match your menu
   protected function getRootUlId(){ return 'menu';} 

   protected function getRootUlClass(){ return '';} 

   protected function getSubUlClass(){ return '';} 

   protected function getLiClass(){ return '';} 

   protected function getAClass(){ return '';} 

   protected function getLinkKey(){ return 'link';} 

   protected function getNameKey(){ return 'name';}

   protected function getIdKey(){ return 'id'; }

   protected function getParentIdKey(){ return 'parent_id';}
   
   protected function getSubIconHtml() { return '';}
   
   protected function getFocusClass() { return '';}
   
   public function __construct($items) {
     $this->items = $items; 
   }
   private $focus_id = null;
   public function setFocusId($v) {
      $this->focus_id = $v;
   }
   
   private $buildCount = 0;
   function build ( $parent = 0) {

      $rootUlClass = '';
      $rootUlId = '';

      if($this->buildCount == 0) {
        $rootUlClass = $this->getRootUlClass() ;
        $rootUlId =  $this->getRootUlId();
      }
      else {
         $rootUlClass = $this->getSubUlClass();
      }

      $this->buildCount ++;

      $hasChildren = false;
      $outputHtml = '<ul id="'.$rootUlId.'" class="'.$rootUlClass.'" >%s</ul>';
      $childrenHtml = '';

      foreach($this->items as $item) {
         if ($item[$this->getParentIdKey()] == $parent) {

            $nameKey = $this->getNameKey();
            $linkKey = $this->getLinkKey();
            $hasChildren = true;
            $id = $item[$this->getIdKey()];
            
            $focus = $this->focus_id && $this->focus_id == $id;
             
            $childrenHtml .= '<li data-id="'.$id.'" class="'.($focus?$this->getFocusClass():'').' '.$this->getLiClass().'">';

            $href = '';

            if($item[$linkKey]) {
              $href = 'href="'.$item[$linkKey].'"';
            }

            $a = '<a class="'.$this->getAClass().'" '.$href.'>'.$item[$nameKey].'</a>';         
             
            
            $childrenHtml .= $a;
            $r = $this->build ($id); 
            if($r) {
               $iconHtml = $this->getSubIconHtml();
               $childrenHtml .= $iconHtml.$r;
            }
            $childrenHtml .= '</li>';           
         }
      }

      // Without children, we do not need the <ul> tag.
      if (!$hasChildren) {
          $outputHtml = '';
      } 
      // Returns the HTML
      return sprintf($outputHtml, $childrenHtml);
   }
   
   protected function getPCIdKey() {
      return $this->getIdKey();
   }
   
   protected function getPCParentIdKey() {
      return $this->getParentIdKey();
   }
   
   function getPath( $parent = NULL, $idKey = 'id', $parentIdKey = 'parent_id', $maxLevel = -1, $currentLevel = 0) {
      $ids = array();
 
      $currentLevel ++; 
      foreach($this->items as $item) {
         if ($item[$idKey] == $parent && ($maxLevel == -1 || ($maxLevel != -1 && $currentLevel <= $maxLevel))) {

            $r = $this->getPath($item[$parentIdKey], $idKey, $parentIdKey, $maxLevel , $currentLevel); 
             
            $ids [] = $item;
            if($r) {
              $ids  = array_merge($ids, $r);
            }
         }
      }
      return $ids;
   }
   
   function getParents( $parent = NULL, $maxLevel = -1, $currentLevel = 0) {
       
      return $this->getPath($parent, 'id', 'parent_id', $maxLevel , $currentLevel );
   }
   
   function getChilds( $parent = NULL, $maxLevel = -1, $currentLevel = 0) {
       
      return $this->getPath($parent, 'parent_id', 'id', $maxLevel , $currentLevel );
   }

   public static function toArray(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = self::toArray($elements, $element['id']);
            if ($children) {
                $element['subs'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
  }
   
} 

 
