<?php
 

class PlugDataMenu extends RecursiveMenu {
   
    
   public static $datas = null;
   public static function getDatas() {
      if(self::$datas === null) {
         $c = PlugMyPeer::getCriteria('MenuCategoryPeer');
         $dats = MenuCategoryPeer::doSelect($c);
         $data = array();
         foreach($dats as $r) {
            $data [] = array(
                'id' => $r->getId(),
                'name' => $r->getShowName(),
                'link' => $r->getDetailUrl(),
                'parent_id' => $r->getParentId()
            );
         }

         self::$datas = $data;
      }
      return self::$datas;
   } 
    
}
