<?php 

abstract class AbstractFrontend extends PlugMyAction {

	public function execute($r) {
 		
 		$json = array();
		foreach($this->getReturnParams() as $k => $method) {
			$json [$k] = $this->$method();
		} 
		return $this->renderJson($json);
	}	
 	
 	protected $returnParams = array (
 		'menu' => 'getMenu',
 		'hot_posts' => 'getHotPosts',
 		'new_posts' => 'getNewPosts',
 	);

	protected function getReturnParams() {
		return $this->returnParams;
	}

	protected function addReturnParam($key, $method) {
		$this->returnParams[$key] = $method;
	}

	protected function addReturnParams($arr) {
		foreach($arr as $key =>$method) {
			$this->addReturnParam($key, $method);
		}
	} 

	protected function removeReturnParam($key) {
		unset($this->returnParams[$key]);
	}

	//get header menu
	protected function getMenu() {

		$key = 'top_menu';
		$ins = MyFileCache::getInstance();
		
		$data = array();
		if(!$ins->has($key)) {

			$json = array();
			$c = PlugMyPeer::getCriteria('HCategoryPeer');
			$c->addAscendingOrderByColumn(HCategoryPeer::ID);
			foreach(HCategoryPeer::doSelect($c) as $item) {
				 
				$json [] = array(
					'id' => $item->getId(), 
					'name' => $item->getName(),
					'link' => $item->getDetailUrl(), 
					'parent_id' => intval($item->getParentId())); 
			}
			$data = RecursiveMenu::toArray($json);
			$ins->set($key, $data);
 		}
 		else { 
 			$data = $ins->get($key);
 		}	
		return $data;
  		
	}

	protected function getHotPosts($page = 1) {
 		
 		$c = $this->getPostsCriteria($page); 
		$c->addDescendingOrderByColumn(HPostPeer::REAL_VIEW_COUNT);
		$posts = HPostPeer::doSelect($c);

		$data = array();

		foreach($posts as $post) {
			$data [] = $this->getPostData($post);
		}
		return $data;
	} 

	protected function getNewPosts($page = 1) {
 
		$c = $this->getPostsCriteria($page);
 		$c->addDescendingOrderByColumn(HPostPeer::ID);
		$posts = HPostPeer::doSelect($c);

		$data = array();

		foreach($posts as $post) {
			$data [] = $this->getPostData($post);
		}
		return $data;
	} 

	protected function getPostsCriteria($page = 1) {
		$mpp = HConfigPeer::get('POST_MAX_PER_PAGE');
		$c = PlugMyPeer::getCriteria('HPostPeer'); 
		$c->setLimit($mpp);
		if($page > 1) {
			$c->setOffset($mpp*($page-1));
		}
		return $c;
	}

	protected function getPostData($post) {
		return array (
			'id' => $post->getId(), 
			'datetime' => $post->getDateTime(),
			'name' => $post->getName(), 
			'img' => $post->getImg(),
			'link' => $post->getDetailurl(),
			'description' => $post->getDescription(),
			'view_count' => $post->getViewCount());
	}
}