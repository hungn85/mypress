<?php 
    
    class MyRoute {
        
        public static $myRoute = null;
        public static function getInstance() {
            if(self::$myRoute === null) {
                self::$myRoute = new MyRoute();
            }
            return self::$myRoute;
        }

        public function hasRouteId() {
            $params = $this->getParams();
            return isset($params['hroute_id']);            
        }

        public function getRequestObjects() {

            $params = $this->getParams(); 
            
            //get route object
            $route = HRoutePeer::retrieveByPK($params['hroute_id']);

            $retObjects = array('route' => $route);

            if($route) {
                $retObjects['hroute_id'] = $route;
                //extract param, modelName from route object
                $models = $route->getModels();
                preg_match_all('#[a-zA-Z0-9_\.]+#', $models, $m);
                
                $i = 0;
                $m = $m[0];

                while(true) {
                    if(!isset($m[$i]) || !isset($m[$i+1]))  {
                        break;
                    }
                    $k = $m[$i];
                    $modelNameMethod = $m[$i+1];
                    
                    list($modelName, $method) = explode('.', $modelNameMethod);
                    //echo$modelName.'::'.$method.'('.$params[$k].')';die();
                    // get object
                    if(isset($params[$k]) && method_exists($modelName, $method)) {
                        $retObjects[$k] = call_user_func(array($modelName, $method), $params[$k]);
                    }
                    $i+= 2;
                } 
            }
            return $retObjects;
        }

        protected function getParams() {
            $routing = sfContext::getInstance()->getRouting();
            // getRoute
            $uri = $routing->getCurrentInternalUri();
            $x = explode('?', $uri);
            if(sizeof($x) == 1) {
                return array();
            } 

            $uri = $x[1];
            //route param to array
            parse_str($uri, $params);
            return $params;
        }

        public static function parse($url) {
           return sfContext::getInstance()->getRouting()->parse($url);
        }

        public static function import() {
            include_once(sfConfig::get('sf_lib_dir').'/symfony/yaml/sfYaml.class.php');
          
            // Load the YAML file
            $array = sfYaml::load(sfConfig::get('sf_app_dir').'/config/routing.yml');
             
            // Check what we have got
            foreach($array as $routeName => $dat){
                if(in_array($routeName, array('default_symfony' , 'default_index', 'default')))
                    continue;

                $hr = new HRoute();
                $hr->setParttern($dat['url']);
                $hr->setName($routeName);
                $hr->setDescription($routeName);
                $params = $dat['param'];

                $hr->setModule($params['module']);

                unset($params['module']);

                $hr->setAction($params['action']);
                unset($params['action']);

                
                if($params) {
                    $strParams = '';
                    foreach($params as $k => $v) {
                      $strParams .= strlen($strParams) > 0 ? '|' : '';
                      $strParams .= $k.': '.$v;
                    }
                    $hr->setParams($strParams);
                }
                

                if(isset($dat['requirements'])) {
                    $rqs = $dat['requirements'];
                    $strRqs = '';

                    foreach($rqs as $k => $v) {
                        $strRqs .= strlen($strRqs) > 0 ? '|' : '';
                        $strRqs .= $k.': '.$v;
                    }
                    $hr->setRequirements($strRqs);
                }
                
                $hr->save();
              
            }
            return sfView::NONE;
        }
    }