<?php
	
	class ConditionalCacheFilter extends sfFilter {
		public function execute($filterChain) {	
			
			$context = $this->getContext();
			$user = $context->getUser();
			$cache = $context->getViewCacheManager();
			 
			//if (!$user->isAuthenticated()) 
			{
				foreach(frontendConfiguration::getAllRoutes() as $route) {
					if(!$route->getAddCache()) {
						continue;
					}
					$cache->addCache (
						$route->getModule(), 
						$route->getAction(), 
						array(
							'lifeTime' => 86400, 
							'withLayout' => true,));
				}
			}
			
			$filterChain->execute();
		}
	}