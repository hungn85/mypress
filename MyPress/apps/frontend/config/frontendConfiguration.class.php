<?php

class frontendConfiguration extends sfApplicationConfiguration
{
    public function configure() {
        $this->dispatcher->connect('routing.load_configuration', array($this, 'prependRoute')); 
    }
 
  public function prependRoute(sfEvent $event) {

      $routing = $event->getSubject();

      $c = new Criteria;
      $c->addAscendingOrderByColumn(HRoutePeer::PRIORITY);;
      
      $this->routes = array();
      $this->routeByNames = array();
      foreach(self::getAllRoutes() as $route) {
        
        $name = $route->getName();

        $this->routes [$route->getId()] = $route;

        $this->routeByNames [$name] = $route;

        $requirements = self::getToArray($route->getRequirements());

        $defaultValue = array('module' => $route->getModule(), 'action' => $route->getAction());
        
        $defaultValue = array_merge($defaultValue, self::getToArray($route->getParams()));
        $defaultValue = array_merge($defaultValue, array('hroute_id' => $route->getId()));

        $routing->prependRoute (
          $name,                                  // Route name
          $route->getParttern(),                                   // Route pattern
          $defaultValue, // Default values
          $requirements                             // Requirements
        );
      }
      
      //redirect if has new route
      $this->redirectIfHasNewRoute($routing);

    }

    public function redirectIfHasNewRoute($routing) {
      $file =  $_SERVER['SCRIPT_NAME'];
      
      $uri = $_SERVER['REQUEST_URI']; 
      
      if(strpos($uri, $file) === 0) {
        $uri = str_replace($file, '', $uri);
      }
      
      $info = $routing->parse($uri);
      if(isset($info['hroute_id'])) {
        $id = $info['hroute_id'];
        $r = $this->routes[$id];
        $routeName = $r->getName();

        $newRouteName = 'new_'.$routeName;

        if(isset($this->routeByNames[$newRouteName])) {
          header('location:'.$routing->generate($newRouteName, $info));
          die();
        }
      }
    }

    public static function getToArray($strs) {
      
      $data = array();
      if($strs) {
         $i = 0;
         foreach(explode('|', $strs) as $col) {
            if(strpos($col, ':') === false) {
               $data [$i+1] = $col;
               $i++;
            }
            else {
               list($k, $v) = explode(':', $col);
               $data [trim($k)] = trim($v);
            }
            
         }
      }
      return $data;
   }

    protected static $all_routes = array();
    public static function getAllRoutes() {
      if(!self::$all_routes) {

        $ins = MyFileCache::getInstance();
        $key = 'all_route';
        $data = array();
        if(!$ins->has($key)) {
          $c = new Criteria;
          $c->addDescendingOrderByColumn(HRoutePeer::PRIORITY);;
          $c->add (
            HRoutePeer::REQUIREMENTS, 
            'IFNULL('.HRoutePeer::REQUIREMENTS.', \'\') NOT LIKE \'%page_uri:%\'', 
            Criteria::CUSTOM);
          foreach(HRoutePeer::doSelect($c) as $route) {
            self::$all_routes [$route->getId()] = $route;
          }
          $ins->set($key, self::$all_routes);
        }
        else {
          self::$all_routes = $ins->get($key);
        }
        
        
      }
      return self::$all_routes;
      
    }
}
