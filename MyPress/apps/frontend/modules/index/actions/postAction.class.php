<?php
	
	class postAction extends PlugMyAction {

		public function execute($request) {

			$objects = MyRoute::getInstance()->getRequestObjects();
			
			$this->item = isset($objects['slug']) ? $objects['slug'] : $objects['id'];
			$this->forward404Unless($this->item);
			
			$this->item->setViewCount($this->item->getViewCount()+1);
			$this->item->setRealViewCount($this->item->getRealViewCount()+1);
			$this->item->save();

			$c = new Criteria;
			$c->setLimit(10);
			$this->items = HPostPeer::doSelect($c);
	    }
	}