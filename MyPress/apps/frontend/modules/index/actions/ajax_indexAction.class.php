<?php 
	 
	class ajax_indexAction extends AbstractFrontend {
		
		public function execute($r) {

			$this->maxPerPage = HConfigPeer::get('POST_MAX_PER_PAGE');
			$this->cid = $this->getRequestParameter('id', 0);
 			$this->page = intval($this->getRequestParameter('page', 1));

			$this->addReturnParams(array('posts' => 'getResult', 'loadAll' => 'isLoadAll'));
			return parent::execute($r);
		}

		protected function isLoadAll() {
			$posts = $this->getPosts();
			return sizeof($posts) < $this->maxPerPage ;
		}
 
 
		protected function getResult() {

 			$key = 'post_list_'.$this->cid.'_'.$this->page;
 			$ins = MyFileCache::getInstance();
 			$data = array();

 			if(!$ins->has($key)) {

 				$posts = $this->getPosts();

				foreach($posts as $post) {
					$data [] = $this->getPostData($post);
				}
				$ins->set($key, $data);
 			}
 			else {
 				$data = $ins->get($key);
 			}
			
			return $data;
		}

		protected function getPosts() {
			$c = PlugMyPeer::getCriteria('HPostPeer');
			$c->addDescendingOrderByColumn(HPostPeer::ID);

			if($this->cid) {
				$c->addJoin(HPostPeer::ID, HCategoryPostPeer::POST_ID);
				$c->addJoin(HCategoryPostPeer::CATEGORY_ID, HCategoryPeer::ID);
				$c->add(HCategoryPeer::ID, $this->cid);
			}
 			
			//paging
			$this->addPaging($c);
			return HPostPeer::doSelect($c);
			
		}

		protected function addPaging($c) { 
			$offset = $this->maxPerPage * ($this->page - 1); 
			$c->setLimit($this->maxPerPage);
			$c->setOffset($offset);
		}

		
	}