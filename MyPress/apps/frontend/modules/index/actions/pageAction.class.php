<?php
	
	class pageAction extends PlugMyAction {

		public function execute($request) {

			$objects = MyRoute::getInstance()->getRequestObjects();
			
			$this->item = $objects['id'];
			$this->forward404Unless($this->item);
			if($this->item->getId() == 1) {
				$this->setTemplate('page1');
			}
	    }
	}