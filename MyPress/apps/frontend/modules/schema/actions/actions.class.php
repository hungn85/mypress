<?php

/**
 * system actions.
 *
 * @package    sf_sandbox
 * @subpackage system
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 9301 2008-05-27 01:08:46Z dwhittle $
 */
class schemaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  const MODE_VIEW = 'VIEW';
  const MODE_PROCESS = 'PROCESS';
  private static $mode = '';
  public function getSchema() {
     return sfYaml::load(sfConfig::get('sf_config_dir').'/schema.yml');
  }
  public function executeIndex($r) {
     $array = $this->getSchema();
     $array = $array['propel'];
     unset($array['_attributes']);
     $this->schema = array();
     foreach($array as $tbl => $colds) {
      $this->schema [] = $tbl;
     }
        
     sort($this->schema);
  }
  
  public function executeBuild_model($r){
     chdir(sfConfig::get('sf_root_dir'));
     echo '<pre>';
     echo system ('php symfony propel-build-model');
     echo '<br><br>';
     echo system ('php symfony cc');
     echo '</pre>';
     //$this->clearApc();
     return sfView::NONE;
  }
  
  public function executeCc($r){
     chdir(sfConfig::get('sf_root_dir'));
     echo '<pre>';
     echo system ('php symfony cc');
     echo '</pre>';
     $this->clearApc();
     echo 'Done';
     return sfView::NONE;
  }
  
  public function clearApc() {
    if(function_exists('apc_clear_cache')) {
      //apc_clear_cache();
      apc_clear_cache('user');
      //apc_clear_cache('opcode');
     }
  }
  public function executeInit_admin($r){
     chdir(sfConfig::get('sf_root_dir'));
     $tbl = $r->getParameter('tbl');
     $arr = explode('_', $tbl);
     $model = '';
     foreach($arr as $ar) {
        $model .= ucfirst($ar);
     }
     echo '<pre>';
     echo system ('php symfony propel-init-admin backend '.$tbl.' '.$model);
     echo '</pre>';
     header('location:/backend.php/'.$tbl.'/list');
     //return sfView::NONE;
     return $this->renderText('<meta http-equiv="refresh" content="0; url=/backend.php/'.$tbl.'/list">');
  }
  
  public function executeSync($request)
  {
   // Include the symfony YAML library
   // include_once(sfConfig::get('sf_lib_dir').'/symfony/yaml/sfYaml.class.php');
      
      if($request->hasParameter('mode')) {
         self::$mode = $request->getParameter('mode');
      }
      echo '<a href="?mode='.self::MODE_PROCESS.'">Click Here</a> IF has New Column';
      //self::$mode = self::MODE_PROCESS;
      // Load the YAML file
      $array = sfYaml::load(sfConfig::get('sf_config_dir').'/schema.yml');
            
      $array = $array['propel'];
      unset($array['_attributes']);
      // Check what we have got
      foreach($array as $tbl => $cols) {
         foreach($cols as $c => $v) {
            if($c[0] == '_') {
               unset($cols[$c]);
            }
         }
         
         try {
            if(!self::tableExists($tbl)) {
               self::createTable($tbl);
            }
            
            $rs = self::describe($tbl);
            $fields = array();
            while($rs->next()) {
               $fields [] = $rs->get("Field");
            }
            $sc = array_keys($cols);
            $newCols = array_diff($sc, $fields);

            if($newCols) {

               foreach($newCols as $newCol) {
                  $type = isset($cols[$newCol]['type'])?$cols[$newCol]['type']:'';
                  if(!$type) { continue;}
                  $size = isset($cols[$newCol]['size'])?$cols[$newCol]['size']:0;
                  $default = isset($cols[$newCol]['default'])?$cols[$newCol]['default']:null;
                  self::addColumn($type, $tbl, $newCol, $size, $notNull = false, $default);
               }
            }
         }
         catch(Exception $e) {
            echo $msg = $e->getMessage();
        
         }
      }
  }
  
  public static function addColumn($type, $tbl, $col, $size = 200, $notNull = false, $default = '') {
     switch ($type) {
        case 'VARCHAR':
           self::addColumnVarchar($tbl, $col, $size, $default);
           break;
        case 'LONGVARCHAR':
           self::addColumnText($tbl, $col);
           break;
        case 'INTEGER':
           self::addColumnInt($tbl, $col, $size, $notNull, $default);
           break;
        case 'FLOAT':
           self::addColumnFloat($tbl, $col, $size, $notNull, $default);
           break;
        case 'BOOLEAN':
           self::addColumnBoolean($tbl, $col, $notNull, $default);
           break;
        case 'DATE':
           self::addColumnDate($tbl, $col, $notNull, $default);
           break;
        case 'TIME':
           self::addColumnTime($tbl, $col, $notNull, $default);
           break;
        case 'DATETIME':
           self::addColumnDateTime($tbl, $col, $notNull, $default);
           break;
        case 'TIMESTAMP':
           self::addColumnTimeStamp($tbl, $col, $notNull, $default);
           break;
        default: 
           echo '<b>'.$tbl.'.'.$col.'</b> not has type <B>'.$type.'</b>';
     }   
     
  }
  
  public static function addColumnVarchar($tbl, $col, $size, $default = '') {
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` VARCHAR('.$size.') '.($default?('DEFAULT \''.$default.'\''):'');
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     if(self::$mode == self::MODE_PROCESS) {
      return self::executeQuery($sql);
     }
  }
  
  
  public static function addColumnInt($tbl, $col, $size, $notNull = false, $default = '') {
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     $default = ($default!==null?('DEFAULT '.$default):'');
     $notNull = $notNull?'NOT NULL':'';
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` INT '.$notNull.' '.$default;
     if(self::$mode == self::MODE_PROCESS) {
      return self::executeQuery($sql);
     }
  }
  
  public static function addColumnFloat($tbl, $col, $size, $notNull = false, $default = '') {
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     $default = ($default!==null?('DEFAULT '.$default):'');
     $notNull = $notNull?'NOT NULL':'';
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` FLOAT '.$notNull.' '.$default;
     if(self::$mode == self::MODE_PROCESS) {
      return self::executeQuery($sql);
     }
  }
  
  public static function addColumnBoolean($tbl, $col, $notNull = false, $default = '') {
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     $default = ($default!==null?('DEFAULT '.$default):'');
     $notNull = $notNull?'NOT NULL':'';
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` BOOLEAN '.$notNull.' '.$default;
     if(self::$mode == self::MODE_PROCESS) {
         return self::executeQuery($sql);
     }
  }
  public static function addColumnDate($tbl, $col, $notNull = false, $default = '') {
     
      
      return self::addColumnDT($tbl, $col, 'DATE', $notNull, $default);
        
  }
  
  public static function addColumnTime($tbl, $col, $notNull = false, $default = '') {
        
      return self::addColumnDT($tbl, $col, 'TIME', $notNull, $default);
        
     
  }
  
  public static function addColumnDateTime($tbl, $col, $notNull = false, $default = '') {
        
      return self::addColumnDT($tbl, $col, 'DATETIME', $notNull, $default);
        
  }
  
  public static function addColumnTimeStamp($tbl, $col, $notNull = false, $default = '') {
        
      return self::addColumnDT($tbl, $col, 'TIMESTAMP', $notNull, $default);
        
  }
  
  public static function addColumnDT($tbl, $col, $type, $notNull = false, $default = '') {
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     $default = ($default?('DEFAULT '.$default):'');
     $notNull = $notNull?'NOT NULL':'NULL';
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` '.$type.' '.$notNull.' '.$default;
     if(self::$mode == self::MODE_PROCESS) {
      return self::executeQuery($sql);
     }
  }
  
  public static function addColumnText($tbl, $col) {
     echo "<p>Add Column: ".$tbl.".".$col."</p>";
     $sql = 'ALTER TABLE `'.$tbl.'` ADD `'.$col.'` TEXT default null';
        
     if(self::$mode == self::MODE_PROCESS) {
      return self::executeQuery($sql);
     }
  }
  
  public static function createTable($tbl) {
      echo "<p>Create table: ".$tbl."</p>";
        
      $sql = "CREATE TABLE  IF NOT EXISTS ".$tbl." (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (id)
      ) ENGINE=InnoDB;";
      
      if(self::$mode == self::MODE_PROCESS) {
         return self::executeQuery($sql);
      }
  }
  public static function tableExists($table) {
     $rs = self::executeQuery("SHOW TABLES LIKE '".$table."'");
     if($rs->next()) {
        return true;
     }
     return false;
  }
  
  public static function describe($tbl) {
     return self::executeQuery("describe ".$tbl);
  }

  public static function executeQuery($sql) {
     return Propel::getConnection()->prepareStatement($sql)->executeQuery();
  }
}
