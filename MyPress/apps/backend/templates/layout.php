 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="stylesheet" type="text/css" media="all" href="/assets/select2/css/select2.css" />
<script type="text/javascript" src="/js/jquery-1.10.1.min.js?v=1.3"></script>
<script>jQuery.migrateMute = true;</script>
<script src="/js/jquery-migrate-1.2.1.min.js?v=1.3"></script>
<script type="text/javascript" src="/assets/select2/js/select2.full.js?v=1"></script> 
<Script src="/js/pace.min.js"></Script>
<script src="/ckeditor/ckeditor.js"></script>
<script src="/ckeditor/adapters/jquery.js"></script> 
<link href="/css/pace.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/css/backend.css"/> 
    
<?php include_http_metas() ?>
<?php include_metas() ?>

<?php include_title() ?> 
</head>
 
<body>
<style>
#sf_admin_list_th_user_info{ width: 100px; }
</style>
<Table class="container">
	<tr>
		<td class="td_menu">
			<div style="text-align: center;">
				<a href="/backend.php"><img alt="LOgO" class="logo" src="/favicon/android-icon-48x48.png"/></a>
			</div>
			<div class="menu_div">
      <?php include_component('h_backend_menu', 'menu');?>
			</div>
		</td>

		<td style="vertical-align:top">
			<div class="hmenu">
				<a href="<?=url_for('h_security/logout')?>">LOGOUT</a>
			</div>
			<?php echo $sf_content ?>

		</td>
	</tr>
</table>
</body>
</html>
